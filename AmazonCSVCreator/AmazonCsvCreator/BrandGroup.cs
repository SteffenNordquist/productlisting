﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCsvCreator
{
    public class BrandGroup
    {
        public string brandname { get; set; }
        public List<string> forPrinter { get; set; }

        public BrandGroup(string brandName, List<string> forPrinter)
        {
            this.brandname = brandName;
            this.forPrinter = forPrinter;
        }
    }
}

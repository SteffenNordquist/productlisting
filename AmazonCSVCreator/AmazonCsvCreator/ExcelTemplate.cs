﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
namespace AmazonCsvCreator
{
    public class ExcelTemplate
    {
       Application application = new Application();
        Workbook workbook;
        Worksheet worksheet;
        int startRow = 4;
        Dictionary<string, int> csvdictionary =
            new Dictionary<string, int>();
        public ExcelTemplate()
        {
            workbook = application.Workbooks.Open("template.xlsx");
            worksheet = workbook.Worksheets[1] as Worksheet;
           Init_Dictionary();
        }

     

        public void CreateCSV(List<CsvEntity> lines)
        {
            Console.WriteLine("Creating CSV.");
            foreach (var line in lines)
            {

                Console.WriteLine(line.external_product_id);
                this.SetValue(startRow, csvdictionary["item_sku"], line.item_sku);
                this.SetValue(startRow, csvdictionary["item_name"], line.item_name);
                this.SetValue(startRow, csvdictionary["external_product_id"], line.external_product_id);
                this.SetValue(startRow, csvdictionary["manufacturer"], line.manufacturer);
                this.SetValue(startRow, csvdictionary["part_number"], line.part_number);
                this.SetValue(startRow, csvdictionary["product_description"], line.product_description);
                this.SetValue(startRow, csvdictionary["quantity"], line.quantity.ToString());
                this.SetValue(startRow, csvdictionary["standard_price"], value: line.standard_price.ToString(CultureInfo.InvariantCulture));
                this.SetValue(startRow, csvdictionary["currency"], line.currency);
                this.SetValue(startRow, csvdictionary["item_weight"], value: line.item_weight.ToString(CultureInfo.InvariantCulture));
                this.SetValue(startRow, csvdictionary["item_weight_unit_of_measure"], line.item_weight_unit_of_measure);
                this.SetValue(startRow, csvdictionary["item_length"], value: line.item_length.ToString());
                this.SetValue(startRow, csvdictionary["item_length_unit_of_measure"], line.item_length_unit_of_measure);
                this.SetValue(startRow, csvdictionary["item_width"], line.item_width.ToString());
                this.SetValue(startRow, csvdictionary["item_width_unit_of_measure"], line.item_width_unit_of_measure);
                this.SetValue(startRow, csvdictionary["item_height"], line.item_height.ToString());
                this.SetValue(startRow, csvdictionary["item_height_unit_of_measure"], line.item_height_unit_of_measure);
                this.SetValue(startRow, csvdictionary["bullet_point1"], line.bullet_point1);
                this.SetValue(startRow, csvdictionary["bullet_point2"], line.bullet_point2);
                this.SetValue(startRow, csvdictionary["bullet_point3"], line.bullet_point3);
                this.SetValue(startRow, csvdictionary["bullet_point4"], line.bullet_point4);
                this.SetValue(startRow, csvdictionary["bullet_point5"], line.bullet_point5);
                this.SetValue(startRow, csvdictionary["main_image_url"], line.main_image_url);
                this.SetValue(startRow, csvdictionary["swatch_image_url"], line.swatch_image_url);
                this.SetValue(startRow, csvdictionary["other_image_url1"], line.other_image_url1);
                this.SetValue(startRow, csvdictionary["other_image_url2"], line.other_image_url2);
                this.SetValue(startRow, csvdictionary["other_image_url3"], line.other_image_url3);
                this.SetValue(startRow, csvdictionary["other_image_url4"], line.other_image_url4);
                this.SetValue(startRow, csvdictionary["other_image_url5"], line.other_image_url5);
                this.SetValue(startRow, csvdictionary["other_image_url6"], line.other_image_url6);
                this.SetValue(startRow, csvdictionary["other_image_url7"], line.other_image_url7);
                this.SetValue(startRow, csvdictionary["other_image_url8"], line.other_image_url8);
                this.SetValue(startRow, csvdictionary["parent_child"], line.parent_child);
                this.SetValue(startRow, csvdictionary["parent_sku"], line.parent_sku);
                this.SetValue(startRow, csvdictionary["relationship_type"], line.relationship_type);
                this.SetValue(startRow, csvdictionary["external_product_id_type"], line.external_product_id_type);
                this.SetValue(startRow, csvdictionary["condition_type"], line.condition_type);
                this.SetValue(startRow, csvdictionary["fulfillment_latency"], line.fulfillment_latency.ToString());
                startRow++;
            }
            workbook.SaveAs("betaTOner.xlsx");
            workbook.Close();
        }

        private void SetValue(int row, int column, string value)
        {
            var range = (Range)worksheet.Cells[row, column];
            range.Value2 = value;

        }

        private void Init_Dictionary()
        {
            csvdictionary.Add("item_sku", 1);
            csvdictionary.Add("item_name", 2);
            csvdictionary.Add("external_product_id", 3);
            csvdictionary.Add("manufacturer", 7);
            csvdictionary.Add("part_number", 8);
            csvdictionary.Add("product_description", 8);
            csvdictionary.Add("quantity", 11);
            csvdictionary.Add("standard_price", 12);
            csvdictionary.Add("currency", 13);
            csvdictionary.Add("item_weight", 34);
            csvdictionary.Add("item_weight_unit_of_measure", 35);
            csvdictionary.Add("item_length", 36);
            csvdictionary.Add("item_length_unit_of_measure", 37);
            csvdictionary.Add("item_width", 38);
            csvdictionary.Add("item_width_unit_of_measure", 39);
            csvdictionary.Add("item_height", 40);
            csvdictionary.Add("item_height_unit_of_measure", 41);
            csvdictionary.Add("bullet_point1", 46);
            csvdictionary.Add("bullet_point2", 47);
            csvdictionary.Add("bullet_point3", 48);
            csvdictionary.Add("bullet_point4", 49);
            csvdictionary.Add("bullet_point5", 50);
            csvdictionary.Add("main_image_url", 65);
            csvdictionary.Add("swatch_image_url", 66);
            csvdictionary.Add("other_image_url1", 67);
            csvdictionary.Add("other_image_url2", 68);
            csvdictionary.Add("other_image_url3", 69);
            csvdictionary.Add("other_image_url4", 70);
            csvdictionary.Add("other_image_url5", 71);
            csvdictionary.Add("other_image_url6", 72);
            csvdictionary.Add("other_image_url7", 73);
            csvdictionary.Add("other_image_url8", 74);
            csvdictionary.Add("parent_child", 83);
            csvdictionary.Add("parent_sku", 84);
            csvdictionary.Add("relationship_type", 82);
            csvdictionary.Add("external_product_id_type", 4);
            csvdictionary.Add("condition_type", 14);
            csvdictionary.Add("condition_note", 16);
            csvdictionary.Add("fulfillment_latency", 17);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCsvCreator
{
    public class CsvEntity
    {
        public string item_sku { get; set; }
        public string item_name { get; set; }
        public string external_product_id { get; set; }
        public string external_product_id_type { get; set; }
        public string manufacturer { get; set; }
        public string part_number { get; set; }
        public string product_description { get; set; }

        public int quantity { get; set; }
        public double standard_price { get; set; }
        public string currency { get; set; }
        public string condition_type { get; set; }
        public int fulfillment_latency { get; set; }
        public double item_weight { get; set; }
        public string item_weight_unit_of_measure { get; set; }
        public double item_length { get; set; }
        public string item_length_unit_of_measure { get; set; }
        public double item_width { get; set; }
        public string item_width_unit_of_measure { get; set; }
        public double item_height { get; set; }
        public string item_height_unit_of_measure { get; set; }

        public string bullet_point1 { get; set; }
        public string bullet_point2 { get; set; }
        public string bullet_point3 { get; set; }
        public string bullet_point4 { get; set; }
        public string bullet_point5 { get; set; }

        public string main_image_url { get; set; }
        public string swatch_image_url { get; set; }
        public string other_image_url1 { get; set; }
        public string other_image_url2 { get; set; }
        public string other_image_url3 { get; set; }
        public string other_image_url4 { get; set; }
        public string other_image_url5 { get; set; }
        public string other_image_url6 { get; set; }
        public string other_image_url7 { get; set; }
        public string other_image_url8 { get; set; }

        public string parent_child { get; set; }
        public string parent_sku { get; set; }
        public string relationship_type { get; set; }


        public CsvEntity(
            string item_sku,
            string item_name,
            string external_product_id,
            string manufacturer,
            string part_number,
            string product_description,
            int quantity,
            double standard_price,
            string currency,
            double item_weight,
            string item_weight_unit_of_measure,
            double item_length,
            string item_length_unit_of_measure,
            double item_width,
            string item_width_unit_of_measure,
            double item_height,
            string item_height_unit_of_measure,
            string bullet_point1,
            string bullet_point2,
            string bullet_point3,
            string bullet_point4,
            string bullet_point5,
            string main_image_url,
            string swatch_image_url,
            string other_image_url1,
            string other_image_url2,
            string other_image_url3,
            string other_image_url4,
            string other_image_url5,
            string other_image_url6,
            string other_image_url7,
            string other_image_url8,
            string parent_child,
            string parent_sku,
          string external_product_id_type = "EAN",
            string condition_type = "New",
            int fulfillment_latency = 4
         )
        {
            this.item_sku = item_sku;
            this.item_name = item_name;
            this.external_product_id = external_product_id;
            this.manufacturer = manufacturer;
            this.part_number = part_number;
            this.product_description = product_description;
            this.quantity = quantity;
            this.standard_price = standard_price;
            this.currency = currency;
            this.item_weight = item_weight;
            this.item_weight_unit_of_measure = item_weight_unit_of_measure;
            this.item_length = item_length;
            this.item_length_unit_of_measure = item_length_unit_of_measure;
            this.item_width = item_width;
            this.item_width_unit_of_measure = item_width_unit_of_measure;
            this.item_height = item_height;
            this.item_height_unit_of_measure = item_height_unit_of_measure;
            this.bullet_point1 = bullet_point1;
            this.bullet_point2 = bullet_point2;
            this.bullet_point3 = bullet_point3;
            this.bullet_point4 = bullet_point4;
            this.bullet_point5 = bullet_point5;
            this.main_image_url = main_image_url;
            this.swatch_image_url = swatch_image_url;
            this.other_image_url1 = other_image_url1;
            this.other_image_url2 = other_image_url2;
            this.other_image_url3 = other_image_url3;
            this.other_image_url4 = other_image_url4;
            this.other_image_url5 = other_image_url5;
            this.other_image_url6 = other_image_url6;
            this.other_image_url7 = other_image_url7;
            this.other_image_url8 = other_image_url8;
            this.parent_child = parent_child;
            this.external_product_id_type = external_product_id_type;
            this.condition_type = condition_type;
            this.fulfillment_latency = fulfillment_latency;
            if (this.parent_child == "child")
            {
                this.parent_sku = parent_sku;
                relationship_type = "Variation";

            }
        }

    }
}

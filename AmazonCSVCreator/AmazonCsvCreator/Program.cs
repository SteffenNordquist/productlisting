﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ProcuctDB.Harper;
using ProcuctDB.ProductSizes;
using ProcuctDB.Suppliers.Toner;

namespace AmazonCsvCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            //HarperDB harperDb =new HarperDB();
            //List<CsvEntity> csvEntities=new List<CsvEntity>();

            //ExcelTemplate template=new ExcelTemplate();
            //foreach (var entity in harperDb.GetAllProducts())
            //{
            //    csvEntities.Add(new CsvEntity("HAR-"+ entity.product.ean, 
            //        entity.product.modelname,
            //        entity.product.ean, "Harper", 
            //        entity.product.artnr,
            //        HtmlStyte(entity.product.productfeatures_ger),
            //        entity.product.qty,Convert.ToDecimal(entity.getPrice()),"EUR",Convert.ToDouble(entity.product.weight_kg),"KG",
            //        Convert.ToDouble(entity.product.length_mm),"mm",
            //        Convert.ToDouble(entity.product.width_mm),"mm",
            //        Convert.ToDouble(entity.product.height_mm),"mm");
            //}

            //TonerDBChildClass childClass = new TonerDBChildClass();
            //childClass.createPlus();
            //Console.Read();
            List<CsvEntity> list=new List<CsvEntity>();
            TonerDBChildClass db=new TonerDBChildClass();
            foreach (var product in db.GetAllProduct())
            {
                int counter = 2;
       
                var toner = (TonerEntity) product;
           //     Console.WriteLine(toner.product.ean);
                bool isparent = toner.plus["brandGroup"]["forPrinter"].AsBsonArray.Count > 0;
                bool isAlone = toner.plus["brandGroup"]["forPrinter"].AsBsonArray.Count == 1;
                string printablepages = "";
                try
                {
                  printablepages=  toner.product.printablepages;
                }
                catch
                {
                }
                foreach (var printername in toner.plus["brandGroup"]["forPrinter"].AsBsonArray)
                {
                    CsvEntity entity;

                    if (isAlone)
                    {
                        entity = new CsvEntity("TON-" + toner.product.ean,
                                toner.plus["brandGroup"]["brandname"].AsString + " " + printername.AsString + " Toner (Printable Pages:" + printablepages + ")",
                                toner.product.ean,
                                toner.plus["brandGroup"]["brandname"].AsString,
                                toner.product.pos,
                                "<p>Printable Pages:" + printablepages + "<p> <br/> " + HtmlStyte(toner.plus["brandGroup"]["forPrinter"].AsBsonArray), toner.product.stock,
                                toner.getPrice(),
                                "EUR",
                                1.03, "KG",
                                327.56, "mm",
                                150.93, "mm",
                                118.42, "mm",
                                "",
                                "",
                                "",
                                "",
                                "",

                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "parent",
                                "TON-" + toner.product.ean
                                );
                    }
                    else
                    {
                        if (isparent)
                        {
                            list.Add(new CsvEntity("TON-" + toner.product.ean +"-P",
                                toner.plus["brandGroup"]["brandname"].AsString + " " + printername.AsString + " Toner (Printable Pages:" + printablepages + ")",
                                "",
                                toner.plus["brandGroup"]["brandname"].AsString,
                                toner.product.pos,
                                "<p>Printable Pages:" + printablepages + "<p> <br/> " + HtmlStyte(toner.plus["brandGroup"]["forPrinter"].AsBsonArray), toner.product.stock,
                                toner.getPrice(),
                                "EUR",
                                1.03, "KG",
                                327.56, "mm",
                                150.93, "mm",
                                118.42, "mm",
                                "",
                                "",
                                "",
                                "",
                                "",

                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "parent",
                                "TON-" + toner.product.ean +"-P"
                                ));
                           entity=new CsvEntity("TON-" + toner.product.ean +"-C1",
                                                         toner.plus["brandGroup"]["brandname"].AsString + " " + printername.AsString + " Toner (Printable Pages:" + printablepages + ")" ,
                                                         toner.product.ean,
                                                         toner.plus["brandGroup"]["brandname"].AsString,
                                                         toner.product.pos,
                                                          "<p>Printable Pages:" + printablepages + "<p> <br/> " + HtmlStyte(toner.plus["brandGroup"]["forPrinter"].AsBsonArray), toner.product.stock,
                                                         toner.getPrice(),
                                                         "EUR",
                                                         1.03, "KG",
                                                         327.56, "mm",
                                                         150.93, "mm",
                                                         118.42, "mm",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",

                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "",
                                                         "child",
                                                         "TON-" + toner.product.ean +"-P"
                                                         );
                            isparent = false;
                        }
                        else
                        {
                            entity = new CsvEntity("TON-" + toner.product.ean +"-C"+counter,
                                                        toner.plus["brandGroup"]["brandname"].AsString + " " + printername.AsString + " Toner (Printable Pages:" + printablepages + ")" ,
                                                        toner.product.ean,
                                                        toner.plus["brandGroup"]["brandname"].AsString,
                                                        toner.product.pos,
                                                        "<p>Printable Pages:" + printablepages + "<p> <br/> " + HtmlStyte(toner.plus["brandGroup"]["forPrinter"].AsBsonArray), toner.product.stock,
                                                        toner.getPrice(),
                                                        "EUR",
                                                        1.03, "KG",
                                                        327.56, "mm",
                                                        150.93, "mm",
                                                        118.42, "mm",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",

                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "",
                                                        "child",
                                                        "TON-" + toner.product.ean +"-P"
                                                        );
                            counter++;
                        }
                    }
                 
                    list.Add(entity);
                    Console.WriteLine("Product: " + entity.external_product_id);
                }
               
            }
            ExcelTemplate template=new ExcelTemplate();
            template.CreateCSV(list);
        }

        public static string HtmlStyte(BsonArray text)
        {
            string output = "<p><b>Compatible Printers: </b></p> <br/> ";
            
            foreach (var insideText in text)
            {
                if (insideText != "")
                {
                    try
                    {
                        output += "<p>"+ insideText.AsString +"</p>";
                    }
                    catch
                    {
                    }
                }


            }
            
            return output;
        }

        public static string HtmlStyte(string text)
        {
            string output = "";
            foreach (var insideText in text.Split(';'))
            {
                if (insideText != "")
                {
                    try
                    {
                        output += "<p><b>" + insideText.Split(':')[0] + ":</b>" + insideText.Split(':')[1] + "</p>";
                    }
                    catch
                    {
                    }
                }


            }
            return output;
        }

    }
}

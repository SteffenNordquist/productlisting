﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ProcuctDB.Suppliers.Toner;

namespace AmazonCsvCreator
{
    class TonerDBChildClass:TonerDB
    {

        public void createPlus()
        {
            foreach (var entity in mongoCollection.FindAll().ToList())
            {
                Console.WriteLine(entity.GetEan());
             ////   BsonArray compatibleToArray=new BsonArray();
             //   Console.WriteLine("Printer for: " +entity.product.ean);
             //   Console.WriteLine(parseCompatibleTo(entity));
             //  Console.WriteLine("Press any key"); Console.Read();
               // entity.plus["brandGroup"] = parseCompatibleTo(entity).;
             //   BsonDocument temp= new BsonDocument();
              
              //  temp.Add("brandGroup",parseCompatibleTo(entity).ToBsonDocument());
             //   temp.Add(entity.plus);
                try
                {
                    if (entity.plus["brandGroup"] == null)
                        entity.plus.Add("brandGroup", parseCompatibleTo(entity).ToBsonDocument());
                    else
                        entity.plus["brandGroup"] = parseCompatibleTo(entity).ToBsonDocument();
        }
                catch
                {
                 
                        entity.plus["brandGroup"] = parseCompatibleTo(entity).ToBsonDocument();
        
                }
             
                mongoCollection.Save(entity);
            }
        }
    
       public BrandGroup parseCompatibleTo(TonerEntity entity)
       {
           string firstprinter = "";
           List<string> sb = new List<string>();//new BsonArray());
            foreach (string printerList in entity.product.compatibleprinters.Split('|'))
            {
                bool firstprinterflag = true;
                foreach (var printer in printerList.Split(','))
                {
                    
                      firstprinter = printerList.Split(',')[0].Split(' ')[0];
                    var printername = firstprinter.Substring(0, firstprinter.LastIndexOf(" ") < 0 ? 0 : firstprinter.LastIndexOf(" "));
                    string firstmodel = "";
                    bool first = true;
                    foreach (var model in printerList.Split(',')[0].Split(' '))
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            firstmodel += model + " ";
                  
                        }
                    }
                    if (printername != firstmodel && firstmodel.Trim() != "")
                        sb.Add(firstmodel);
                    if (firstprinterflag)
                    {
                        if(printername!="")
                        sb.Add(printername);
                        firstprinterflag = false;
                     
                    }
                    else
                    {
                        if(printer.Trim()!="")
                            sb.Add(printer.Replace(firstprinter + " ", "")); 
                    }
                }
            }
            return new BrandGroup(firstprinter, sb.Distinct().ToList());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.Web;
using System.Web.UI.WebControls;
using HtmlAgilityPack;
using ProcuctDB.Harper;

namespace HarperCsvCreator
{
    public class Program
    {
        private static List<string> addedIds = new List<string>();
        private static string[] myFiles = Directory.GetFiles(@"D:\harper_final");


        private static void Main(string[] args)
        {
            var application = new Application();
            var workbook = application.Workbooks.Open("D:\\HarperV51.xlsx");
            var worksheet = workbook.Worksheets[1] as
                Microsoft.Office.Interop.Excel.Worksheet;
            int startRow = 4;
           string test = "";
            do
            {
                try
                {
                    test = worksheet.Cells[startRow, 2].Value.ToString();
                    Console.Write(test);
                    int startColumn = 65;
                    foreach (var pic in GetPictures(test))
                    {
                        var img = (Range) worksheet.Cells[startRow, startColumn];
                        img.Value2 = "http://81.169.171.24/images/harper/" + pic;
                      Console.WriteLine(pic);
                        startColumn++;
                    }
                }
                catch
                {
                    workbook.SaveAs("D:\\beta.xlsx");
                    workbook.Close();
                    break;

                }
                //   break;
                startRow++;
            } while (test.Trim() != "" || startRow < 240);

         
        }

        private static List<string> GetPictures(string filename)
        {
            List<string> temp=new List<string>();
           foreach (var x in myFiles)
            {
                string[] splitName = x.Split('\\');
                string fileName = splitName[splitName.Length - 1];
                if (fileName.StartsWith(filename) )
                {
                    temp.Add(fileName);
                    Console.WriteLine(fileName);
                }
            }
            return temp;
        }

        /*
        static void Main(string[] args)
        {
            // Open Excel and get first worksheet.
            var application = new Application();
            var workbook = application.Workbooks.Open("D:\\template.xlsx");
            var worksheet = workbook.Worksheets[1] as
                Microsoft.Office.Interop.Excel.Worksheet;
            HarperDB harperDb = new HarperDB();
            Dictionary<string, string> picturesDictionary = new Dictionary<string, string>();
            Dictionary<string, string> attributeDictionary = new Dictionary<string, string>();
            foreach (var x in File.ReadAllLines("D:\\_log.txt").ToList())
            {
           
                    picturesDictionary.Add(x.Split('\t')[0], x.Split('\t')[2]);
                    attributeDictionary.Add(x.Split('\t')[0], x.Split('\t')[3]);
              
            }
            int startRow = 4;

            foreach (var harper in harperDb.GetAllProducts())
            {
                string skuText = "HAR-" + harper.product.ean;
                Console.WriteLine(harper.product.ean);
                var sku = (Range)worksheet.Cells[startRow, 1];
                sku.Value2 = skuText;
                var itemname = (Range)worksheet.Cells[startRow, 2];
                itemname.Value2 = harper.product.modelname;
                var externalproductid = (Range)worksheet.Cells[startRow, 3];
                externalproductid.Value2 = harper.product.ean;
                var externalproductidtype = (Range)worksheet.Cells[startRow, 4];
                externalproductidtype.Value2 = "EAN";
                var manufacturer = (Range)worksheet.Cells[startRow, 7];
                manufacturer.Value2 = "Harper";
                var partnumber = (Range)worksheet.Cells[startRow, 8];
                partnumber.Value2 = harper.product.artnr;
                var productdescription = (Range)worksheet.Cells[startRow, 9];
                productdescription.Value2 = HtmlStyte(harper.product.productfeatures_ger);
                var quantity = (Range)worksheet.Cells[startRow, 11];
                quantity.Value2 = harper.product.qty;

                var standardprice = (Range)worksheet.Cells[startRow, 12];
                standardprice.Value2 = harper.getPrice();

                var currency = (Range)worksheet.Cells[startRow, 13];
                currency.Value2 = "EUR";

                var conditionType = (Range)worksheet.Cells[startRow, 14];
                conditionType.Value2 = "New";

                var fulfillmentLatency = (Range)worksheet.Cells[startRow, 15];
                fulfillmentLatency.Value2 = 4;

                var itemweight = (Range)worksheet.Cells[startRow, 34];
                itemweight.Value2 = harper.product.weight_kg;

                var itemweightType = (Range)worksheet.Cells[startRow, 35];
                itemweightType.Value2 = "KG";

                var itemlength = (Range)worksheet.Cells[startRow, 36];
                itemlength.Value2 = harper.product.length_mm;

                var itemlengthType = (Range)worksheet.Cells[startRow, 37];
                itemlengthType.Value2 = "mm";

                var itemwidth = (Range)worksheet.Cells[startRow, 38];
                itemwidth.Value2 = harper.product.width_mm;

                var itemwidthType = (Range)worksheet.Cells[startRow, 39];
                itemwidthType.Value2 = "mm";

                var itemheight = (Range)worksheet.Cells[startRow, 40];
                itemheight.Value2 = harper.product.height_mm;

                var attribute = (Range)worksheet.Cells[startRow, 46];
                try
                {
                    attribute.Value2 = attributeDictionary[skuText];
                }
                catch
                {
                }
                var itemheightType = (Range)worksheet.Cells[startRow, 41];
                itemheightType.Value2 = "mm";
                int startcolumn = 65;
                bool found = false;
                try
                {
                    foreach (
                        var pic in
                            picturesDictionary[skuText
                                ]
                                .Split(';'))
                    {
                        var img = (Range) worksheet.Cells[startRow, startcolumn];
                        img.Value2 = "http://81.169.171.24/images/harper/" + pic.Replace(";", "").Replace("/ ", "/");
                        Console.WriteLine("http://81.169.171.24/images/harper/" +
                                          pic.Replace(";", "").Replace("/ ", "/"));
                        startcolumn++;
                    }
                }
                catch
                {
                    //try
                    //{
                    //      startcolumn = 65;
                    //foreach (
                    //  var pic in
                    //      picturesDictionary[
                    //          harper.product.modelname.Split(' ')[0] + " " + harper.product.modelname.Split(' ')[1]]
                    //          .Split(';'))
                    //{
                    //    var img = (Range)worksheet.Cells[startRow, startcolumn];
                    //    img.Value2 = "http://81.169.171.24/images/harper/" + pic.Replace(";", "").Replace("/ ", "/");
                    //    Console.WriteLine("http://81.169.171.24/images/harper/" +
                    //                      pic.Replace(";", "").Replace("/ ", "/"));
                    //    startcolumn++;
                    //}
                    //}
                    //catch
                    //{
                    //}
                  
                }

                try
                {
                    var color = (Range)worksheet.Cells[startRow, 110];
                    color.Value2 = harper.product.modelname.Split(' ')[2];

                }
                catch
                {
                }
              var parentchild = (Range)worksheet.Cells[startRow, 83];
                //string filename =skuText
                if (
                    IsInList(skuText))
                {
                   parentchild.Value2 = "child";
             
                }
                else
                {
                    addedIds.Add(skuText);
                    parentchild.Value2 = "parent";
                }
                startRow++;
            }
            workbook.SaveAs("D:\\beta.xlsx");
            workbook.Close();
        }
        */
         public static string HtmlStyte(string text)
        {
            string output = "";
            foreach (var insideText in text.Split(';'))
            {
                if (insideText != "")
                {
                    try
                    {
                        output += "<p><b>" + insideText.Split(':')[0] + ":</b>" + insideText.Split(':')[1] + "</p>";
                    }
                    catch
                    {
                    }
                }


            }
            return output;
        }

        public static bool IsInList(string somestring)
        {
            bool flag = false;
            foreach (var listitem in addedIds)
            {
                if (somestring == listitem)
                {

                    flag = true;
                    break;
                }
            }
            return flag;
        }

        public static string GoogleTranslate(string input, string from = "ru", string to = "de")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(input)) return input;
                var fuckedInput = HttpUtility.UrlEncode(input);
                string translateUrlFormat = "https://translate.google.com/?hl=en&ie=UTF8&text=" + fuckedInput +
                                            "&langpair=" + from + "|" + to;
                //  string translateUrlFormat = "https://translate.google.com/#ru/de/" + input;
                WebClient webClient = new WebClient();
                webClient.Encoding = Encoding.Default;
                string source = webClient.DownloadString(translateUrlFormat);
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(source);
                string Result = document.GetElementbyId("result_box").InnerText.ToString();
                //  var asdf = document.DocumentNode.SelectNodes(".//span[@id='result_box']/span");
                return Result;
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }
        }
    }
}

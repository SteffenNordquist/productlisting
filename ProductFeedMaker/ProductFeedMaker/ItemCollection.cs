﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductFeedMaker
{
    public class ItemCollection
    {
        public MongoClient Client;
        public MongoServer Server;
        public MongoDatabase PDatabase;

        public MongoCollection<Item> mongoCollection;
        private long count = 0;
        public ItemCollection(string connString, string CollectionName)
        {
            GetMongoCollection(connString, CollectionName);
        }

        public long CountDocumentBySku(string sku)
        {

            return mongoCollection.Find(Query.EQ("sku", sku)).Count(); ;
        }
        private void GetMongoCollection(string connString, string CollectionName)
        {
            Client = new MongoClient(connString);
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("RepricerCollection");
            mongoCollection = PDatabase.GetCollection<Item>(CollectionName);
            count = mongoCollection.Count();
        }
        public long Count()
        {
            return this.count;
        }
        public Item GetItemBySKU(string sku)
        {
            return mongoCollection.FindOne(Query.EQ("sku", sku));

        }

        public Item GetItemByASIN(string asin)
        {
            return mongoCollection.FindOne(Query.EQ("asin", asin));
        }

        public List<Item> GetItemByPrefix(string prefix)
        {
            return mongoCollection.Find(Query.Matches("sku", prefix)).ToList();
        }

        public List<Item> GetItemByType(string type)
        {
            return mongoCollection.Find(Query.Matches("type", type)).ToList();
        }
        public List<Item> GetItemsByPrefix(string prefix, bool descending)
        {
            if (descending)
                return mongoCollection.Find(Query.Matches("sku", prefix))
                                    .SetSortOrder(
                                        SortBy.Descending("updateDate"))
                                    .ToList();
            else
                return mongoCollection.Find(Query.Matches("sku", prefix))
                                    .SetSortOrder(
                                        SortBy.Ascending("updateDate"))
                                    .ToList();
        }
        public List<Item> GetItemsByType(string type, bool descending)
        {
            if (descending)
                return mongoCollection.Find(Query.Matches("type", type))
                                    .SetSortOrder(
                                        SortBy.Descending("updateDate"))
                                    .SetLimit(10000).ToList();
            else
                return mongoCollection.Find(Query.Matches("type", type))
                                    .SetSortOrder(
                                        SortBy.Ascending("updateDate"))
                                    .SetLimit(10000).ToList();
        }
        public void SaveBook(Item book)
        {
            if (GetItemBySKU(book.sku) != null)
            {
                book.id = GetItemBySKU(book.sku).id;
                mongoCollection.Save(book);
            }
            else
                mongoCollection.Insert(book);
        }
    }
}

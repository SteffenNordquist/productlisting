﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace ProductFeedMaker
{
    public class Product
    {
        public string SKU = "";
        public string EAN = "";
        public string ConditionNote = "";
        public int Inventory;
        public string Currency;
        public string StandardPrice;
        public double price;
        public double priceNetto;
        public double buyPrice;
        public double buyPriceNetto;

        public double Discount;

        public string taxString;
        public double tax;

        public string priceDate;

        public string length;
        public string width;
        public string height;

        public string weight;
        public int Weight = 0;

        public string packingName;

        public Packing packing;
        public ProductSize productSize;
        public ShippingType shippingType;

        public int totalWeight;

        public int intHeight = 9999;

        public int lengthInt = 0;

        public double amaFee = 0.0;

        public double profit = 0.0;

        public double libriCost = 1.5;
        public double inforius = 0.0;
        public double competitvePrice = 0.0;
        public double competitveProfit = 0.0;
        public double competitiveAma = 0.0;
        public double competitivePriceNetto = 0.0;

        double amaPorto = 3;

        //double fightPrice = 999.99;

        public string priceType = "04";

        //public Product(string sku, string ean , string conditionNote, int inventory)
        //{
        //    SKU = sku;
        //    EAN = ean;
        //    ConditionNote = conditionNote;
        //    Inventory = inventory;
        //}

        /// <summary>
        /// sku = ean
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="ean"></param>
        /// <param name="conditionNote"></param>
        public Product(string ean, string conditionNote, int inventory, string currency, string standardPrice)
        {
            SKU = ean;
            EAN = ean;
            ConditionNote = conditionNote;
            Inventory = inventory;
            Currency = currency;
            StandardPrice = standardPrice;
        }


        public Product(string productString, string conditionNote, PackingList packingList)
        {
            try
            {
                if (productString.Length > 0)
                {
                    string[] productItem = productString.Split('\t');

                    length = productItem[8];
                    width = productItem[9];
                    height = productItem[10];
                    weight = productItem[11];

                    if (length == "0") { length = "1000"; }
                    if (width == "0") { width = "1000"; }
                    if (height == "0") { height = "1000"; }
                    if (weight == "0") { weight = "3000"; }

                    priceType = productItem[12];

                    if (length.Length > 0 && width.Length > 0 && height.Length > 0 && weight.Length > 0)
                    {

                        if (productItem.Length > 5)
                        {

                            if (productItem.Length > 13)
                            {
                                try
                                {
                                    
                                    competitvePrice = double.Parse(productItem[13], CultureInfo.InvariantCulture) - 3;

                                    competitivePriceNetto = competitvePrice / ((100 + tax) / 100);
                                }
                                catch { }
                            }



                            if (Properties.Settings.Default.KNVListing)
                                SKU = productItem[0];
                            else
                                SKU = productItem[1];
                            EAN = productItem[1];
                            Inventory = int.Parse(productItem[2]);
                            Currency = productItem[3];
                            StandardPrice = productItem[4];

                            Discount = double.Parse(productItem[5], CultureInfo.InvariantCulture);

                            taxString = productItem[6];
                            priceDate = productItem[7];

                            if (EAN == "4014489105657")
                            { 
                            }


                            int.TryParse(height, out intHeight);

                            int.TryParse(length, out lengthInt);




                            //try
                            //{
                            //    priceType = productItem[12];
                            //    competitvePrice = double.Parse(productItem[13], CultureInfo.InvariantCulture) - 3;

                            //    competitivePriceNetto = competitvePrice / ((100 + tax) / 100);
                            //}
                            //catch { }




                            bool gotTheWeight = int.TryParse(weight, out Weight);

                            if (gotTheWeight)
                            {
                                libriCost = Weight * double.Parse(Properties.Settings.Default.Buchwagen) / 100000;
                            }

                            ConditionNote = conditionNote;

                            double.TryParse(taxString, NumberStyles.Number, CultureInfo.InvariantCulture, out tax);
                            double.TryParse(StandardPrice, NumberStyles.Number, CultureInfo.InvariantCulture, out price);

                            priceNetto = price / ((100 + tax) / 100);

                            buyPrice = getBuyPrice(StandardPrice, Discount);
                            buyPriceNetto = buyPrice / ((100 + tax) / 100);


                            productSize = new ProductSize(length, width, height, weight, packingList);
                            packingName = productSize.getPackingName();
                            packing = productSize.getPacking(packingList);

                            totalWeight = getTotalWeight();



                            inforius = price * 0.03;


                            amaFee = getAmaFee(StandardPrice);
                            competitiveAma = getAmaFee(competitvePrice);


                            shippingType = new ShippingType(this);



                            profit = calcProfit();
                            calcFightPrice();

                            //competitveProfit = calcCompetitiveProfit(); 

                            if (Currency != "EUR")
                            {
                                Inventory = 0;
                            }

                        }
                    }
                }
            }
            catch { }
        }

        private double calcProfit()
        {
            //double price = double.Parse(StandardPrice, CultureInfo.InvariantCulture);

            if (tax == 19.0)
            {
                amaPorto = double.Parse(Properties.Settings.Default.ShippingPrice.Replace(",", "."), CultureInfo.InvariantCulture) * 0.81;
            }
            else
            {
                amaPorto = 3 * 0.93;
            }

            if (EAN == "9783868523027")
            {
            }

            //if(packing != null)
            //    return priceNetto - buyPriceNetto - amaFee - inforius + amaPorto - packing.netPrice - shippingType.price - libriCost;
            //else
            //    return priceNetto - buyPriceNetto - amaFee - inforius + amaPorto - 1.26 - shippingType.price - libriCost;


            if (packing != null)
                profit = priceNetto - buyPriceNetto - amaFee - inforius + amaPorto - packing.netPrice - shippingType.price - libriCost;
            else
                profit = priceNetto - buyPriceNetto - amaFee - inforius + amaPorto - 0.5 - shippingType.price - libriCost;

            return profit;
        }

        private void calcFightPrice()
        {

            double profitWanted = (double)int.Parse(Properties.Settings.Default.Profit) / 100;

            if (priceType == "02")
            {
                while (profit > profitWanted)
                {
                    price = price - 0.01;
                    double fightPriceNetto = price / ((100 + tax) / 100);
                    double fightPriceAma = getAmaFee(price);

                    double inforiusFP = price * 0.03;
                    inforius = inforiusFP;

                    if (packing != null)
                        profit = fightPriceNetto - buyPriceNetto - fightPriceAma - inforiusFP + amaPorto - packing.netPrice - shippingType.price - libriCost;
                    else
                        profit = fightPriceNetto - buyPriceNetto - fightPriceAma - inforiusFP + amaPorto - 0.5 - shippingType.price - libriCost;
                }

                while (profit <= profitWanted)
                {
                    price = price + 0.01;
                    double fightPriceNetto = price / ((100 + tax) / 100);
                    double fightPriceAma = getAmaFee(price);
                    double inforiusFP = price * 0.03;
                    inforius = inforiusFP;

                    if (packing != null)
                        profit = fightPriceNetto - buyPriceNetto - fightPriceAma - inforiusFP + amaPorto - packing.netPrice - shippingType.price - libriCost;
                    else
                        profit = fightPriceNetto - buyPriceNetto - fightPriceAma - inforiusFP + amaPorto - 0.5 - shippingType.price - libriCost;
                }
                price = Math.Round(price, 2);
            }


        }


        //private double calcCompetitiveProfit()
        //{
        //    double price = double.Parse(StandardPrice, CultureInfo.InvariantCulture);
        //    if (priceType == "02" && competitvePrice >0.0)
        //    {
        //        if (packing != null)
        //            return competitivePriceNetto - buyPriceNetto - competitiveAma - inforius + 3 - packing.netPrice - shippingType.price - libriCost;
        //        else
        //            return competitivePriceNetto - buyPriceNetto - competitiveAma - inforius + 3 - 1.26 - shippingType.price - libriCost;
        //    }
        //    return 0.0;

        //}

        private double getAmaFee(string StandardPrice)
        {
            try
            {
                double price = double.Parse(StandardPrice, CultureInfo.InvariantCulture);
                double tax = double.Parse(this.taxString, CultureInfo.InvariantCulture);
                return price * 0.15 + 1.01; // überprüfen
            }
            catch { }

            return 9999.99;
        }

        private double getAmaFee(double price)
        {
            try
            {

                double tax = double.Parse(this.taxString, CultureInfo.InvariantCulture);
                double amaBaseFee = 1.01;

                //if(tax == 19.0)
                //{
                //    amaBaseFee = 0.585;
                //}

                return price * 0.15 + amaBaseFee; // überprüfen
            }
            catch { }

            return 9999.99;
        }

        private double getBuyPrice(string StandardPrice, double Discount)
        {
            try
            {
                return double.Parse(StandardPrice, CultureInfo.InvariantCulture) * ((100 - Discount) / 100);
            }
            catch { }

            return 9999.99;
        }


        private string getShipping()
        {
            return "";
        }



        private int getTotalWeight()
        {
            int totalWeight = 9999;
            try
            {
                totalWeight = int.Parse(this.weight, CultureInfo.InstalledUICulture) + this.packing.weight;
            }
            catch { }

            return totalWeight;
        }


    }
}

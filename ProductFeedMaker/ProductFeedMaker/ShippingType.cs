﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductFeedMaker
{
	public class ShippingType
	{
		string type = "undefined";
		public double price= 0.0;

		public int maxPackageWeigt = 600;
        

        double brief500 = 1.13;
        double brief1000 = 1.93;

        double dpd1000 = 2.63;


		bool bookSendingPossible = false; 

		public ShippingType(Product p)
		{
            //Brief
			if (p.intHeight < 20 && p.totalWeight < 500 && (p.packingName != "" && p.packingName != "K/10"))
			{
				if (p.price < 40)
				{
					type = "Brief";
                    price = brief500;
				}
				else
				{
					type = string.Format("Paket_{0}_1000", p.packing.name);
					price = dpd1000; 
				}
			}
            //Maxibrief
			else if (p.intHeight < 50 && p.totalWeight < 1000 && (p.packingName != "" && p.packingName != "K/10"))
			{
				if (p.price < 40)
				{
					type = "Brief Maxi";
                    price = brief1000;
				}
				else
				{
					type = string.Format("Paket_{0}_1000", p.packing.name);
                    price = dpd1000;
				}
			}
            else if (p.intHeight >= 50 && p.totalWeight < 500 && (p.packingName != "" && p.packingName != "K/10") && p.priceType == "04")
			{
				if (p.price < 40)
				{
					type = "Büchersendung";
					price = 1;
				}
				else
				{
					type = string.Format("Paket_{0}_1000", p.packing.name);
					price = dpd1000; 
				}
			}
            else if (p.intHeight >= 50 && p.totalWeight < 1000 && (p.packingName != "" && p.packingName != "K/10") && p.priceType == "04")
			{

				if (p.price < 40)
				{
					type = "Büchersendung Maxi";
					price = 1.65;
				}
				else
				{
					type = string.Format("Paket_{0}_1000", p.packing.name);
					price = dpd1000;
				}
			}
			else {

				if (p.packing!= null)
				{
					if (p.Weight < 1000 - p.packing.weight)
					{
						type = string.Format("Paket_{0}_1000",p.packing.name);
						price = 2.9;
					}
					else if (p.Weight < 2000 - p.packing.weight)
					{
						type = string.Format("Paket_{0}_2000", p.packing.name);
						price = 3.1;
					}
					else if (p.Weight < 3000 - p.packing.weight)
					{
						type = string.Format("Paket_{0}_3000", p.packing.name);
						price = 3.45;
					}
					else if (p.Weight < 6000 - p.packing.weight)
					{
						type = string.Format("Paket_{0}_6000", p.packing.name);
						price = 3.5;
					}
					else if (p.Weight < 31000 - p.packing.weight)
					{
						type = string.Format("Paket_{0}_10000", p.packing.name);
						price = 3.61;
					}
				}
				else
				{
					if (p.Weight < 1000 - maxPackageWeigt)
					{
						type = "Paket_undefinedSize_1000";
						price = 2.9;
					}
					else if (p.Weight < 2000 - maxPackageWeigt)
					{
						type = "Paket_undefinedSize_2000";
						price = 3.1;
					}
					else if (p.Weight < 3000 - maxPackageWeigt)
					{
						type = "Paket_undefinedSize_3000";
						price = 3.45;
					}
					else if (p.Weight < 6000 - maxPackageWeigt)
					{
						type = "Paket_undefinedSize_6000";
						price = 3.5;
					}
					else if (p.Weight < 31000 - maxPackageWeigt)
					{
						type = "Paket_undefinedSize_10000";
						price = 3.61;
					}
					else
					{
						price = 4.81;
					}
				}
			}


            //Warensendung Überschreibung


            if (p.price <40 && p.tax == 19 && p.Weight < 500 && p.productSize.length >= 100 && p.productSize.length < 350 && p.productSize.width >= 70 && p.productSize.width < 300 && p.productSize.height < 150)
            {
                if (price > 1.93 || type.StartsWith("Büchersendung"))
                {
                    type = "Warensendung";
                    price = 1.90;
                }
            }

		}


		public override string ToString()
		{
			return type;
		}

		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProductFeedMaker
{
	public class PackingList
	{
		public Dictionary<string, Packing> packingListDict = new Dictionary<string, Packing>();

		public PackingList()
		{
			string[] lines = File.ReadAllLines("Templates\\Packings.txt");

			foreach (var line in lines)
			{
				Packing packing = new Packing(line);
				packingListDict.Add(packing.name, packing);
			}
		}

		public Packing getPacking(string name)
		{
 			return packingListDict[name];
		}

	}
}

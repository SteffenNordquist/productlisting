﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductFeedMaker
{
	public class ProductSize
	{
		public int length = 0;
		public int width = 0; 
		public int height = 0; 

		public int weight = 0;
		public string packingName = "";

		PackingList packingList; 

		public ProductSize(string lengthString, string widthString, string heightString, string weightString, PackingList packingL)
		{
			try
			{
				length = int.Parse(lengthString);
				width = int.Parse(widthString);
				height = int.Parse(heightString);
				weight = int.Parse(weightString);

				packingList = packingL; 
			}
			catch { }
		}



		public Packing getPacking(PackingList packingL)
		{
			packingList = packingL;
			if (packingList != null)
			{
				int effectiveLength = Math.Max(length, width);
				int effectiveWidth = Math.Min(length, width);

				double packingLength = effectiveLength + 2 * height * 1.2;
				 double packingWidth = effectiveWidth + height * 1.2;


				foreach (var packing in packingList.packingListDict)
				{
					if (packing.Value.lenght > packingLength && packing.Value.width > packingWidth)
					{
						return packing.Value;
					}
				}
			}
			else
			{
				return new Packing(); 
			}

			return new Packing();
		}


		public string getPackingName()
		{
			Packing packing = getPacking(packingList);

			if (packing != null)
			{
				return packing.name; 
			}
			return ""; 
		}



		






	}
}

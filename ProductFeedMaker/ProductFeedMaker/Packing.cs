﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace ProductFeedMaker
{
	public class Packing
	{
		public string name ="";
		public int lenght = 9999;
		public int width = 9999;
		public int weight = 600;
		public double price = 0.50 * 0.19;
		public double netPrice = 0.50; 


		public Packing(string packingLine)
		{
			string []pack = packingLine.Split('\t');
			name = pack[0];
			lenght = int.Parse(pack[1]);
			width = int.Parse(pack[2]); 
			weight = int.Parse(pack[3]);
			price = double.Parse(pack[4],CultureInfo.InvariantCulture);

			netPrice = price / 1.19; 
		}


		public Packing()
		{
		}
	}



}

﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductFeedMaker
{
    public class Item
    {
        public ObjectId id { get; set; }
        public string sku { get; set; }
        public string asin{get;set;}
        public double buyPrice { get; set; }
        public double minPrice { get; set; }
        public double minProfit { get; set; }
        public string condition{get;set;}
        public string subondition{get;set;}
        public double listingPrice{get;set;}
        public double shippingfee = 3;

        //for the computation
        public double lowestAcceptablePrice { get; set; }
        public double lowestGoodPrice { get; set; }
        public double lowestVerygoodPrice { get; set; }
        public double lowestMintPrice  {get;set;}
        public double lowestNewPrice { get; set; }

        public double highestVeryGoodPrice { get; set; }
        public double highestGoodPrice { get; set; }
        public double highestNewPrice { get; set; }
        public string type { get; set; }
        public DateTime updateDate { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Item\n");
            sb.Append("    SKU:" + sku + "\n");
            sb.Append("    ASIN:" + asin + "\n");
            sb.Append("    condition:" + condition + "\n");
            sb.Append("    subondition:" + subondition + "\n");
            sb.Append("    listingPrice:" + listingPrice + "\n");
            sb.Append("    lowestAcceptablePrice:" + lowestAcceptablePrice + "\n");
            sb.Append("    lowestGoodPrice:" + lowestGoodPrice + "\n");
            sb.Append("    lowestVerygoodPrice:" + lowestVerygoodPrice + "\n");
            sb.Append("    lowestMintPrice:" + lowestMintPrice + "\n");
            sb.Append("    lowestNewPrice:" + lowestNewPrice + "\n");
            sb.Append("    highestVeryGoodPrice:" + highestVeryGoodPrice + "\n");
            sb.Append("    highestGoodPrice:" + highestGoodPrice + "\n");
            sb.Append("    highestNewPrice:" + highestNewPrice + "\n");
            sb.Append("    updateDate:" + updateDate + "\n");
          
            return sb.ToString();
        }
    }
}

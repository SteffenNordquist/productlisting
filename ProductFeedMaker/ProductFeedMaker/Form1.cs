﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Xml;


namespace ProductFeedMaker
{
    public partial class Form1 : Form
    {

        List<Product> ProductList = new List<Product>();
        string conditionNote = Properties.Settings.Default.ConditionNote;
        string Envelope_Template = File.ReadAllText("Templates\\Envelope_Template.xml");
        string Product_Template = File.ReadAllText("Templates\\Product_Template.xml");
        string Delete_Product_Template = File.ReadAllText("Templates\\Delete_Product_Template.xml");
        string Inventory_Template = File.ReadAllText("Templates\\Inventory_Template.xml");
        string Price_Template = File.ReadAllText("Templates\\Price_Template.xml");

        int ItemsPerFile = Properties.Settings.Default.ItemsPerFile;
        string productListPath = Properties.Settings.Default.ProductListPath;
        Inventory pastIventory = new Inventory();


        PackingList packingList = new PackingList();

        public Form1()
        {
            try
            {
                InitializeComponent();


                DeleteAllFilesInXMLFolder();

                List<string> productListStrings = new List<string>();

                string products = "";

                if (productListPath.Contains("http".ToLower()))
                {                   
                    int errorCount = 0;
                    while (products == string.Empty)
                    {

                        try
                        {
                            products = (new System.Net.WebClient().DownloadString(productListPath));
                            productListStrings = Regex.Split(products, "\r\n|\r|\n").ToList<string>();
                        }
                        catch (Exception ex)
                        {
                            string errorFile = string.Format("C:\\Ama3\\errorCode{0}.txt", errorCount++);
                            File.WriteAllText(errorFile, ex.ToString());
                            Process.Start(errorFile);
                        }
                    }
                }
                else
                {
                    productListStrings = File.ReadAllLines(productListPath).ToList();
                }





                foreach (string productString in productListStrings)
                {
                    Product pro = new Product(productString, conditionNote, packingList);

                    //if(pro.Discount > Properties.Settings.Default.MinPercent && double.Parse(pro.StandardPrice,CultureInfo.InvariantCulture) >Properties.Settings.Default.MinPrice && pro.Inventory>=Properties.Settings.Default.MinInventory)
                    //{
                    //    ProductList.Add(pro);
                    //    pastIventory.noCeanUp(pro);
                    //}

                    if (pro.SKU != string.Empty)
                    {
                        String blacklist = Properties.Settings.Default.Blacklist;

                        bool validProduct = pro.EAN.Length == 13 && pro.price != 0;
                        bool matchesProfit = pro.profit >= Properties.Settings.Default.MinPrice/100;
                        bool matchesSize = pro.lengthInt < 1700;
                        bool matchesMinInventory = pro.Inventory >= Properties.Settings.Default.MinInventory;
                        bool notTooHigh = pro.price < 500;
                        bool notBlackListed = !blacklist.Contains(pro.SKU);
                        bool hoffmann = !Properties.Settings.Default.Hoffmann || pro.shippingType.ToString().Contains("Brief");

                        if (validProduct && matchesProfit && matchesSize && matchesMinInventory && notBlackListed && hoffmann)
                        {
                                ProductList.Add(pro);
                                pastIventory.noCeanUp(pro);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DeleteAllFilesInXMLFolder();
                string errorFile = string.Format("C:\\Ama3\\errorCode{0}.txt", 0);
                File.WriteAllText(errorFile, ex.ToString());
            }
        }

        private static void DeleteAllFilesInXMLFolder()
        {
            DirectoryInfo di = new DirectoryInfo("C:\\ama3");
            if (!di.Exists)
            {
                di.Create();
            }

            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            CreateProductFiles();
        }

        private void CreateProductFiles()
        {
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Product";

            foreach (var product in ProductList)
            {

                if (!pastIventory.inventoryDic.ContainsKey(product.SKU))
                {
                    string productMessage = Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.SKU).Replace("@EAN@", product.EAN).Replace("@CONDITIONNOTE@", product.ConditionNote);
                    messageList.AppendLine(productMessage);
                    itemCount++;
                }
                if (itemCount == ItemsPerFile)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }



        private int makeEnvelopeFile(StringBuilder productList, int fileCount, string messageType)
        {
            string envelope = Envelope_Template.Replace("@PRODUCTLIST@", productList.ToString()).Replace("@MESSAGETYPE@", messageType);
            File.WriteAllText(string.Format(@"C:\ama3\{1}{0}.xml", ++fileCount, messageType), envelope);
            productList.Clear();
            return fileCount;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            CreateInventoryFiles();

        }

        private void CreateInventoryFiles()
        {
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Inventory";

            foreach (var product in ProductList)
            {
                InventoryLine iLine = null;
                bool found = pastIventory.inventoryDic.TryGetValue(product.SKU, out iLine);


                if (!found || found && product.Inventory.ToString() != iLine.quantity)
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.SKU).Replace("@QUANTITY@", product.Inventory.ToString());

                        messageList.AppendLine(productMessage);
                    if (itemCount++ == ItemsPerFile - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }
                else
                {
                    if (iLine != null)
                        iLine.sendCleanUp = false;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }

            itemCount = 0;

            foreach (var iLines in pastIventory.inventoryDic)
            {
                if (iLines.Value.sendCleanUp)
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", iLines.Value.sku).Replace("@QUANTITY@", "0");
                    messageList.AppendLine(productMessage);
                    if (itemCount++ == ItemsPerFile - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CreatePriceFiles();
        }

        private void CreatePriceFiles()
        {
            ItemCollection collection = new ItemCollection(@"mongodb://client148:client148devnetworks@148.251.0.235/RepricerCollection", "umnikova");
         
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Price";

            bool sendAllPrices = false;

            foreach (var product in ProductList)
            {
                double inventoryPrice = 0.0;

                if (pastIventory.inventoryDic.ContainsKey(product.SKU))
                    inventoryPrice = double.Parse(pastIventory.inventoryDic[product.SKU].price, CultureInfo.InvariantCulture);


                if (!pastIventory.inventoryDic.ContainsKey(product.SKU) || (inventoryPrice != product.price && product.Inventory > 0))
                {string productMessage ="";
                    if(product.priceType =="04")
                         productMessage = Price_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.SKU).Replace("@CURRENCY@", product.Currency).Replace("@STANDARDPRICE@", product.price.ToString(CultureInfo.InvariantCulture));
                   
                    collection.SaveBook(new Item { sku = product.SKU, minPrice = product.price, type = product.priceType, minProfit = product.profit, buyPrice = product.buyPrice, condition = "new", subondition = "new" });
     
                    messageList.AppendLine(productMessage);
                    itemCount++;
                }
                if (itemCount == ItemsPerFile)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                messageList.Clear();
            }
            //var lines = File.ReadAllLines(Properties.Settings.Default.CorrectPrice);
            //foreach (var line in lines)
            //{
            //    string[] items = line.Split('\t');
            //    string productMessage = Price_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", items[0]).Replace("@CURRENCY@", "EUR").Replace("@STANDARDPRICE@", items[5]);
            //    messageList.AppendLine(productMessage);
            //}
            //fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Inventory";

            foreach (var product in ProductList)
            {
                if (!pastIventory.inventoryDic.ContainsKey(product.EAN) || pastIventory.inventoryDic[product.EAN].quantity != product.Inventory.ToString())
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.SKU).Replace("@QUANTITY@", product.Inventory.ToString());
                    messageList.AppendLine(productMessage);
                    if (itemCount++ == ItemsPerFile - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }

            foreach (var iLines in pastIventory.inventoryDic)
            {
                if (iLines.Value.sendCleanUp)
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", iLines.Value.sku).Replace("@QUANTITY@", "0");
                    messageList.AppendLine(productMessage);
                    if (itemCount++ == ItemsPerFile - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }

        private void btnPriceCorrect_Click(object sender, EventArgs e)
        {
            var lines = File.ReadAllLines(Properties.Settings.Default.CorrectPrice);
            foreach (var line in lines)
            {
                string[] items = line.Split('\t');

            }
        }



        /// <summary>
        /// this makes a cleanup 
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            // Open the inevntory file

            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK)
            {

                Inventory inventory = new Inventory(ofd.FileName);



                // match it for stock from Transfer.html
                List<Product> stockUpdateProductList = inventory.AddProductListForCleanUp(ProductList);

                //update

                createStockInventoryCleanupXML(stockUpdateProductList);


                //write a new inventory




            }
        }

        private void createStockInventoryCleanupXML(List<Product> stockUpdateProductList)
        {
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Inventory";

            foreach (var product in stockUpdateProductList)
            {
                InventoryLine iLine = null;
                //bool found = pastIventory.inventoryDic.TryGetValue(product.SKU,out iLine);

                if (/*!found || found && product.Inventory.ToString() != iLine.quantity*/ true)
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.SKU).Replace("@QUANTITY@", product.Inventory.ToString());
                    messageList.AppendLine(productMessage);
                    if (itemCount++ == ItemsPerFile - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }
                //else {
                //    if(iLine!=null)
                //    iLine.sendCleanUp = false;
                //}
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }

        private void btnPacking_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> packings = new Dictionary<string, string>();

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("ean\tLänge\tBreite\tHöhe\tPackung\tGewicht\tGewicht+Packung\tSendungsart\tVerkauf(Libri)\tVerkaufNetto\tProzente\tEinkaufNetto\tAmazon Fee\tVerpackung\tPorto\tLibri\tVerkaufspreis\tInforius\tProfit");
            foreach (var product in ProductList)
            {


                if (product.EAN == "4002051741457")
                {
                }

                //packings.Add(product.EAN,product.packingName); 

                if (product.shippingType != null)
                    sb.AppendLine(product.EAN + "\t" + product.length + "\t" + product.width + "\t" + product.height + "\t" + product.packingName + "\t" + product.weight + "\t" + product.totalWeight + "\t" + product.shippingType.ToString() + "\t" + product.StandardPrice.Replace(".", ",") + "\t" + product.priceNetto + "\t" + product.Discount + "\t" + product.buyPriceNetto.ToString(CultureInfo.InvariantCulture).Replace(".", ",") + "\t" + product.amaFee + "\t" + product.packing.netPrice + "\t" + product.shippingType.price + "\t" + product.libriCost + "\t" + product.price.ToString(CultureInfo.InvariantCulture).Replace(".", ",") + "\t" + product.inforius + "\t" + product.profit);
                //("C:\\Verpackung.txt", );
            }

            string fileName = "C:\\Ama3\\Verpackung.txt";



            File.WriteAllText(fileName, sb.ToString());

            Process.Start(fileName);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //try
          //  {
                if (Properties.Settings.Default.AutoRun && ProductList.Count > 0)
                {
                    CreateInventoryFiles();
                    CreateProductFiles();
                    CreatePriceFiles();
                    this.Close();
                }
          //  }
         //   catch
         //   {

         //   }
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {


            var lines = File.ReadAllLines(Properties.Settings.Default.InventoryFile);

            List<string> skuList = new List<string>();

            foreach (var line in lines)
            {

                string[] splitLine = line.Split('\t');

                if (splitLine[0] != "sku")
                {
                    skuList.Add(splitLine[0]);




                }

            }




            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Product";

            foreach (var sku in skuList)
            {

                string productMessage = Delete_Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", sku);
                messageList.AppendLine(productMessage);
                itemCount++;

                if (itemCount == ItemsPerFile)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {

            string path = @"c:\ama3\";
            int countfile = 1;
            string filepath = path + "Price";

            string resultpath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string resultfilepath = resultpath + "\\Prices.csv";


            int countfiles = GetCountFiles(path);

            while (countfile <= countfiles)
            {
                //Console.WriteLine("Now getting the data from Price" + countfile);
                XmlDocument xml = new XmlDocument();

                string fullpath = filepath + countfile + ".xml";
                xml.Load(fullpath);

                XmlNodeList xnList = xml.SelectNodes("/AmazonEnvelope/Message/Price");
                foreach (XmlNode xn in xnList)
                {
                    string SKU = xn["SKU"].InnerText;
                    string StandardPrice = xn["StandardPrice"].InnerText;
                    writetoCSV(resultfilepath, SKU, StandardPrice);
                }

                countfile++;
            }
            MessageBox.Show("Created CSV for Prices");
            Console.ReadLine();

        }


        private static int GetCountFiles(string path)
        {
            int fileCount = 0;
            string[] files = new DirectoryInfo(path).GetFiles().Select(o => o.Name).ToArray();

            foreach (var file in files)
            {

                if (file.Contains("Price"))
                {

                    fileCount++;
                }
            }

            return fileCount;
        }

        private static void writetoCSV(string resultfilepath, string SKU, string StandardPrice)
        {

            if (!File.Exists(resultfilepath))
            {

                File.Create(resultfilepath);

            }
            else
            {


                using (StreamWriter sw = File.AppendText(resultfilepath))
                {

                    sw.WriteLine(SKU + "," + StandardPrice);

                }

            }


        }

        private void button7_Click(object sender, EventArgs e)
        {
            CreateInventoryFiles();
            CreateProductFiles();
            CreatePriceFiles();
        }

    }
}

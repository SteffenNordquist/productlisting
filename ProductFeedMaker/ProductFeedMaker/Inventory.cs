﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace ProductFeedMaker
{
	public class Inventory
	{
		public Dictionary<string, InventoryLine> inventoryDic = new Dictionary<string, InventoryLine>();
		public Dictionary<string, InventoryLine> stockCleanUpList = new Dictionary<string, InventoryLine>();

		static string inventoryFile = Properties.Settings.Default.InventoryFile;
        
		public Inventory()
		{
            FileInfo fi = new FileInfo(inventoryFile);
            if (!fi.Exists)
            {
                if(!fi.Directory.Exists)
                {
                    fi.Directory.Create();                 
                }
                fi.Create();
                Thread.Sleep(100);
            }

            var inventoryLines = File.ReadAllLines(inventoryFile).ToList();

            foreach (var expression in Properties.Settings.Default.RegexToIgnore)
            {
                inventoryLines = inventoryLines.Where(x => !new Regex(expression).Match(x).Success).ToList();
            }

            foreach (var line in inventoryLines)
			{
				if (line.Length>0)
				{
					try
					{
						InventoryLine iLine = new InventoryLine(line);
						inventoryDic.Add(iLine.sku, iLine);
					}catch
					{}
				}
			}
		}


		public Inventory(string localInventoryFile)
		{
            var inventoryLines = File.ReadAllLines(localInventoryFile).ToList();

            foreach (var expression in Properties.Settings.Default.RegexToIgnore)
            {
                inventoryLines = inventoryLines.Where(x => !new Regex(expression).Match(x).Success).ToList();
            }

            foreach (var line in inventoryLines)
            {
				if (line.Length > 0)
				{
					InventoryLine iLine = new InventoryLine(line);
					inventoryDic.Add(iLine.sku, iLine);
				}
			}
		}


		public List<Product> AddProductListForCleanUp(List<Product> productList)
		{
			foreach (var product in productList)
			{
				if (inventoryDic.ContainsKey(product.SKU))
				{
					inventoryDic[product.SKU].sendCleanUp = false;
					if (int.Parse(inventoryDic[product.SKU].quantity) != product.Inventory)
					{
						InventoryLine iLine = inventoryDic[product.SKU];
						iLine.quantity = product.Inventory.ToString();
						stockCleanUpList.Add(product.SKU, iLine);
					}
				}
			}

			foreach (var inventoryLine in inventoryDic)
			{
				if (inventoryLine.Value.sendCleanUp)
				{
					InventoryLine iLine = inventoryLine.Value;
					iLine.quantity = 0.ToString();
					stockCleanUpList.Add(iLine.sku,iLine);
				}
			}

			List<Product> returnProductList = new List<Product>();
			foreach (var iLine in stockCleanUpList)
			{
				InventoryLine iLV = iLine.Value;

				Product product = new Product(iLV.sku,"",int.Parse(iLV.quantity),"EUR","9999.99");
				returnProductList.Add(product);
			}


			return returnProductList;

		}


		public bool AddLine(string sku, string price, string quantity)
		{
			if (!inventoryDic.ContainsKey(sku))
			{
				InventoryLine iLine = new InventoryLine(sku, "", price, quantity);
				inventoryDic.Add(iLine.sku, iLine);
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public bool AddLine(Product product)
		{
			if (!inventoryDic.ContainsKey(product.SKU))
			{
				InventoryLine iLine = new InventoryLine(product.SKU, "", product.StandardPrice, product.Inventory.ToString());
				inventoryDic.Add(iLine.sku, iLine);
				return true;
			}
			else
			{
				return false;
			}
		}
		
		internal void noCeanUp(Product pro)
		{
			if (inventoryDic.ContainsKey(pro.SKU))
			{
				inventoryDic[pro.SKU].noCleaUp();
			}
			//else {
			//    AddLine(pro);
			//    inventoryDic[pro.SKU].noCleaUp();
			//}
		}
	}
}

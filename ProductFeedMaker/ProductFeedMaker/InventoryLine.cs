﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductFeedMaker
{
	public class InventoryLine
	{
		public string sku;
		public string asin;
		public string quantity;
		public string price;


	
		/// <summary>
		/// will send a quantity cleanup to clear old // default true
		/// </summary>
		public bool sendCleanUp = true; 		

		public InventoryLine(string line)
		{
			string[] itemlist = line.Split('\t');
			sku = itemlist[0];
			asin = itemlist[1];
			price = itemlist[2];
			quantity = itemlist[3];


			sendCleanUp = !(quantity == "0");
		}
		
		public InventoryLine(string sku, string asin, string price, string quantity)
		{
			this.sku = sku;
			this.asin = asin;
			this.price = price;
			this.quantity = quantity;
			sendCleanUp = !(quantity == "0");
		}
		
		public override string ToString()
		{
			return sku + "\t" + asin + "\t" + price + "\t" + quantity;
		}

		public void noCleaUp()
		{
			sendCleanUp = false;
		}
	}
}

﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.DiscountFactors;

namespace TransferMaker.Tests.DiscountFactors
{
    [TestClass]
    public class SupplierDiscountFactorsTests
    {
        [TestInitialize]
        public void Initialize()
        {
            IConfigurationManager configManager = new AppConfigurationManager(new NullConfigurationSource());
            configManager.SetValue("suppliersconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/Supplier");
            configManager.SetValue("suppliersdatabasename", "Supplier");
            configManager.SetValue("supplierscollectionname", "supplier");

            IDbConnectionFactory dbConnectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());

            IDataContext suppliersDataContext = new SuppliersDataContext(configManager, dbConnectionFactory);
            AppCollections.SetSuppliersCollection(suppliersDataContext);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetDiscountFactors_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            IDiscountFactors discountFactors = new SupplierDiscountFactors();

            //Act
            discountFactors.GetDiscountFactors<string>(null);

            //Assert is done by ExpectedException attribute
        }

        [TestMethod]
        public void GetDiscountFactors_WithValidParam_ReturnsArrayASizeOfTwo()
        {
            //Arrange
            IDiscountFactors discountFactors = new SupplierDiscountFactors();

                //Act
                var discountFactorsValues = discountFactors.GetDiscountFactors<string>("nothing");
            
            //Assert
                Assert.AreEqual(2, discountFactorsValues.Count());
        }

        //will succeed if and only if berk discount in database is 4
        [TestMethod]
        public void GetDiscountFactors_WithSetParam_ReturnsExpectedValueOnFirstValueOfArray()
        {
            //Arrange
            IDiscountFactors discountFactors = new SupplierDiscountFactors();

            //Act
            var discountFactorsValues = discountFactors.GetDiscountFactors<string>("berk");

            //Assert
            Assert.AreEqual(0.04, discountFactorsValues[0]);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.TransfersFactory;

namespace TransferMaker.Tests.Fakes.Transfers.TransfersFactory
{
    public class FakeDefaultTransferListFactory : ITransferListFactory
    {
        public IEnumerable<Core.Transfers.ITransfer> GetTransfers()
        {
            return new List<FakeTransfer>();
        }
    }
}

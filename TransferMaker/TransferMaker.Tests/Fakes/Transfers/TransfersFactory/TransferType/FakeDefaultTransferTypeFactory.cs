﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.TransfersFactory.TransferType;

namespace TransferMaker.Tests.Fakes.Transfers.TransfersFactory.TransferType
{
    public class FakeDefaultTransferTypeFactory : ITransferTypeFactory
    {
        public Core.Transfers.ITransfer GetTransfer<T>(T obj)
        {
            return new FakeTransfer();
        }
    }
}

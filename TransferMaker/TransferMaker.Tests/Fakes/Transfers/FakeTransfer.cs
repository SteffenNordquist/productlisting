﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers;

namespace TransferMaker.Tests.Fakes.Transfers
{
    public class FakeTransfer : ITransfer
    {
        public void DoTransfer()
        {
        }


        public string ErrorLog
        {
            get { return ""; }
        }

        public string SupplierTransferName
        {
            get { return ""; }
        }
    }
}

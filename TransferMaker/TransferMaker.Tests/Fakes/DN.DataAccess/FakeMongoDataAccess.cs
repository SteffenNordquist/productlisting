﻿using DN.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Tests.Fakes.DN.DataAccess.DataCommand;
using TransferMaker.Tests.Fakes.DN.DataAccess.DataQuery;

namespace TransferMaker.Tests.Fakes.DN.DataAccess
{
    public class FakeMongoDataAccess : IDataAccess
    {
        public global::DN.DataAccess.DataCommand.IDataCommand Commands
        {
            get { return new FakeDataCommands(); }
        }

        public global::DN.DataAccess.DataQuery.IDataQuery Queries
        {
            get { return new FakeDataQuery(); }
        }
    }
}

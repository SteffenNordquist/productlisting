﻿using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Tests.Fakes.DN.DataAccess.ConnectionFactory
{
    public class FakeDbConnection_ForSkuBlackList : IDbConnection
    {
        public global::DN.DataAccess.IDataAccess DataAccess
        {
            get { return new FakeMongoDataAccess_ForSkuBlackList(); }
        }
    }
}

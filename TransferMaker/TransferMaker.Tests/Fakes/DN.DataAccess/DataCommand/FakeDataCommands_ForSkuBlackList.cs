﻿using DN.DataAccess.DataCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Tests.Fakes.DN.DataAccess.DataCommand
{
    public class FakeDataCommands_ForSkuBlackList : IDataCommand
    {
        public bool Update<TValue>(string queryKey, string queryValue, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {
            return false;
        }

        public bool Update<TValue>(string query, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {
            return false;
        }

        public bool AddToSet<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            return false;
        }

        public bool Delete(string key, string value)
        {
            return false;
        }

        public bool Delete(string query)
        {
            return false;
        }

        public bool Insert<T>(T obj)
        {
            return false;
        }


        public bool Pull<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            return false;
        }

        public bool Push<TValue>(string query, string arrayField, TValue arrayValue, bool multi = false, bool upsert = false)
        {
            return false;
        }
    }
}

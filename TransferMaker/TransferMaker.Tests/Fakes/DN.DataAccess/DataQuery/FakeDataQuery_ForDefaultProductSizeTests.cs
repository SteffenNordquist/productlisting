﻿using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Tests.Fakes.DN.DataAccess.DataQuery
{
    public class FakeDataQuery_ForDefaultProductSizeTests : IDataQuery
    {
        public List<BsonDocument> tempDocuments = new List<BsonDocument>();
        public BsonDocument document = new BsonDocument();

        public T FindByKey<T>(string key, string value)
        {
            if (value.Contains("Apple"))
            {
                document = new BsonDocument { 
                    { "plus" , new BsonDocument{
                            {"simpleSize" , new BsonDocument{
                                    {"Length",1},
                                    {"Height",2},
                                    {"Width",3},
                                    {"Weight",4}
                                }
                            }
                        }
                    }
                };
            }
            else
            {
                return default(T);
            }

            return BsonSerializer.Deserialize<T>(document);
        }

        public IEnumerable<T> Find<T>(string key, string value, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            List<T> mappedResults = new List<T>();

            tempDocuments.ForEach(x =>
                mappedResults.Add(BsonSerializer.Deserialize<T>(x)));

            return mappedResults;
        }

        public IEnumerable<T> Find<T>(string query, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            List<T> mappedResults = new List<T>();

            tempDocuments.ForEach(x =>
                mappedResults.Add(BsonSerializer.Deserialize<T>(x)));

            return mappedResults;
        }

        public IEnumerable<T> Find<T>(string query, List<MongoDB.Bson.BsonValue> notInThisValues, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            List<T> mappedResults = new List<T>();

            tempDocuments.ForEach(x =>
                mappedResults.Add(BsonSerializer.Deserialize<T>(x)));

            return mappedResults;
        }
    }
}

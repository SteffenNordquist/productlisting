﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Tests.Fakes.DN.DataAccess.ConnectionFactory;

namespace TransferMaker.Tests.Fakes.DataContext
{
    public class FakeDataContext_ForDefaultProductSizeTests : IDataContext
    {
        public global::DN.DataAccess.ConnectionFactory.IDbConnection DbConnection
        {
            get { return new FakeDbConnection_ForDefaultProductSizeTests(); }
        }
    }
}

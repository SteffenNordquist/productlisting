﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Rules;

namespace TransferMaker.Tests.Fakes.Rules
{
    public class AlwaysFalse_FakeRule : IRule
    {

        public bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct
        {
            return false;
        }
    }
}

﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Tests.Fakes.Rules.LimiterRules
{
    public class FakeIProduct_ForSalesLimiterRule_GetSkuReturns9786543210123 : IProduct
    {
        public string getSupplierName()
        {
            throw new NotImplementedException();
        }

        public double getPrice()
        {
            throw new NotImplementedException();
        }

        public double getDiscount()
        {
            throw new NotImplementedException();
        }

        public DN_Classes.Products.Price.PriceType getPriceType()
        {
            throw new NotImplementedException();
        }

        public Measurements getMeasurements()
        {
            throw new NotImplementedException();
        }

        public string getSKU()
        {
            return "9786543210123";
        }

        public List<string> GetSKUList()
        {
            throw new NotImplementedException();
        }

        public List<string> getTitle()
        {
            throw new NotImplementedException();
        }

        public string getCurrency()
        {
            throw new NotImplementedException();
        }

        public List<string> getAsins()
        {
            throw new NotImplementedException();
        }

        public List<DN_Classes.Entities.AsinMultiplier.AsinMultiplierEntity> getAsinMultipliers()
        {
            throw new NotImplementedException();
        }

        public int GetMultiplierByAsin(string asin)
        {
            throw new NotImplementedException();
        }

        public double Stock()
        {
            throw new NotImplementedException();
        }

        public int getStock()
        {
            throw new NotImplementedException();
        }

        public int getTax()
        {
            throw new NotImplementedException();
        }

        public double getWeight()
        {
            throw new NotImplementedException();
        }

        public int getHeight()
        {
            throw new NotImplementedException();
        }

        public int getWidth()
        {
            throw new NotImplementedException();
        }

        public int getLength()
        {
            throw new NotImplementedException();
        }

        public int getSmallestNotZero()
        {
            throw new NotImplementedException();
        }

        public List<string> GetEanList()
        {
            throw new NotImplementedException();
        }

        public List<string> getPublisher()
        {
            throw new NotImplementedException();
        }

        public MongoDB.Bson.BsonDocument Entity()
        {
            throw new NotImplementedException();
        }

        public string GetDescription()
        {
            throw new NotImplementedException();
        }

        public string getArticleNumber()
        {
            throw new NotImplementedException();
        }

        public string getID()
        {
            throw new NotImplementedException();
        }

        public int GetDaysToDelivery()
        {
            throw new NotImplementedException();
        }

        public int GetPackingUnit()
        {
            throw new NotImplementedException();
        }

        public string GetPrefix()
        {
            throw new NotImplementedException();
        }


        public MongoDB.Bson.BsonDocument GetProductPlus()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.BuyPriceDiscount;

namespace TransferMaker.Tests.BuyPriceDiscount
{
    [TestClass]
    public class DefaultBuyPriceDiscountTests
    {
        [TestMethod]
        public void GetDiscountedBuyPrice_WithSetParams_ReturnsExpectedValue()
        {
            //arrange
            DefaultBuyPriceDiscount defaultBuyPriceDiscount = new DefaultBuyPriceDiscount();

            //act
            var value = defaultBuyPriceDiscount.GetDiscountedBuyPrice(7, 78, 5, 0.2, 0 );

            //assert
            Assert.IsTrue(value == 55.40);
        }
    }
}

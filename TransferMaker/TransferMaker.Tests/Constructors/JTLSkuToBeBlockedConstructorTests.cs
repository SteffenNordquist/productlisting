﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Constructors;

namespace TransferMaker.Tests.Constructors
{
    [TestClass]
    public class JTLSkuToBeBlockedConstructorTests
    {
        [TestMethod]
        public void GetSku_JtlSkuExpected(){
            var jTLSkuToBeBlockedConstructor = new JTLSkuToBeBlockedConstructor();

            string jtlSku = jTLSkuToBeBlockedConstructor.GetSKU("WUR-B01616ZYSE-4046778690360");

            Assert.IsTrue(jtlSku == "JTL-B01616ZYSE-4046778690360");
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Constructors;

namespace TransferMaker.Tests.Constructors
{
    [TestClass]
    public class Wur2SkuToBeBlockedConstructorTest
    {
        [TestMethod]
        public void GetSku_WithWurth1Sku_Wurth2Expected()
        {
            var wur2SkuConstructor = new Wur2SkuToBeBlockedConstructor();

            string wur2Sku = wur2SkuConstructor.GetSKU("WUR-B01616ZYSE-4046778690360");

            Assert.IsTrue(wur2Sku == "WUR2-B01616ZYSE-4046778690360");
        }
    }
}

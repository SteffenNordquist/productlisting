﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory.Mongo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB.ProductSizes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Tests.Fakes.DataContext;

namespace TransferMaker.Tests.ProductSize
{
    [TestClass]
    public class DefaultProductSizeTests
    {
        [TestInitialize]
        public void Initialize()
        {
            AppCollections.SetSuppliersCollection(new FakeDataContext_ForDefaultProductSizeTests()); 
        }

        [TestMethod]
        public void LengthWidthHeightWeight_ProcessProductSizesNotYetExecuted_Returns0ForAllSizes()
        {
            //arrange
            TransferMaker.Core.ProductSize.SupplierDefaultProductSize defaultProductSize = new TransferMaker.Core.ProductSize.SupplierDefaultProductSize(new DefaultSupplierNameFactory());

            //act

            //assert
            Assert.IsTrue(defaultProductSize.Length == 0 &&
                defaultProductSize.Height == 0 &&
                defaultProductSize.Width == 0 &&
                defaultProductSize.Weight ==0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProcessProductSizes_WithNullParam_ThrowsArgumentNullException()
        {
            //arrange
            TransferMaker.Core.ProductSize.SupplierDefaultProductSize defaultProductSize = new TransferMaker.Core.ProductSize.SupplierDefaultProductSize(new DefaultSupplierNameFactory());

            //act
            defaultProductSize.ProcessProductSizes(null);

            //assert is done by ExpectedException
        }

        [TestMethod]
        public void ProcessProductSizes_WithValidParam_NoException()
        {
            //arrange
            TransferMaker.Core.ProductSize.SupplierDefaultProductSize defaultProductSize = new TransferMaker.Core.ProductSize.SupplierDefaultProductSize(new DefaultSupplierNameFactory());

            try
            {
                //act
                defaultProductSize.ProcessProductSizes("this_supplier_does_not_exists");
            }
            catch (Exception ex)
            {
                //assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void ProcessProductSizes_WithSetFakeDataContextAndSetParam_ReturnsExpectedValues()
        {
            //arrange
            TransferMaker.Core.ProductSize.SupplierDefaultProductSize defaultProductSize = new TransferMaker.Core.ProductSize.SupplierDefaultProductSize(new DefaultSupplierNameFactory());

            //act
            defaultProductSize.ProcessProductSizes("Apple");

            //assert
            Assert.IsTrue(defaultProductSize.Length == 1 &&
                defaultProductSize.Height == 2 &&
                defaultProductSize.Width == 3 &&
                defaultProductSize.Weight == 4);
        }
    }
}

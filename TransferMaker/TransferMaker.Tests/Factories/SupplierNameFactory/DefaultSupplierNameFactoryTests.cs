﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.Factories.SupplierNameFactory;

namespace TransferMaker.Tests.Factories.SupplierNameFactory
{
    [TestClass]
    public class DefaultSupplierNameFactoryTests
    {

        [TestInitialize]
        public void Initialize()
        {
            IConfigurationManager configManager = new AppConfigurationManager(new NullConfigurationSource());
            configManager.SetValue("suppliersconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/Supplier");
            configManager.SetValue("suppliersdatabasename", "Supplier");
            configManager.SetValue("supplierscollectionname", "supplier");

            IDbConnectionFactory dbConnectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());

            IDataContext suppliersDataContext = new SuppliersDataContext(configManager, dbConnectionFactory);
            AppCollections.SetSuppliersCollection(suppliersDataContext); 
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetSupplierName_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            ISupplierNameFactory supplierNameFactory = new DefaultSupplierNameFactory();

            //Act
            supplierNameFactory.GetSupplierName(null);

            //Assert done by ExpectedException attribute
        }

        [TestMethod]
        public void GetSupplierName_WithValidParam_NoException()
        {
            //Arrange
            ISupplierNameFactory supplierNameFactory = new DefaultSupplierNameFactory();

            try
            {
                //Act
                supplierNameFactory.GetSupplierName("nothing");
            }
            catch(Exception ex)
            {
                //Assert

                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void GetSupplierName_WithSetParam_ReturnsExpectedValue()
        {
            //Arrange
            ISupplierNameFactory supplierNameFactory = new DefaultSupplierNameFactory();

            //Act
            string supplierName = supplierNameFactory.GetSupplierName("JpC"); 
            
            //Assert
            Assert.AreEqual("JPC", supplierName);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory.Mongo;
using DN.DataAccess.ConnectionFactory;

namespace TransferMaker.Tests.DataContext
{
    [TestClass]
    public class SupppliersDataContextTests
    {
        [TestMethod]
        public void DbConnection_WithCorrectConString_NoException()
        {
            //Arrange
            IConfigurationManager configManager = new AppConfigurationManager(new NullConfigurationSource());
            configManager.SetValue("suppliersconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/Supplier");
            configManager.SetValue("suppliersdatabasename", "Supplier");
            configManager.SetValue("supplierscollectionname", "supplier");
            SuppliersDataContext suppliersDataContext = new SuppliersDataContext(configManager, new MongoConnectionFactory(new MongoConnectionProperties()));

            try
            {
                //Act
                IDbConnection dbConnection = suppliersDataContext.DbConnection;
            }
            catch { 
                //Assert
                Assert.Fail();
            }
        }
    }
}

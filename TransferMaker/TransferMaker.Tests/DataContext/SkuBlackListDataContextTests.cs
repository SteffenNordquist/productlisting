﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Tests.DataContext
{
    [TestClass]
    public class SkuBlackListDataContextTests
    {
        [TestMethod]
        public void DbConnection_WithCorrectConString_NoException()
        {
            //Arrange
            IConfigurationManager configManager = new AppConfigurationManager(new NullConfigurationSource());
            configManager.SetValue("skublacklistconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/SkuBlacklist");
            configManager.SetValue("skublacklistdatabasename", "SkuBlacklist");
            configManager.SetValue("skublacklistcollectionname", "skus");
            SkuBlackListDataContext suppliersDataContext = new SkuBlackListDataContext(configManager, new MongoConnectionFactory(new MongoConnectionProperties()));

            try
            {
                //Act
                IDbConnection dbConnection = suppliersDataContext.DbConnection;
            }
            catch
            {
                //Assert
                Assert.Fail();
            }
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransferMaker.Core.DataContext;
using MongoDB.Driver;
using MongoDB.Bson;
using TransferMaker.Tests.Fakes.DataContext;

namespace TransferMaker.Tests.DataContext
{
    [TestClass]
    public class AppCollectionsTests
    {
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void SuppliersCollection_NotSetCollection_ThrowsNullReferenceException()
        {
            //Arrange

            //Act
            var appCollections = AppCollections.SuppliersCollection();

            //Assert done by ExpectedException
        }

        [TestMethod]
        public void SuppliersCollection_SetCollection_NoException()
        {
            //Arrange
            AppCollections.SetSuppliersCollection(new FakeDataContext());

            try
            {
                //Act
                var appCollections = AppCollections.SuppliersCollection();
            }
            catch (Exception ex)
            {
                //Assert 
                Assert.Fail(ex.Message);
            }
        }
    }
}

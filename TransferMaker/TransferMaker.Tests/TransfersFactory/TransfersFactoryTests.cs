﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransferMaker.Core.Transfers.TransfersFactory;

namespace TransferMaker.Tests.TransfersFactory
{
    [TestClass]
    public class TransfersFactoryTests
    {
        [TestMethod]
        public void GetTransfers_WhenNothingToReturn_ReturnsEmptyList()
        {
            //Arrange
            ITransfersFactory defaultTransfersFactory = new DefaultTransfersFactory();

            //Act
            var transfers = defaultTransfersFactory.GetTransfers();

            //Assert 
            Assert.IsNotNull(transfers);

        }
    }
}

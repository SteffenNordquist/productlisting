﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransferMaker.Core.Activation;
using TransferMaker.Core.DataContext;
using TransferMaker.Tests.Fakes;
using TransferMaker.Tests.Fakes.DN.DataAccess.DataQuery;

namespace TransferMaker.Tests.Activation
{
    [TestClass]
    public class SupplierActivationCheckerTests
    {
        [TestInitialize]
        public void Initialize()
        {
            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsActivated_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            SupplierActivationChecker supplierActivationChecker = new SupplierActivationChecker(new FakeDataQuery());

            //Act
            supplierActivationChecker.IsActivated<string>(null);

            //Assert is done by 'ExpectedException' attribute
        }

        [TestMethod]
        public void IsActivated_WithNotNullParam_NoException()
        {
            //Arrange
            SupplierActivationChecker supplierActivationChecker = new SupplierActivationChecker(new FakeDataQuery());

            try
            {
                //Act
                Console.WriteLine(supplierActivationChecker.IsActivated<string>("1234567890"));
            }
            catch (Exception ex)
            {
                //Assert
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void IsActivated_WithSetDataContextAndParam_ReturnsFalse()
        {
            //Arrange
            FakeDataQuery fakeMongoDataQuery = new FakeDataQuery();
            fakeMongoDataQuery.document.Add(new MongoDB.Bson.BsonElement("_id", "1234567890"));
            fakeMongoDataQuery.tempDocuments.Add(fakeMongoDataQuery.document);

            SupplierActivationChecker supplierActivationChecker = new SupplierActivationChecker(fakeMongoDataQuery);

           
                //Act
                bool isActivated = supplierActivationChecker.IsActivated<string>("1234567890");
            
           
                //Assert
                Assert.IsFalse(isActivated);
            
        }

        [TestMethod]
        public void IsActivated_WithSetDataContextAndParam_ReturnsTrue()
        {
            //Arrange
            SupplierActivationChecker supplierActivationChecker = new SupplierActivationChecker(new FakeDataQuery());


            //Act
            bool isActivated = supplierActivationChecker.IsActivated<string>("djsdjsad293");


            //Assert
            Assert.IsTrue(isActivated);

        }
    }
}

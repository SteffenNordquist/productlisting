﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransferMaker.Core.Transfers.TransfersFactory;
using TransferMaker.Core.Transfers.TransfersFactory.TransferType;
using MongoDB.Driver;
using MongoDB.Bson;
using TransferMaker.Core.DataContext;
using ProcuctDB;
using TransferMaker.Core.Transfers;
using DN.DataAccess.ConfigurationData;
using TransferMaker.Tests.Fakes.DataContext;
using TransferMaker.Tests.Fakes.DN.DataAccess.ConfigurationData;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Core.DiscountFactors;
using TransferMaker.Core.Multiplier;
using TransferMaker.Core.Transfers.Rows;
using TransferMaker.Core.BuyPriceDiscount;
using TransferMaker.Core.Rules;
using TransferMaker.Core.Transfers.Header;
using DN_Classes.OrderLimiter;
using TransferMaker.Core.ProductSize;
using ProcuctDB.Suppliers;

namespace TransferMaker.Tests.Transfers.TransfersFactory.TransferType
{
    [TestClass]
    public class DefaultTransferTypeFactoryTests
    {
        private IConfigurationManager configManager;
        private ISupplierNameFactory supplierNameFactory;
        private IDiscountFactors discountFactorsWorker;
        private ISalesLimiter salesLimiter;
        private IHeader transferHeader;
        private IJtlHeader jtlTransferHeader;
        private IRulesManager rulesManager;
        private IMultiplier multiplierWorker;
        private ITransferRow transferRowWorkder;
        private IJtlTransferRow jtlTransferRowWorker;
        private IBuyPriceDiscount buyPriceDiscountWorker;

        [TestInitialize]
        public void Initialize()
        {
            configManager = new AppConfigurationManager(new FakeConfigurationSource());
            configManager.SetValue("normalBasedSuppliers", "libri;knv;bvw;bremer;hoffmann");
            configManager.SetValue("underscoreAsinBasedSuppliers", "sww");
            configManager.SetValue("reductionBasedSuppliers", "jtl");

            AppCollections.SetSuppliersCollection(new FakeDataContext());

            supplierNameFactory = new  DefaultSupplierNameFactory();
            discountFactorsWorker = new SupplierDiscountFactors();
            salesLimiter = new SalesLimiter();
            transferHeader = new TransferHeader();
            jtlTransferHeader = new JtlTransferHeader(transferHeader);
            rulesManager = new DefaultRulesManager();
            multiplierWorker = new DefaultMultiplier();
            transferRowWorkder = new DefaultTransferRow(new SupplierDefaultProductSize(new DefaultSupplierNameFactory()), new ProductSizesCollectionWorker());
            jtlTransferRowWorker = new DefaultJtlTransferRow();
            buyPriceDiscountWorker = new DefaultBuyPriceDiscount();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetTransfer_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            DefaultTransferTypeFactory defaultTransferTypeFactory = new DefaultTransferTypeFactory(
                configManager,
                supplierNameFactory,
                discountFactorsWorker,
                salesLimiter,
                transferHeader,
                jtlTransferHeader,
                rulesManager,
                multiplierWorker,
                transferRowWorkder,
                jtlTransferRowWorker,
                buyPriceDiscountWorker);

            //Act
            defaultTransferTypeFactory.GetTransfer<object>(null);

            //Assert done by the ExpectedException
        }

        [TestMethod]
        public void GetTransfer_WithReductionBasedSupplierSet_ReturnsReductionBasedTransfer()
        {
            //Arrange
            DefaultTransferTypeFactory defaultTransferTypeFactory = new DefaultTransferTypeFactory(
                configManager,
                supplierNameFactory,
                discountFactorsWorker,
                salesLimiter,
                transferHeader,
                jtlTransferHeader,
                rulesManager,
                multiplierWorker,
                transferRowWorkder,
                jtlTransferRowWorker,
                buyPriceDiscountWorker);

            //Act
            IDatabase jtlDB = new JTLDB();
            var transfer = defaultTransferTypeFactory.GetTransfer<IDatabase>(jtlDB);

            //Assert 
            Assert.IsInstanceOfType(transfer, typeof(ReductionBasedTransfer));
        }

        [TestMethod]
        public void GetTransfer_WithUnderscoreAsinBasedSet_ReturnsUnderscoreAsinBasedTransfer()
        {
            //Arrange
            DefaultTransferTypeFactory defaultTransferTypeFactory = new DefaultTransferTypeFactory(
                configManager,
                supplierNameFactory,
                discountFactorsWorker,
                salesLimiter,
                transferHeader,
                jtlTransferHeader,
                rulesManager,
                multiplierWorker,
                transferRowWorkder,
                jtlTransferRowWorker,
                buyPriceDiscountWorker);

            //Act
            IDatabase swwDB = new SWWDB();
            var transfer = defaultTransferTypeFactory.GetTransfer<IDatabase>(swwDB);

            //Assert 
            Assert.IsInstanceOfType(transfer, typeof(UnderscoreAsinBasedTransfer));
        }

        [TestMethod]
        public void GetTransfer_WithNormalBasedSupplierSet_ReturnsNormalTransfer()
        {
            //Arrange
            DefaultTransferTypeFactory defaultTransferTypeFactory = new DefaultTransferTypeFactory(
                configManager,
                supplierNameFactory,
                discountFactorsWorker,
                salesLimiter,
                transferHeader,
                jtlTransferHeader,
                rulesManager,
                multiplierWorker,
                transferRowWorkder,
                jtlTransferRowWorker,
                buyPriceDiscountWorker);

            //Act
            IAmazonTransfer knvDB = new KnvDB();
            var transfer = defaultTransferTypeFactory.GetTransfer<IAmazonTransfer>(knvDB);

            //Assert 
            Assert.IsInstanceOfType(transfer, typeof(NormalTransfer));
        }

        [TestMethod]
        public void GetTransfer_WithAsinBasedSupplierSet_ReturnsAsinBasedTransfer()
        {
            //Arrange
            DefaultTransferTypeFactory defaultTransferTypeFactory = new DefaultTransferTypeFactory(
                configManager,
                supplierNameFactory,
                discountFactorsWorker,
                salesLimiter,
                transferHeader,
                jtlTransferHeader,
                rulesManager,
                multiplierWorker,
                transferRowWorkder,
                jtlTransferRowWorker,
                buyPriceDiscountWorker);

            //Act
            IDatabase jpcDB = new JpcDB();
            var transfer = defaultTransferTypeFactory.GetTransfer<IDatabase>(jpcDB);

            //Assert 
            Assert.IsInstanceOfType(transfer, typeof(AsinBasedTransfer));
        }
    }
}

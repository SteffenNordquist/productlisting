﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransferMaker.Core.Transfers.TransfersFactory;
using TransferMaker.Core.Transfers.TransfersFactory.TransferType;
using MongoDB.Driver;
using MongoDB.Bson;
using TransferMaker.Core.DataContext;
using TransferMaker.Tests.Fakes;
using DN.DataAccess.ConfigurationData;
using TransferMaker.Tests.Fakes.Transfers.TransfersFactory.TransferType;
using TransferMaker.Tests.Fakes.DN.DataAccess.ConfigurationData;

namespace TransferMaker.Tests.Transfers.TransfersFactory
{
    [TestClass]
    public class DefaultTransferListFactoryTests
    {
        private IConfigurationManager configManager;

        [TestInitialize]
        public void Initialize()
        {
            configManager = new AppConfigurationManager(new FakeConfigurationSource());
            
        }

        [TestMethod]
        public void GetTransfers_WhenNothingToReturn_ReturnsEmptyList()
        {
            //Arrange
            ITransferListFactory defaultTransfersFactory = new DefaultTransferListFactory(new FakeDefaultTransferTypeFactory(), configManager);

            //Act
            var transfers = defaultTransfersFactory.GetTransfers();

            //Assert 
            Assert.IsNotNull(transfers);
        }

        [TestMethod]
        public void GetTransfers_WhenSettingsConfiguredOneTransferToTrue_ReturnsNotEmptyNotNull()
        {
            //Arrange
            configManager.SetValue("TransferForJTL", "true");
            ITransferListFactory defaultTransfersFactory = new DefaultTransferListFactory(new FakeDefaultTransferTypeFactory(), configManager);

            //Act
            var transfers = defaultTransfersFactory.GetTransfers();

            var enumerator = transfers.GetEnumerator();
            int counter = 0;
            while (enumerator.MoveNext())
            {
                counter++;
            }

            //Assert 
            Assert.AreEqual(1, counter);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.Header;

namespace TransferMaker.Tests.Transfers.Header
{
    [TestClass]
    public class JtlTransferHeaderTests
    {
        [TestMethod]
        public void GetHeader_ReturnsNotEmpty()
        {
            //Arrange
            JtlTransferHeader transferHeader = new JtlTransferHeader(new TransferHeader());

            //Act
            string header = transferHeader.GetJtlHeader();

            //Assert
            Assert.AreNotEqual("", header);
        }
    }
}

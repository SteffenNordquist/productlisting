﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.Header;

namespace TransferMaker.Tests.Transfers.Header
{
    [TestClass]
    public class TransferHeaderTests
    {
        [TestMethod]
        public void GetHeader_ReturnsNotEmpty()
        {
            //Arrange
            TransferHeader transferHeader = new TransferHeader();

            //Act
            string header = transferHeader.GetHeader();

            //Assert
            Assert.AreNotEqual("", header);
        }
    }
}

﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory.Mongo;
using DN_Classes.OrderLimiter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Activation;
using TransferMaker.Core.BuyPriceDiscount;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.DiscountFactors;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Core.Multiplier;
using TransferMaker.Core.ProductSize;
using TransferMaker.Core.Rules;
using TransferMaker.Core.Transfers;
using TransferMaker.Core.Transfers.Header;
using TransferMaker.Core.Transfers.Rows;

namespace TransferMaker.Tests.Transfers
{
    [TestClass]
    public class AsinBasedTransferTests
    {
        private AsinBasedTransfer asinBasedTransfer;
        private IConfigurationManager configManager;

        //arrange
        [TestInitialize]
        public void Initialize()
        {
            configManager = new AppConfigurationManager(new NullConfigurationSource());
            configManager.SetValue("suppliersconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/Supplier");
            configManager.SetValue("suppliersdatabasename", "Supplier");
            configManager.SetValue("supplierscollectionname", "supplier");
            configManager.SetValue("TransferFilename", @"C:\temp\{0}transferV3.txt");
            configManager.SetValue("JTLTransferFilename", @"C:\temp\jtl_{0}transferV3.txt");
            configManager.SetValue("SalesLimiterBlockedListFileName", @"C:\temp\{0}BlockedList.txt");
            IDataContext suppliersDataContext = new SuppliersDataContext(configManager, new MongoConnectionFactory(new MongoConnectionProperties()));
            AppCollections.SetSuppliersCollection(suppliersDataContext); 

            asinBasedTransfer = new AsinBasedTransfer(
                    new BongDB(),
                    new SupplierActivationChecker(AppCollections.SuppliersCollection().DataAccess.Queries),
                    new DefaultSupplierNameFactory(),
                    new SupplierDiscountFactors(),
                    configManager,
                    new SalesLimiter(),
                    new TransferHeader(),
                    new JtlTransferHeader(new TransferHeader()),
                    new DefaultRulesManager(),
                    new DefaultMultiplier(),
                    new DefaultTransferRow(new SupplierDefaultProductSize(new DefaultSupplierNameFactory()), new ProductSizesCollectionWorker()),
                    new DefaultJtlTransferRow(),
                    new DefaultBuyPriceDiscount()
                );
        }

        [TestMethod]
        public void SupplierTransferName_NotNull()
        {
            //act
            string supplierName = asinBasedTransfer.SupplierTransferName;

            //assert
            Assert.IsNotNull(supplierName);
        }

        [TestMethod]
        public void ErrorLog_NotNull() 
        {
            //act
            string errorLog = asinBasedTransfer.ErrorLog;

            //assert
            Assert.IsNotNull(errorLog);
        }

        [TestMethod]
        public void DoTransfer_TransferFileCreated()
        {
            string filename = String.Format(configManager.GetValue("TransferFilename"), asinBasedTransfer.SupplierTransferName);
            string jtlfilename = String.Format(configManager.GetValue("JTLTransferFilename"), asinBasedTransfer.SupplierTransferName);

            //act
            asinBasedTransfer.DoTransfer();
            string errorLog = asinBasedTransfer.ErrorLog;

            //assert
            Assert.IsTrue(File.Exists(filename) && File.Exists(jtlfilename) && errorLog !="");
        }
    }
}

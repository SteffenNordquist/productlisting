﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Core.ProductSize;
using TransferMaker.Core.Transfers.Rows;

namespace TransferMaker.Tests.Transfers.Rows
{
    [TestClass]
    public class DefaultTransferRowTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetProductLine_WithNullIProductParam_ThrowsArgumentNullException()
        {
            //Arrange
            DefaultTransferRow defaultTransferRow = new DefaultTransferRow(new SupplierDefaultProductSize(new DefaultSupplierNameFactory()), new ProductSizesCollectionWorker());

            //Act
            defaultTransferRow.GetProductLine(null, "", "", 0);

            //Assert is done by ExpectedException
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.Generator;
using TransferMaker.Tests.Fakes.Transfers.TransfersFactory;

namespace TransferMaker.Tests.Transfers.Generator
{
    [TestClass]
    public class DefaultTransfersGeneratorTests 
    {
        [TestMethod]
        public void Generate_NoException()
        {
            //Arrange
            ITransfersGenerator defaultTransfersGenerator = new DefaultTransfersGenerator(new FakeDefaultTransferListFactory());

            try
            {
                //Act
                defaultTransfersGenerator.Generate();
            }
            catch(Exception ex)
            {
                //Assert
                Assert.Fail(ex.Message);
            }
        }
    }
}

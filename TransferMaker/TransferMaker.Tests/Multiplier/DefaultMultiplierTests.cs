﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Multiplier;

namespace TransferMaker.Tests.Multiplier
{
    [TestClass]
    public class DefaultMultiplierTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetMultiplier_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            DefaultMultiplier defaultMulitplier = new DefaultMultiplier();

            //Act
            defaultMulitplier.GetMultiplier(null);

            //Assert, done by the ExpectedException attribute

        }

        [TestMethod]
        public void GetMultiplier_WithSetSkuParam_Returns1()
        {
            //Arrange
            DefaultMultiplier defaultMulitplier = new DefaultMultiplier();
 
            //Act
            int multiplier = defaultMulitplier.GetMultiplier("hyphenSplit_Count_0");

            //Assert
            Assert.IsTrue(multiplier == 1);
        }

        [TestMethod]
        public void GetMultiplier_WithSetSkuParam_Returns4()
        {
            //Arrange
            DefaultMultiplier defaultMulitplier = new DefaultMultiplier();

            //Act
            int multiplier = defaultMulitplier.GetMultiplier("the-multiplier-is-4");

            //Assert
            Assert.IsTrue(multiplier == 4);
        }
    }
}

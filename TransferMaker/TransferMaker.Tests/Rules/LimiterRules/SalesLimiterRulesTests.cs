﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Rules.LimiterRules;
using TransferMaker.Tests.Fakes.Rules.LimiterRules;

namespace TransferMaker.Tests.Rules.LimiterRules
{
    [TestClass]
    public class SalesLimiterRulesTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void HasPassed_WithNullParams_ThrowsArgumentNullException()
        {
            //Arrange
            List<string> blackListEans = new List<string>();
            SalesLimiterRules salesLimiterRules = new SalesLimiterRules(blackListEans);

            //Act
            salesLimiterRules.HasPassed<KnvEntity, string>(null, null);

            //Assert is done by ExpectedException
        }

        [TestMethod]
        public void HasPassed_WithSetBlackListAndIProduct_ReturnsTrue()
        {
            //Arrange
            List<string> blackListEans = new List<string>();
            blackListEans.Add("9782");
            SalesLimiterRules salesLimiterRules = new SalesLimiterRules(blackListEans);

            //Act
            bool hasPassed = salesLimiterRules.HasPassed<FakeIProduct_ForSalesLimiterRule_GetSkuReturns9786543210123, string>(new FakeIProduct_ForSalesLimiterRule_GetSkuReturns9786543210123(), "nothing");

            //Assert
            Assert.IsTrue(hasPassed);
        }

        [TestMethod]
        public void HasPassed_WithSetBlackListAndIProduct_ReturnsFalse()
        {
            //Arrange
            List<string> blackListEans = new List<string>();
            blackListEans.Add("9786543210123");
            SalesLimiterRules salesLimiterRules = new SalesLimiterRules(blackListEans);

            //Act
            bool hasPassed = salesLimiterRules.HasPassed<FakeIProduct_ForSalesLimiterRule_GetSkuReturns9786543210123, string>(new FakeIProduct_ForSalesLimiterRule_GetSkuReturns9786543210123(), "nothing");

            //Assert
            Assert.IsFalse(hasPassed);
        }
    }
}

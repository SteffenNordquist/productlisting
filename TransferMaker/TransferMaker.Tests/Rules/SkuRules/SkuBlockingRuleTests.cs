﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.Rules.SizeRules;
using TransferMaker.Core.Rules.SkuRules;
using TransferMaker.Tests.Fakes.DataContext;

namespace TransferMaker.Tests.Rules.SkuRules
{
    [TestClass]
    public class SkuBlockingRuleTests
    {
        [TestInitialize]
        public void Initialize()
        {
            AppCollections.SetSkuBlackListCollection(new FakeDataContext_ForSkuBlackList());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void HasPassed_WithNullParams_ThrowsArgumentNullException()
        {
            //Arrange
            SkuBlockingRule skuBlockingRule = new SkuBlockingRule();

            //Act
            skuBlockingRule.HasPassed<KnvEntity, string>(null, null);

            //Assert done by ExpectedException attribue
        }

        [TestMethod]
        public void HasPassed_WithSetEmptyBlackList_ReturnsTrue()
        {
            //Arrange
            SkuBlockingRule skuBlockingRule = new SkuBlockingRule();

            //Act
            bool hasPassed = skuBlockingRule.HasPassed<KnvEntity, string>(new KnvEntity(), "1234567890");

            //Assert 
            Assert.IsTrue(hasPassed);
        }

        [TestMethod]
        public void HasPassed_WithSetSkuBlackListAndSkuParam_ReturnsFalse()
        {
            //Arrange
            SkuBlockingRule skuBlockingRule = new SkuBlockingRule();

            //Act
            bool hasPassed = skuBlockingRule.HasPassed<KnvEntity, string>(new KnvEntity(), "1");

            //Assert 
            Assert.IsFalse(hasPassed);
        }

        [TestMethod]
        public void HasPassed_WithSetSkuBlackListAndSkuParam_ReturnsTrue()
        {
            //Arrange
            SkuBlockingRule skuBlockingRule = new SkuBlockingRule();

            //Act
            bool hasPassed = skuBlockingRule.HasPassed<KnvEntity, string>(new KnvEntity(), "10");

            //Assert 
            Assert.IsTrue(hasPassed);
        }

        [TestMethod]
        public void HasPassed_WithWurthSKU_ExcludesWurth2SKU()
        {
            //Arrange
            SkuBlockingRule skuBlockingRule = new SkuBlockingRule();

            //Act
            bool hasPassed = skuBlockingRule.HasPassed<Wuerth2Entity, string>(new Wuerth2Entity(), "WUR2-B004LSUCCI-4038898764328-1");

            //Assert 
            Assert.IsTrue(hasPassed);
        }
    }
}

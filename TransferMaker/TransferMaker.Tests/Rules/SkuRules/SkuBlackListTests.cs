﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.Rules.SkuRules;
using TransferMaker.Tests.Fakes.DataContext;

namespace TransferMaker.Tests.Rules.SkuRules
{
    [TestClass]
    public class SkuBlackListTests : SkuBlackList
    {
        [TestInitialize]
        public void Initialize(){
            AppCollections.SetSkuBlackListCollection(new FakeDataContext_ForSkuBlackList());
        }

        [TestMethod]
        public void GetBlackList_WithSetFakeDataContext_ReturnsNotEmptyList()
        {
            //Arrange is done by inheriting SkuBlackList

            //Act
            var list = GetBlackList();

            //Assert
            Assert.IsTrue(list.Count() > 0);
            
        }
    }
}

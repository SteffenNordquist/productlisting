﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Rules.SizeRules;
using TransferMaker.Tests.Fakes.Rules.SizeRules;

namespace TransferMaker.Tests.Rules.SizeRules
{
    [TestClass]
    public class SizeRuleTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void HasPassed_WithNullParams_ThrowsArgumentNullException()
        {
            //Arrange
            SizeRule sizeRule = new SizeRule();

            //Act
            sizeRule.HasPassed<KnvEntity, string>(null, null);

            //Assert, done by the ExpectedException

        }

        [TestMethod]
        public void HasPassed_WithSetIProduct_ReturnsTrue()
        {
            //Arrange
            SizeRule sizeRule = new SizeRule();

            //Act
            bool hasPassed = sizeRule.HasPassed<FakeIProduct_ForSizeRule_WillPass, string>(new FakeIProduct_ForSizeRule_WillPass(), "sku here");

            //Assert
            Assert.IsTrue(hasPassed);

        }

        [TestMethod]
        public void HasPassed_WithSetIProduct_ReturnsFalse()
        {
            //Arrange
            SizeRule sizeRule = new SizeRule();

            //Act
            bool hasPassed = sizeRule.HasPassed<FakeIProduct_ForSizeRule_WillNotPass, string>(new FakeIProduct_ForSizeRule_WillNotPass(), "sku here");

            //Assert
            Assert.IsFalse(hasPassed);

        }
    }

    
}

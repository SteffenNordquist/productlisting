﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Rules.SizeRules;
using TransferMaker.Tests.Fakes.Rules.SizeRules;

namespace TransferMaker.Tests.Rules.SizeRules
{
    [TestClass]
    public class GramsRuleTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void HasPassed_WithNullParams_ThrowsArgumentNullException()
        {
            //Arrange
            GramsRule gramsRule = new GramsRule();

            //Act
            gramsRule.HasPassed<KnvEntity, string>(null, null);

            //Assert, done by the ExpectedException

        }

        [TestMethod]
        public void HasPassed_WithSetIProduct_ReturnsTrue()
        {
            //Arrange
            GramsRule gramsRule = new GramsRule();

            //Act
            bool hasPassed = gramsRule.HasPassed<FakeIProduct_ForSizeRule_WillPass, string>(new FakeIProduct_ForSizeRule_WillPass(), "sku here");

            //Assert
            Assert.IsTrue(hasPassed);

        }

        [TestMethod]
        public void HasPassed_WithSetIProduct_ReturnsFalse()
        {
            //Arrange
            GramsRule gramsRule = new GramsRule();

            //Act
            bool hasPassed = gramsRule.HasPassed<FakeIProduct_ForSizeRule_WillNotPass, string>(new FakeIProduct_ForSizeRule_WillNotPass(), "sku here");

            //Assert
            Assert.IsFalse(hasPassed);

        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Rules;
using TransferMaker.Tests.Fakes.Rules;

namespace TransferMaker.Tests.Rules
{
    [TestClass]
    public class DefaultRulesManagerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddRule_WithNullParam_ThrowsArgumentNullException()
        {
            //Arrange
            IRulesManager defaultRulesManager = new DefaultRulesManager();

            //Act
            defaultRulesManager.AddRule(null);

            //Assert is done by ExpectedException attribute
        }

        [TestMethod]
        public void AddRule_WithAddedNewRule_RulesCountIncreases()
        {
            //Arrange
            IRulesManager defaultRulesManager = new DefaultRulesManager();
            
            //Act
            defaultRulesManager.AddRule(new AlwaysTrue_FakeRule());

            //Assert
            Assert.AreEqual(1, defaultRulesManager.RulesCount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void HasPassed_WithNullParams_ThrowsArgumentNullException()
        {
            //Arrange
            IRulesManager defaultRulesManager = new DefaultRulesManager();

            //Act
            defaultRulesManager.HasPassed<IProduct, string>(null, null);

            //Assert is done by ExpectedException attribute
        }

        [TestMethod]
        public void HasPassed_WithAddedRuleSetToReturnTrue_ReturnsTrue()
        {
            //Arrange
            IRulesManager defaultRulesManager = new DefaultRulesManager();
            defaultRulesManager.AddRule(new AlwaysTrue_FakeRule());

            //Act
            bool hasPassed = defaultRulesManager.HasPassed<IProduct, string>(new KnvEntity(), "sku here");

            //Assert
            Assert.IsTrue(hasPassed);
        }

        [TestMethod]
        public void HasPassed_WithAddedRulesOneSetToReturnFalse_ReturnsFalse()
        {
            //Arrange
            IRulesManager defaultRulesManager = new DefaultRulesManager();
            defaultRulesManager.AddRule(new AlwaysTrue_FakeRule());
            defaultRulesManager.AddRule(new AlwaysFalse_FakeRule());

            //Act
            bool hasPassed = defaultRulesManager.HasPassed<IProduct, string>(new KnvEntity(), "sku here");

            //Assert
            Assert.IsFalse(hasPassed);
        }

        [TestMethod]
        public void Clear_RulesCountIsZero()
        {
            //Arrange
            IRulesManager defaultRulesManager = new DefaultRulesManager();
            defaultRulesManager.AddRule(new AlwaysTrue_FakeRule());
            defaultRulesManager.AddRule(new AlwaysFalse_FakeRule());

            //Act
            defaultRulesManager.Clear();

            //Assert
            Assert.AreEqual(0, defaultRulesManager.RulesCount);
        }

    }
}

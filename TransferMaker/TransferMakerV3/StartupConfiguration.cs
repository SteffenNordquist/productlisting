﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.BuyPriceDiscount;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.DiscountFactors;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Core.Multiplier;
using TransferMaker.Core.ProductSize;
using TransferMaker.Core.Rules;
using TransferMaker.Core.Transfers.Generator;
using TransferMaker.Core.Transfers.Header;
using TransferMaker.Core.Transfers.Rows;
using TransferMaker.Core.Transfers.TransfersFactory;
using TransferMaker.Core.Transfers.TransfersFactory.TransferType;
using System.Configuration;
using DN_Classes.OrderLimiter;
using DN_Classes.Sales;
using TransferMaker.Core.Suppliers;
using TransferMaker.Core.Transfers.Sales;

namespace TransferMakerV3
{
    public class StartupConfiguration
    {
        private IUnityContainer container;

        public StartupConfiguration(IUnityContainer container)
        {
            this.container = container;

            RegisterRequirements();
        }

        private void RegisterRequirements()
        {
            string configPath = AppDomain.CurrentDomain.BaseDirectory + @"\TransferMaker.config";
            IConfigurationSource configSource = new ConfigurationSource(configPath);
            container.RegisterInstance(configSource);

            container.RegisterType<IConfigurationManager, AppConfigurationManager>();
            container.RegisterType<IConnectionProperties, MongoConnectionProperties>();
            container.RegisterType<IDbConnectionFactory, MongoConnectionFactory>();

            //supplier collection
            IDataContext suppliersDataContext = new SuppliersDataContext(container.Resolve<IConfigurationManager>(), container.Resolve<IDbConnectionFactory>());
            AppCollections.SetSuppliersCollection(suppliersDataContext); 
            //sku black list collection
            IDataContext skuBlackListDataContext = new SkuBlackListDataContext(container.Resolve<IConfigurationManager>(), container.Resolve<IDbConnectionFactory>());
            AppCollections.SetSkuBlackListCollection(skuBlackListDataContext);
            //product sizes collection
            IDataContext productSizesDataContext = new ProductSizesDataContext(container.Resolve<IConfigurationManager>(), container.Resolve<IDbConnectionFactory>());
            AppCollections.SetProductSizesCollection(productSizesDataContext); 

            container.RegisterType<ITransferTypeFactory, DefaultTransferTypeFactory>();

            container.RegisterType<ISupplierNameFactory, DefaultSupplierNameFactory>();
            container.RegisterType<IDiscountFactors, SupplierDiscountFactors>();
            container.RegisterType<ISalesLimiter, SalesLimiter>();
            container.RegisterType<ISold, EansSold>();
            container.RegisterType<IHeader, TransferHeader>();
            container.RegisterType<IJtlHeader, JtlTransferHeader>();
            container.RegisterType<IRulesManager, DefaultRulesManager>();
            container.RegisterType<IMultiplier, DefaultMultiplier>();
            container.RegisterType<ITransferRow, DefaultTransferRow>();
            container.RegisterType<IJtlTransferRow, DefaultJtlTransferRow>();
            container.RegisterType<IBuyPriceDiscount, DefaultBuyPriceDiscount>();
            container.RegisterType<IDefaultProductSize, SupplierDefaultProductSize>();
            container.RegisterType<IProductSizesCollection, ProductSizesCollectionWorker>();
            container.RegisterType<ITransferListFactory, DefaultTransferListFactory>();

            container.RegisterType<ITransfersGenerator, DefaultTransfersGenerator>();

            container.RegisterType<ISuppliers, BKSuppliers>();
            container.RegisterType<ISalesTransfer, JtlTransfersBasedSalesTransfer>();


        }

        public TResult Resolve<TResult>()
        {
            return container.Resolve<TResult>();
        }
    }
}

﻿using DN_Classes.OrderLimiter;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TransferMakerV3.InclusionRules
{
    public class LimiterRules : IRule
    {
        private List<string> blockedList;

        public LimiterRules(List<string> blockedList)
        {
            this.blockedList = blockedList;
        }
        public bool HasPassed<T, SKU>(T obj, SKU s)
        {
            IProduct product = (IProduct)obj;

            bool flag = true;
            string ean = new Regex(@"\d{13}").Match(product.getSKU()).Value;
            foreach (string blockedEan in blockedList)
            {
                if (ean.Equals(blockedEan))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }




        public void Clear()
        {
            
        }

    }
}

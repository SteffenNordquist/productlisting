﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Blocking;

namespace TransferMakerV3.InclusionRules.BlockingRules
{
    public class SkuBlockingRule : Transfer, IRule
    {
        public bool HasPassed<T, SKU>(T obj, SKU s)
        {
            string nsku = (string)(object)s;
            nsku = CleanupSKU(nsku);

            if (skuToBlock.Where(x => x.Contains(nsku)).Select(x => x).Count() == 0)
            {
                return true;
            }
            return false;
        }

        private string CleanupSKU(string sku)
        {
            string[] skuChunks = sku.Split('-');

            //removing multiplier
            if (skuChunks.Count() > 3)
            {
                string newSku = String.Join("-", skuChunks.Take(skuChunks.Length - 1));
                sku = newSku;
            }

            return sku;
        }


        public void Clear()
        {
            
        }
    }
}

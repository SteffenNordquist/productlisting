﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.InclusionRules
{
    public class RulesManager : IRule
    {

        private List<IRule> rules;

        public RulesManager()
        {
            rules = new List<IRule>();
        }

        public void AddRule(IRule rule)
        {
            rules.Add(rule);
        }

        public bool HasPassed<T, SKU>(T obj, SKU s)
        {
            IProduct product = (IProduct)obj;
            if (rules.FindAll(x => !x.HasPassed<IProduct, SKU>(product, s)).Count() == 0)
            {
                return true;
            }

            return false;
        }


        public void Clear()
        {
            rules.Clear();
        }
    }
}

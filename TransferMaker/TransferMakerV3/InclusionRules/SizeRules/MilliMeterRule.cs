﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.InclusionRules.SizeRules
{
    public class MilliMeterRule : IRule
    {
        public bool HasPassed<T, SKU>(T obj, SKU s)
        {
            IProduct product = (IProduct)obj;
            List<int> sizes = new List<int>
                    {
                        product.getLength(),
                        product.getWidth(),
                        product.getHeight()
                    };

            if (sizes.Max() < 1000)
            {
                return true;
            }

            return false;
        }


        public void Clear()
        {
            
        }
    }
}

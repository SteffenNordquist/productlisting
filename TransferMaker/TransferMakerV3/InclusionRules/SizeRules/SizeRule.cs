﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.InclusionRules.SizeRules
{
    public class SizeRule : IRule
    {
        private List<IRule> rules;

        public SizeRule()
        {
            this.rules = new List<IRule>
            {
                new GramsRule(),
                new MilliMeterRule()
            };
        }

        public bool HasPassed<T, SKU>(T obj, SKU s)
        {
            IProduct product = (IProduct)obj;

            if (rules.FindAll(x => !x.HasPassed<IProduct, SKU>(product, s)).Count() == 0)
            {
                return true;
            }

            return false;
        }


        public void Clear()
        {

        }

    }
}

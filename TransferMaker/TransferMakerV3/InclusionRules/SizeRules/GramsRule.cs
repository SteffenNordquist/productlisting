﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.InclusionRules.SizeRules
{
    public class GramsRule : IRule
    {
        public bool HasPassed<T, SKU>(T obj, SKU s)
        {
            IProduct product = (IProduct)obj;
            if (product.getWeight() < 30000)
            {
                return true;
            }

            return false;
        }


        public void Clear()
        {
            
        }
    }
}

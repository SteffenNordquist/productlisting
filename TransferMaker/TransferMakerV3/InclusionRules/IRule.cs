﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.InclusionRules
{
    public interface IRule
    {
        bool HasPassed<T, SKU>(T obj, SKU s);

        void Clear();
    }
}

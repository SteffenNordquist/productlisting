﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3
{
    public class Multiplier
    {
        private int multiplier = 1;

        public Multiplier(string sku)
        {

            if(sku.Split('-').Length > 3)
            {
                if (sku.Split('-')[3] != "")
                {
                    try
                    {
                        multiplier = Convert.ToInt32(sku.Split('-')[3]);
                    }
                    catch { }
                }
            }
        }

        public int GetMultiplier()
        {
            return multiplier;
        }
    }
}

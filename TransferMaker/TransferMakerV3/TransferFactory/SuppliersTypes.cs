﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.TransferFactory
{
    public class SuppliersTypes
    {
        public List<string> NormalSuppliersList()
        {
            List<string> normalSuppliers = new List<string>();

            normalSuppliers.Add("libri");
            normalSuppliers.Add("knv");
            normalSuppliers.Add("bvw");
            normalSuppliers.Add("bremer");
          
            

            return normalSuppliers;
        }

        public List<string> UnderscoreAsinSuppliersList()
        {
            List<string> underscoreAsinSuppliersSuppliers = new List<string>();

            underscoreAsinSuppliersSuppliers.Add("sww");
       
            return underscoreAsinSuppliersSuppliers;
        }

        public List<string> NeedReductionSupplierList()
        {
            List<string> suppliers = new List<string>();

            suppliers.Add("jtl");

            return suppliers;
        }
    }
}

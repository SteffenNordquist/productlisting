﻿using DN_Classes.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Interfaces;

namespace TransferMakerV3.TransferFactory
{
    public class TransfersFilter
    {
        public static List<ITransfer> CheckActivation(List<ITransfer> supplierTransfers, List<SupplierEntity> deactivatedSuppliers)
        {
            List<ITransfer> supplierTransfersChecked = new List<ITransfer>();

            foreach (ITransfer supplierTransfer in supplierTransfers)
            {
                if (deactivatedSuppliers.Find(x => x._id.ToLower() == supplierTransfer.GetTransferName().ToLower()) != null)
                {
                    supplierTransfer.SetActivation(false);
                }

                supplierTransfersChecked.Add(supplierTransfer);
            }

            return supplierTransfersChecked;
        }

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.TransferFactory
{
    public class NormalSuppliers
    {
        public List<string> NormalSuppliersList()
        {
            List<string> normalSuppliers = new List<string>();

            normalSuppliers.Add("libri");
            normalSuppliers.Add("knv");
            normalSuppliers.Add("bvw");
            normalSuppliers.Add("bremer");
            normalSuppliers.Add("hoffmann");

            return normalSuppliers;
        }

        public List<string> UnderscoreAsinSuppliersList()
        {
            List<string> underscoreAsinSuppliersSuppliers = new List<string>();

            underscoreAsinSuppliersSuppliers.Add("sww");

            return underscoreAsinSuppliersSuppliers;
        }
    }
}

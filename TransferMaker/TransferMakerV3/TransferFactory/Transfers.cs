﻿using ProcuctDB;
using ProcuctDB.Suppliers;
using ProcuctDB.DatabaseFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Implementations;
using TransferMakerV3.Interfaces;
using TransferMakerV3.TransferFactory;

namespace TransferMakerV3.TransferFactory
{
    public class Transfers
    {
        public static List<ITransfer> GetTransfers()
        {
            bool TranferForJTL = Properties.Settings.Default.TransferForJTL;
            bool TransferForKNVLIBRI = Properties.Settings.Default.TransferForKNVLIBRI;
            bool TransferForOtherSuppliers = Properties.Settings.Default.TransferForOTHERSUPPLIERS;


            TransferTypeFactory transferFactory = new TransferTypeFactory();

            List<ITransfer> SupplierListTransfer = new List<ITransfer>();

            //*********************for JTL only*******************************

            if (TranferForJTL == true)
            {
                JTLDB jtlDB = new JTLDB();
                ITransfer jtltransfer = transferFactory.GetTransfer(transferFactory.GetType(jtlDB.GetType().Name.Replace("DB", "")));
                jtltransfer.SetDatabase<IDatabase>(jtlDB);
                SupplierListTransfer.Add(jtltransfer);
            }
            //****************************************************************


            //*******************for knv and libri*****************************

            if (TransferForKNVLIBRI == true)
            {
                IAmazonTransfer database = new KnvDB();
                ITransfer knvtransfer = transferFactory.GetTransfer(transferFactory.GetType(database.GetType().Name.Replace("DB", "")));
                knvtransfer.SetDatabase<IAmazonTransfer>(database);
                SupplierListTransfer.Add(knvtransfer);

                IAmazonTransfer libridatabase = new LibriDB();
                ITransfer libritransfer = transferFactory.GetTransfer(transferFactory.GetType(database.GetType().Name.Replace("DB", "")));
                libritransfer.SetDatabase<IAmazonTransfer>(libridatabase);
                SupplierListTransfer.Add(libritransfer);
            }
            //*****************************************************************


            //**********for all suppliers except jtl, libri and knv*************

            if (TransferForOtherSuppliers == true)
            {
                ProductFinder productFinder = new ProductFinder();
                foreach (IDatabase database in productFinder.getAllDatabases().Where(x =>
                    !x.GetType().Name.ToLower().Contains("libri") &&
                    !x.GetType().Name.ToLower().Contains("knv") &&
                    !x.GetType().Name.ToLower().Contains("jtl") &&
                    !x.GetType().Name.ToLower().Contains("bremer") &&
                    !x.GetType().Name.ToLower().Contains("bvw") 
                    
                    ))
                {
                    ITransfer transfer = transferFactory.GetTransfer(transferFactory.GetType(database.GetType().Name.Replace("DB", "")));
                    transfer.SetDatabase<IDatabase>(database);
                    SupplierListTransfer.Add(transfer);
                }
                IAmazonTransfer bvwdatabase = new BVWDB();
                ITransfer bvwtransfer = transferFactory.GetTransfer(transferFactory.GetType(bvwdatabase.GetType().Name.Replace("DB", "")));
                bvwtransfer.SetDatabase<IAmazonTransfer>(bvwdatabase);
                
                //IAmazonTransfer hoffmanndatabase = new HoffmannDB();
                //ITransfer hoffmanntransfer = transferFactory.GetTransfer(transferFactory.GetType(hoffmanndatabase.GetType().Name.Replace("DB", "")));
                //hoffmanntransfer.SetDatabase<IAmazonTransfer>(hoffmanndatabase);

                SupplierListTransfer.Add(bvwtransfer);
                //SupplierListTransfer.Add(hoffmanntransfer);
            }
            //******************************************************************

            return SupplierListTransfer;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Implementations;
using TransferMakerV3.Interfaces;

namespace TransferMakerV3.TransferFactory
{
    public class TransferTypeFactory : SuppliersTypes
    {
        public ITransfer GetTransfer(TransferType transferType)
        {
            if (transferType == TransferType.asin)
            {
                return new AsinBasedTransfer();
            }
            else if (transferType == TransferType.underscoreAsin)
            {
                return new UnderscoreAsinBasedTransfer();
            }
            else if (transferType == TransferType.reduction)
            {
                return new ReductionBasedTransfer();
            }
            else
            {
                return new NormalTransfer();
            }
        }

        public TransferType GetType(string supplierName)
        {
            if (NormalSuppliersList().Exists(x=>x ==  supplierName.ToLower()))
            {
                return TransferType.normal;
            }
            else if (UnderscoreAsinSuppliersList().Exists(x => x == supplierName.ToLower()))
            {
                return TransferType.underscoreAsin;
            }
            else if (NeedReductionSupplierList().Exists(x => x == supplierName.ToLower()))
            {
                return TransferType.reduction;
            }
            else
            {
                return TransferType.asin;
            }
        }

        
    }
}

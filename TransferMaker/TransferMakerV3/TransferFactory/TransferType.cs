﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.TransferFactory
{
    public enum TransferType
    {
        normal,
        asin,
        underscoreAsin,
        reduction
    }
}

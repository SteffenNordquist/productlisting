﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.ReductionRules
{
    class SoldAndUnshipStockDeductionRule : IStockDeductionRule
    {
        private MongoCollection<CalculatedProfitEntity> singleCalculatedCollection = null;

        public SoldAndUnshipStockDeductionRule()
        {
            singleCalculatedCollection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/CalculatedProfit")
                .GetServer()
                .GetDatabase("CalculatedProfit")
                .GetCollection<CalculatedProfitEntity>("SingleCalculatedItem");
        }

        public int GetFinalStock(IProduct product)
        {
            string ean = product.GetEanList()[0];
            //string ean = "9781445488127";
            var matchedProducts = singleCalculatedCollection.Find(Query.And(Query.EQ("Ean", ean), Query.GT("PurchasedDate", DateTime.Now.AddDays(-31)))).SetFields("plus");

            int deductionCount = 0;

            if (matchedProducts.Count() > 0)
            {
                deductionCount = matchedProducts
                     .Where(x => 
                                x.plus == null ||
                                (x.plus != null && !x.plus.Contains("dVersandt")) ||
                                (x.plus != null && x.plus.Contains("dVersandt") && x.plus["dVersandt"].AsBsonValue == BsonNull.Value) ||
                                (x.plus != null && x.plus.Contains("dVersandt") && x.plus["dVersandt"].AsBsonValue == null) ||
                                (x.plus != null && x.plus.Contains("dVersandt") && x.plus["dVersandt"].AsString == "")
                            
                         )
                     .Count();
            }

            return product.getStock() - deductionCount;


        }


    }
}

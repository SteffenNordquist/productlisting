﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.ReductionRules
{
    public interface IStockDeductionRule
    {
        int GetFinalStock(IProduct product);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3
{
    public class DiscountedBuyPrice
    {
        public double Tax { get; set; }
        public double SellPrice { get; set; }
        public double Discount { get; set; }
        public double Factor1 { get; set; }
        public double Factor2 { get; set; }

        public DiscountedBuyPrice(double tax, double sellPrice, double discount, double factor1, double factor2)
        {
            this.Tax = tax;
            this.SellPrice = sellPrice;
            this.Discount = discount;
            this.Factor1 = factor1;
            this.Factor2 = factor2;
        }

        public double GetValue()
        {
            double taxFactor = (100 + Tax) / 100;
            var buyprice = SellPrice / taxFactor * ((100 - Discount) / 100);
            buyprice = buyprice - (buyprice * Factor1);
            buyprice = buyprice - (buyprice * Factor2);

            return buyprice;
        }
    }
}

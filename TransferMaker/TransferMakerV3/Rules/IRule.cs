﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Rules
{
    public interface IRule
    {
        bool HasPassed();
    }
}

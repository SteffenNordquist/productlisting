﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Rules.SizeRules
{
    public class GramsRule : IRule
    {
        private int weight;

        public GramsRule(int weight)
        {
            this.weight = weight;
        }

        public bool HasPassed()
        {
            if (weight < 30000)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Rules.SizeRules
{
    public class SizeRule : IRule
    {
        private List<IRule> rules;

        public SizeRule(IProduct product)
        {
            List<IRule> rules = new List<IRule>
            {
                new GramsRule((int)product.getWeight()),

                new MilliMeterRule(
                    new List<int>
                    {
                        product.getLength(),
                        product.getWidth(),
                        product.getHeight()
                    }
                )
            };

            this.rules = rules;
        }

        public bool HasPassed()
        {
            if (rules.FindAll(x => !x.HasPassed()).Count() == 0)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Rules.SizeRules
{
    public class MilliMeterRule : IRule
    {
        private List<int> sizes;

        public MilliMeterRule(List<int> sizes)
        {
            this.sizes = sizes;
        }

        public bool HasPassed()
        {
            if (sizes.Max() < 1000)
            {
                return true;
            }

            return false;
        }
    }
}

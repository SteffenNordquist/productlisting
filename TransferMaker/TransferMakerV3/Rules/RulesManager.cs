﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Rules
{
    public class RulesManager : IRule
    {

        private List<IRule> rules;

        public RulesManager()
        {
            rules = new List<IRule>();
        }

        public void AddRule(IRule rule)
        {
            rules.Add(rule);
        }

        public bool HasPassed()
        {
            if (rules.FindAll(x => !x.HasPassed()).Count() == 0)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Blocking;

namespace TransferMakerV3.Rules.BlockingRules
{
    public class SkuBlockingRule : IRule
    {
        private HashSet<string> skuToBlock;
        private string sku;

        public SkuBlockingRule(HashSet<string> skuToBlock, string sku)
        {
            this.skuToBlock = skuToBlock;

            string[] skuChunks = sku.Split('-');

            //removing multiplier
            if (skuChunks.Count() > 3)
            {
                string newSku = String.Join("-", skuChunks.Take(skuChunks.Length - 1));
                this.sku = newSku;
            }
            else
            {
                this.sku = sku;
            }

        }

        public bool HasPassed()
        {
            var findBlockedSku = skuToBlock.Where(x => x.Contains(sku)).Select(x => x);
            if (findBlockedSku.Count() == 0)
            {
                return true;
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using ProcuctDB;
using DN_Classes.Products.Price;
using TransferMaker.Core.ProductSize;
using TransferMaker.Core.Factories.SupplierNameFactory;

namespace TransferMakerV3.TransferLines
{
    public static class STransferLine
    {
        private static IDefaultProductSize productSize = new SupplierDefaultProductSize(new DefaultSupplierNameFactory());

        public static string GetProductLine(IProduct product, string sku2, string ean2, double price2, int multiplier=1, int stock = 0)
        {
            if (stock == 0)
            {
                stock = product.getStock();
            }
            
            string ean = "none";
            if (ean2 != null) { ean = ean2; }
            else { if (product.GetEanList().Count() > 0) { ean = product.GetEanList()[0]; } }

            
            string sku = "";
            if (sku2 != null) { sku = sku2; }
            else { sku = product.getSKU(); }

            double price = 0;
            if (price2 != 0) { price = price2; }
            else { price = product.getPrice(); }

            bool include = true;

            if (price == 0)
            {
                include = false;
            }

            if (include)
            {
                string priceType = "04";
                if (product.getPriceType() == PriceType.flexiblePrice)
                {
                    priceType = "02";
                }

                string length = product.getLength().ToString()
                    , width = product.getWidth().ToString()
                    , height = product.getHeight().ToString()
                    , weight = Convert.ToInt32(product.getWeight()).ToString();

                productSize.ProcessProductSizes(product.getSupplierName());

                if (length == "0") { length = productSize.Length.ToString(); }
                if (width == "0") { width = productSize.Width.ToString(); ;}
                if (height == "0") { height = productSize.Height.ToString(); ;}
                if (weight == "0") { weight = int.Parse(productSize.Weight.ToString()).ToString();}

                if (length == "0") { length = ""; }
                if (width == "0") { width = ""; }
                if (height == "0") { height = ""; }
                if (weight == "0") { weight = ""; }

                double priceMultiplier = Math.Round(price * multiplier, 2);// : Math.Round(Convert.ToDouble(price) * 1, 2);

                string line = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
                    sku, ean, stock, product.getCurrency(), priceMultiplier.ToString(CultureInfo.InvariantCulture), product.getDiscount().ToString(CultureInfo.InvariantCulture), product.getTax(), string.Empty, length, width, height, weight, priceType, product.GetDaysToDelivery(), product.GetPackingUnit());

                return line;
            }
            else return null;
        }


        public static string getHeaderLine()
        {
            string line = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
                              "SKU", "ProductIdentifier", "Stock", "Currency", "Price", "Discount", "Tax", "Empty", "Length", "Width", "Height", "Weight", "PriceType", "DaysToDelivery", "VE");

            return line; 
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using ProcuctDB;

namespace TransferMakerV3.TransferLines
{
    public class JtlTransferLine
    {
        public string GetProductLine(string transferLine, double discountedBuyPrice, string title, string ean, int weight)
        {
           discountedBuyPrice = Math.Round(discountedBuyPrice, 2);
           return string.Format("{0}\t{1}\t{2}\t{3}\t{4}", transferLine.Replace(".", ","), discountedBuyPrice.ToString(CultureInfo.GetCultureInfo("de-DE")), title, ean, Math.Round((double)weight / 1000, 2).ToString(CultureInfo.GetCultureInfo("de-DE")));
        }

        public string GetProductLine(string transferLine, double discountedBuyPrice, IProduct product)
        {
            string title = "";
            if (product.getTitle().Count() > 0) { title = product.getTitle()[0]; }

            string ean = "";
            if (product.GetEanList().Count() > 0) { ean = product.GetEanList()[0]; }

            double weight = product.getWeight();

            discountedBuyPrice = Math.Round(discountedBuyPrice, 2);
            return string.Format("{0}\t{1}\t{2}\t{3}\t{4}", transferLine.Replace(".", ","), discountedBuyPrice.ToString(CultureInfo.GetCultureInfo("de-DE")), title, ean, Math.Round((double)weight / 1000, 2).ToString(CultureInfo.GetCultureInfo("de-DE")));
        }

        public string GetHeaderLine(string transferLineHeader)
        {
            return string.Format("{0}\t{1}\t{2}\t{3}\t{4}", transferLineHeader, "discountedBuyPrice", "Title", "EAN", "WeightInKG");
        }
    }
}

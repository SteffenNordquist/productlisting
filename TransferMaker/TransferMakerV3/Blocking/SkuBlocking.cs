﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Constructors;

namespace TransferMakerV3.Blocking
{
    public class SkuBlocking
    {
        public HashSet<string> skuToBlock = null;

        private ISkuToBeBlockedConstructor jtlSkuToBeBlockedConstructor = new JTLSkuToBeBlockedConstructor();
        private ISkuToBeBlockedConstructor wur2SkuToBeBlockedCOnstructor = new Wur2SkuToBeBlockedConstructor();

        public SkuBlocking()
        {
            if (skuToBlock == null)
            {
                skuToBlock = new HashSet<string>(); 
                MongoDatabase SkusDatabase = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/SkuBlacklist").GetServer().GetDatabase("SkuBlacklist");
                MongoCollection<Sku> skus = SkusDatabase.GetCollection<Sku>("skus");

                var allSKU = skus.FindAll();

                foreach(var sku in allSKU)
                {
                    if (!skuToBlock.Contains(sku.sku.Trim()))
                    {
                        skuToBlock.Add(sku.sku.Trim()); 
                    }

                    string jtlSku = jtlSkuToBeBlockedConstructor.GetSKU(sku.sku);
                    if (!skuToBlock.Contains(jtlSku.Trim()))
                    {
                        skuToBlock.Add(jtlSku.Trim());
                    }

                    string wur2Sku = wur2SkuToBeBlockedCOnstructor.GetSKU(sku.sku);
                    if (!skuToBlock.Contains(wur2Sku.Trim()) && wur2Sku != "")
                    {
                        skuToBlock.Add(wur2Sku.Trim());
                    }

                }


            }
        }
    }
}

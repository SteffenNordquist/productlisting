﻿using DN_Classes.AppStatus;
using DN_Classes.Supplier;
using ProcuctDB.DatabaseFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Interfaces;

namespace TransferMakerV3.Implementations
{
    public class TransfersGenerator : IGenerator
    {

        private List<ITransfer> SupplierListTransfer = new List<ITransfer>();

        public TransfersGenerator(List<ITransfer> supplierListTransfer)
        {
            this.SupplierListTransfer = supplierListTransfer;
        }

        public void Generate()
        {
            //ParallelOptions parallerOptions = new ParallelOptions { MaxDegreeOfParallelism = 3 };
            foreach (ITransfer transfer in SupplierListTransfer.OrderBy(x => x.GetActivation()))
            //Parallel.ForEach(SupplierListTransfer.OrderBy(x => x.GetActivation()), transfer =>
            {
                Console.WriteLine("Start: " + transfer.GetTransferName() + " " + DateTime.Now.ToString("HH:mm:ss"));

                using (ApplicationStatus appStatus = new ApplicationStatus("TransferMaker-" + transfer.GetTransferName() + "-V3"))
                {
                    try
                    {
                        transfer.DoTransfer();

                        if (transfer.ErrorLogs() == "")
                        {
                            appStatus.AddMessagLine("Succesful transfer");
                            appStatus.Successful();
                        }
                        else
                        {
                            appStatus.AddMessagLine(transfer.ErrorLogs());
                        }

                        Console.WriteLine("End: " + transfer.GetTransferName() + " " + DateTime.Now.ToString("HH:mm:ss"));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        appStatus.AddMessagLine(ex.Message + " StackTrace:" + ex.StackTrace.ToString());

                        Console.WriteLine("Error: " + transfer.GetTransferName());

                    }
                }
            }
            //);

        }


        

        
    }
}

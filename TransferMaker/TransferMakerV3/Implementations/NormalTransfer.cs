﻿using ProcuctDB;
using ProcuctDB.DiscountFactors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Blocking;
using TransferMakerV3.Interfaces;
using TransferMakerV3.InclusionRules;
using TransferMakerV3.InclusionRules.BlockingRules;
using TransferMakerV3.InclusionRules.SizeRules;
using TransferMakerV3.TransferLines;
using DN_Classes.OrderLimiter;
using TransferMakerV3.DiscountSource;

namespace TransferMakerV3.Implementations
{
    public class NormalTransfer : ITransfer
    {

        private List<string> transferLines = new List<string>();
        private List<string> jtlTransferLines = new List<string>();
        private IAmazonTransfer database = null;
        public string SupplierName;
        public bool Activated = true;
        public List<double> discountFactors;

        private string errorLog = "";

        public void SetDatabase<T>(T database)
        {
            this.database = (IAmazonTransfer)database;
            this.SupplierName = database.GetType().Name.ToLower().Replace("db", "");

            //BuyPriceDiscountFactors discountFactors = new BuyPriceDiscountFactors();
            //List<double> factors = discountFactors.GetDiscountFactors(database.GetType().Name);
            List<double> factors = DiscountSourceFactory.GetDiscountSource().GetDiscountFactors<string>(database.GetType().Name).ToList();
            this.discountFactors = factors;
        }

        public void DoTransfer()
        {
            if (Activated)
            {

                SalesLimiter saleslimiter = new SalesLimiter();
                List<string> blockedEans = saleslimiter.BlockedList(this.SupplierName);

                File.WriteAllLines("C:\\temp\\transferlogs\\" + this.SupplierName + "_normal.txt", blockedEans);

                //TransferLine headerTransferLine = new TransferLine();
                //JtlTransferLine headerJtlTransferLine = new JtlTransferLine();
                jtlTransferLines.Add(SJtlTransferLine.GetHeaderLine(STransferLine.getHeaderLine()));

                RulesManager rules = new RulesManager();
                rules.AddRule(new SkuBlockingRule());
                rules.AddRule(new SizeRule());
                rules.AddRule(new LimiterRules(blockedEans));

                var sync = new object();

                ParallelOptions parallelOptions = new ParallelOptions();
                parallelOptions.MaxDegreeOfParallelism = 1;

                int skipped = 0;
                bool hasItems = true;
                while (hasItems) 
                {
                    hasItems = false;
                    int amount = 100000;
                   
                    //foreach(var document in database.TransferItems(skipped, amount))
                    //foreach (IProduct product in database.GetTransferMakerReady(skipped, amount).Where(x => x.GetEanList().Count() > 0))
                    //Parallel.ForEach(database.GetTransferMakerReady(skipped, amount).Where(x => x.GetEanList().Count() > 0), parallelOptions, product =>
                    Parallel.ForEach(database.TransferItems(skipped, amount), parallelOptions, document =>
                    {
                        lock (sync)
                        {
                            try
                            {
                                IProduct product = database.Serialize(document);

                                if (product.GetEanList() != null && product.GetEanList().Count() > 0)
                                {

                                    Console.WriteLine(SupplierName + " transfer : " + product.getSKU());

                                    if (rules.HasPassed<IProduct, string>(product, product.getSKU()))
                                    {
                                        //TransferLine transferLine = new TransferLine();
                                        string line = STransferLine.GetProductLine(product, null, null, 0);

                                        if (line != null)
                                        {
                                            transferLines.Add(line);

                                            DiscountedBuyPrice discountedBuyPrice = new DiscountedBuyPrice((double)product.getTax(), (double)product.getPrice(), (double)product.getDiscount(), discountFactors[0], discountFactors[1]);

                                            //JtlTransferLine jtlTransferLine = new JtlTransferLine();
                                            jtlTransferLines.Add(SJtlTransferLine.GetProductLine(line, Math.Round(discountedBuyPrice.GetValue(), 2), product));

                                        }
                                    }
                                }
                            }
                            catch (Exception ex) { errorLog = ex.Message + "\n " + ex.StackTrace; }
                        }

                        hasItems = true;
                    }
                    );

                    skipped += amount;
                    Console.WriteLine("Number of lines processed : " + skipped);
                }
                //);

                
            }

            string filename = String.Format(Properties.Settings.Default.TransferFilename, SupplierName);
            string jtlfilename = String.Format(Properties.Settings.Default.JTLTransferFilename, SupplierName);

            File.WriteAllLines(filename, transferLines);
            File.WriteAllLines(jtlfilename, jtlTransferLines);
        }

        public bool GetActivation()
        {
            return this.Activated;
        }

        public void SetActivation(bool activation)
        {
            this.Activated = activation;
        }

        public string GetTransferName()
        {
            return this.SupplierName;
        }


        public string ErrorLogs()
        {
            return errorLog;
        }
    }
}

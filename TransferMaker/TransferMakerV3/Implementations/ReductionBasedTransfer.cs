﻿using DN_Classes.OrderLimiter;
using ProcuctDB;
using ProcuctDB.DiscountFactors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Blocking;
using TransferMakerV3.DiscountSource;
using TransferMakerV3.InclusionRules;
using TransferMakerV3.InclusionRules.BlockingRules;
using TransferMakerV3.InclusionRules.SizeRules;
using TransferMakerV3.Interfaces;
using TransferMakerV3.ReductionRules;
using TransferMakerV3.TransferLines;

namespace TransferMakerV3.Implementations
{
    class ReductionBasedTransfer : ITransfer
    {
        private List<string> transferLines = new List<string>();
        private List<string> jtlTransferLines = new List<string>();
        private IDatabase database = null;
        public string SupplierName;
        public bool Activated = true;
        public List<double> discountFactors;

        public void SetDatabase<T>(T database)
        {
            this.database = (IDatabase)database;
            this.SupplierName = database.GetType().Name.ToLower().Replace("db", "");


            //BuyPriceDiscountFactors discountFactors = new BuyPriceDiscountFactors();
            //List<double> factors = discountFactors.GetDiscountFactors(database.GetType().Name);
            List<double> factors = DiscountSourceFactory.GetDiscountSource().GetDiscountFactors<string>(database.GetType().Name).ToList();
            this.discountFactors = factors;
        }

        public void DoTransfer()
        {
            if (Activated)
            {
                SalesLimiter saleslimiter = new SalesLimiter();
                List<string> blockedEans = saleslimiter.BlockedList(this.SupplierName);

                File.WriteAllLines(this.SupplierName + "_reduction.txt", blockedEans);

                TransferLine headerTransferLine = new TransferLine();
                JtlTransferLine headerJtlTransferLine = new JtlTransferLine();
                jtlTransferLines.Add(headerJtlTransferLine.GetHeaderLine(headerTransferLine.getHeaderLine()));

                RulesManager rules = new RulesManager();
                if (!SupplierName.ToLower().Contains("jpc"))
                {
                    rules.AddRule(new SizeRule());
                }
                rules.AddRule(new SkuBlockingRule());
                rules.AddRule(new LimiterRules(blockedEans));

                foreach (IProduct product in database.GetTransferMakerReady(0, 0))
                {

                    IStockDeductionRule deductionRule = new SoldAndUnshipStockDeductionRule();
                    int finalStock = deductionRule.GetFinalStock(product);

                    if (finalStock < 1) { continue; }

                    foreach (string sku in product.GetSKUList())
                    {
                        Console.WriteLine(SupplierName + " transfer : " + sku);

                        if (rules.HasPassed<IProduct, string>(product, sku))
                        {
                            string asin = null;
                            asin = sku.Split('-')[1];

                            Multiplier multiplier = new Multiplier(sku);

                            //TransferLine transferLine = new TransferLine();
                            string line = STransferLine.GetProductLine(product, sku, asin, 0, multiplier.GetMultiplier(), finalStock);

                            if (line != null)
                            {
                                transferLines.Add(line);

                                DiscountedBuyPrice discountedBuyPrice = new DiscountedBuyPrice((double)product.getTax(), (double)product.getPrice(), (double)product.getDiscount(), discountFactors[0], discountFactors[1]);
                                //JtlTransferLine jtlTransferLine = new JtlTransferLine();
                                jtlTransferLines.Add(SJtlTransferLine.GetProductLine(line, Math.Round(discountedBuyPrice.GetValue(), 2), product));
                            }
                        }

                    }


                }
            }

            string filename = String.Format(Properties.Settings.Default.TransferFilename, SupplierName);
            string jtlfilename = String.Format(Properties.Settings.Default.JTLTransferFilename, SupplierName);

            File.WriteAllLines(filename, transferLines);
            File.WriteAllLines(jtlfilename, jtlTransferLines);
        }

        public bool GetActivation()
        {
            return this.Activated;
        }

        public void SetActivation(bool activation)
        {
            this.Activated = activation;
        }

        public string GetTransferName()
        {
            return this.SupplierName;
        }


        public string ErrorLogs()
        {
            return "";
        }
    }
}

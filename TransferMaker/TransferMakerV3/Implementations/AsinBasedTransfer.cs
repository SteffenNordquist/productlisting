﻿using ProcuctDB;
using ProcuctDB.DiscountFactors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Blocking;
using TransferMakerV3.Interfaces;
using TransferMakerV3.InclusionRules;
using TransferMakerV3.InclusionRules.BlockingRules;
using TransferMakerV3.InclusionRules.SizeRules;
using TransferMakerV3.TransferLines;
using DN_Classes.OrderLimiter;
using TransferMakerV3.DiscountSource;
using MongoDB.Bson;
using DN_Classes.Supplier;
using TransferMaker.Core.Suppliers;

namespace TransferMakerV3.Implementations
{

    public class AsinBasedTransfer : ITransfer
    {
        private List<string> transferLines = new List<string>();
        private List<string> jtlTransferLines = new List<string>();
        private IDatabase database = null;
        public string SupplierName = "";
        public bool Activated = true;
        public List<double> discountFactors;

        private string errorLog = "";

        public void SetDatabase<T>(T database)
        {
            this.database = (IDatabase)database;
            //this.SupplierName = database.GetType().Name.ToLower().Replace("db", "");

            try {
                var supplier = SupplierSourceFactory.GetSuppliersSource().GetSupplier<SupplierEntity>(database.GetType().Name.ToLower().Replace("db", ""));
                this.SupplierName = supplier._id;
            }
            catch { }

            List<double> factors = DiscountSourceFactory.GetDiscountSource().GetDiscountFactors<string>(database.GetType().Name).ToList();
            this.discountFactors = factors;
        }

        public void DoTransfer()
        {
            if (Activated)
            {
                SalesLimiter saleslimiter = new SalesLimiter();
                List<string> blockedEans = saleslimiter.BlockedList(this.SupplierName);

                File.WriteAllLines("C:\\temp\\transferlogs\\" + this.SupplierName + ".txt", blockedEans);

                TransferLine headerTransferLine = new TransferLine();
                JtlTransferLine headerJtlTransferLine = new JtlTransferLine();
                jtlTransferLines.Add(headerJtlTransferLine.GetHeaderLine(headerTransferLine.getHeaderLine()));

                RulesManager rules = new RulesManager();
                if (!SupplierName.ToLower().Contains("jpc"))
                {
                    rules.AddRule(new SizeRule());
                }
                rules.AddRule(new SkuBlockingRule());
                rules.AddRule(new LimiterRules(blockedEans));

                var sync = new object();
                var sync2 = new object();

                //foreach (IProduct product in database.GetTransferMakerReady(0,0))
                ParallelOptions parallelOptions = new ParallelOptions{MaxDegreeOfParallelism = 8};
                Parallel.ForEach(database.GetTransferMakerReady(0, 0), parallelOptions, product =>
                {
                    lock (sync)
                    {
                        //foreach (string sku in product.GetSKUList())
                        Parallel.ForEach(product.GetSKUList(), parallelOptions, sku =>
                        {
                            lock (sync2)
                            {
                                try
                                {
                                    Console.WriteLine(SupplierName + " transfer : " + sku);

                                    if (rules.HasPassed<IProduct, string>(product, sku))
                                    {
                                        string asin = null;
                                        asin = sku.Split('-')[1];

                                        Multiplier multiplier = new Multiplier(sku);

                                        //TransferLine transferLine = new TransferLine();
                                        string line = STransferLine.GetProductLine(product, sku, asin, 0, multiplier.GetMultiplier());

                                        if (line != null)
                                        {
                                            transferLines.Add(line);

                                            DiscountedBuyPrice discountedBuyPrice = new DiscountedBuyPrice(
                                                (double)product.getTax(), 
                                                (double)product.getPrice(), 
                                                (double)product.getDiscount(), 
                                                discountFactors[0], 
                                                discountFactors[1]);

                                            //JtlTransferLine jtlTransferLine = new JtlTransferLine();
                                            jtlTransferLines.Add(SJtlTransferLine.GetProductLine(line, Math.Round(discountedBuyPrice.GetValue(), 2), product));
                                        }
                                    }
                                }
                                catch (Exception ex) { errorLog = ex.Message + "\n" + ex.StackTrace; }
                            }
                        }
                        );

                    }


                }
                );
            }

            string filename = String.Format(Properties.Settings.Default.TransferFilename, SupplierName);
            string jtlfilename = String.Format(Properties.Settings.Default.JTLTransferFilename, SupplierName);

            File.WriteAllLines(filename, transferLines);
            File.WriteAllLines(jtlfilename, jtlTransferLines);
        }

        public bool GetActivation()
        {
            return this.Activated;
        }

        public void SetActivation(bool activation)
        {
            this.Activated = activation;
        }

        public string GetTransferName()
        {
            return this.SupplierName;
        }




        public string ErrorLogs()
        {
            return errorLog;
        }
    }
}

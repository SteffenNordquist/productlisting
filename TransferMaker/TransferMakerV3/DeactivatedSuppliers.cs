﻿using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3
{
    public class DeactivatedSuppliers
    {
        public List<SupplierEntity> GetList(string connectionString)
        {
            var SupplierCollection = new MongoClient(connectionString.Replace("@db", "Supplier"))
                .GetServer()
                .GetDatabase("Supplier")
                .GetCollection<SupplierEntity>("supplier");

            var query = Query.EQ("active", false);
            return SupplierCollection.Find(query).SetFields("_id").ToList();

        }


    }
}

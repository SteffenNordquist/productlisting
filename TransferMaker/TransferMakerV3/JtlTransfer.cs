﻿
using DN_Classes.Queries;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMakerV3.Suppliers;

namespace TransferMakerV3
{
    public class JtlTransfer
    {
        public string errorLog = "";

        public void DoTransfer()
        {
            #region for filtered jtl transfer file

            BKSuppliers bkSuppliers = new BKSuppliers();
            List<string> supplierNames = bkSuppliers.SupplierCollection.FindAll().SetFields("_id").Select(x => x._id).ToList();
            Console.WriteLine("suppliers : " + supplierNames.Count());

            List<string> soldEans = new List<string>();
            #region getting sold eans
            var soldOrders = WarehouseDBCollection.JtlOrdersCollection.Find(Query.Exists("plus.orderDetails.Items")).SetFields("plus.orderDetails.Items.Ean");
            soldOrders.ToList().ForEach(x =>
            {
                x.plus["orderDetails"]["Items"].AsBsonArray.ToList().ForEach(y =>
                {
                    if (y.AsBsonDocument.Contains("Ean")) { soldEans.Add(y["Ean"].AsString); }
                });
            });
            #endregion
            Console.WriteLine("sold eans : " + soldEans.Count());

            object _lock = new object();

            StringBuilder mainWriter = new StringBuilder();
            string header = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}",
                              "Supplier", "SKU", "ProductIdentifier", "Stock", "Currency", "Price", "Discount", "Tax", "Empty", "Length", "Width", "Height", "Weight", "PriceType", "DaysToDelivery", "VE", "discountedBuyPrice", "Title", "EAN", "WeightInKG");
            mainWriter.AppendLine(header);

            Parallel.ForEach(supplierNames, new ParallelOptions { MaxDegreeOfParallelism = 8 }, supplierName =>
            {
                
                    try
                    {
                        Console.WriteLine(supplierName);

                        string jtlfilename = String.Format(Properties.Settings.Default.JTLTransferFilename, supplierName);

                        if (File.Exists(jtlfilename))
                        {
                            List<string> jtlLines = File.ReadAllLines(jtlfilename).AsParallel().Where(x => x != "" && x.Split('\t').Count() > 17 && soldEans.Contains(x.Split('\t')[17]))
                                        .Select(x => supplierName + "\t" + x).ToList();
                            lock (_lock)
                            {
                                jtlLines.ForEach(x => mainWriter.AppendLine(x));
                            }
                        }
                    }
                    catch (Exception ex) { errorLog = ex.Message + "\n" + ex.StackTrace; }
                
            });

            string filteredjtlfilename = String.Format(Properties.Settings.Default.JTLTransferFilename, "filtered");
            File.WriteAllText(filteredjtlfilename, mainWriter.ToString());
            //File.WriteAllLines(filteredjtlfilename, mainList);
            #endregion
        }
    }
}

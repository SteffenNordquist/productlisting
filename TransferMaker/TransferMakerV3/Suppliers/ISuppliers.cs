﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Suppliers
{
    public interface ISuppliers
    {
        TReturn GetSupplier<TReturn>(string supplierName);
    }
}

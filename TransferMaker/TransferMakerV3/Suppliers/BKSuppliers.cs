﻿using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Suppliers
{
    public class BKSuppliers : ISuppliers
    {
        public  readonly MongoCollection<SupplierEntity> SupplierCollection;

        public BKSuppliers()
        {
            string con_string = "mongodb://client148:client148devnetworks@136.243.44.111/@db";

            SupplierCollection = new MongoClient(con_string.Replace("@db", "Supplier"))
                .GetServer()
                .GetDatabase("Supplier")
                .GetCollection<SupplierEntity>("supplier");
        }

        public TReturn GetSupplier<TReturn>(string supplierName)
        {
            //flotten supplier special due to bad naming convention
            //missing '24'
            if (supplierName.ToLower().Contains("flotten"))
            {
                supplierName = "FLOTTEN24";
            }

            //3m supplier special due to bad naming convention
            //missing '_' in supplier collection
            if (supplierName.ToLower().Contains("3m"))
            {
                supplierName = "3m";
            }

            var supplier = SupplierCollection.FindOne(Query.EQ("_id", supplierName.ToUpper().Trim()));

            if (supplier == null)
            {
                supplier = SupplierCollection.FindOne(Query.EQ("_id", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(supplierName.Trim())));
            }

            return (TReturn)(object)supplier;
        }
    }
}

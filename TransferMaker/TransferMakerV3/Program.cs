﻿using DN_Classes.AppStatus;
using DN_Classes.Queries;
using DN_Classes.Supplier;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using ProcuctDB;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.Sales;
using TransferMaker.Core.Transfers.TransfersFactory;
using TransferMakerV3.Implementations;
using TransferMakerV3.Interfaces;
using TransferMakerV3.ReductionRules;
using TransferMakerV3.TransferFactory;
using TransferMakerV3.TransferLines;

namespace TransferMakerV3
{
    class Program
    {
        private static readonly string con_string_148 = "mongodb://client148:client148devnetworks@136.243.44.111/@db";

        static void Main(string[] args)
        {

            StartupConfiguration startupConfiguration = new StartupConfiguration(new UnityContainer());
            //ITransferListFactory transferListFactory = startupConfiguration.Resolve<ITransferListFactory>();
            //var transfers = transferListFactory.GetTransfers();

            List<ITransfer> SupplierListTransfer = Transfers.GetTransfers();

            DeactivatedSuppliers deactivatedSuppliers = new DeactivatedSuppliers();

            SupplierListTransfer = TransfersFilter.CheckActivation(SupplierListTransfer, deactivatedSuppliers.GetList(con_string_148));

            IGenerator transferGenerator = new TransfersGenerator(SupplierListTransfer);
            transferGenerator.Generate();

            using (ApplicationStatus appStatus = new ApplicationStatus("TransferMaker_JtlFilteredTransfer"))
            {
                try
                {
                    ISalesTransfer salesTransfer = startupConfiguration.Resolve<ISalesTransfer>();
                    salesTransfer.DoTransfer();

                    if (salesTransfer.ErrorLog == "")
                    {
                        appStatus.Successful();
                        appStatus.AddMessagLine("success");
                    }
                    else { appStatus.AddMessagLine(salesTransfer.ErrorLog); }
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }
            }



            

        }
    }
}

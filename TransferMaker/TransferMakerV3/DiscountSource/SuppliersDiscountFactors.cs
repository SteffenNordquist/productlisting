﻿using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.DiscountSource
{
    public class SuppliersDiscountFactors : IDiscountFactors
    {

        private readonly MongoCollection<SupplierEntity> SupplierCollection;

        public SuppliersDiscountFactors()
        {
            string con_string = "mongodb://client148:client148devnetworks@136.243.44.111/@db";

            SupplierCollection = new MongoClient(con_string.Replace("@db", "Supplier"))
                .GetServer()
                .GetDatabase("Supplier")
                .GetCollection<SupplierEntity>("supplier");
        }
        
        public double[] GetDiscountFactors<T>(T indicator)
        {
            string supplierName = (string)(object)indicator;
            supplierName = supplierName.ToLower().Replace("db", "");

            var supplier = SupplierCollection.FindOne(Query.EQ("_id", supplierName.ToUpper().Trim()));

            if (supplier == null)
            {
                supplier = SupplierCollection.FindOne(Query.EQ("_id", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(supplierName.Trim())));
            }

            if (supplier == null)
            {
                return new double[] { 0, 0};
            }

            double discount = 0;

            if (supplier.contractCondition != null && supplier.contractCondition.Discount != null)
            {
                discount = supplier.contractCondition.Discount;
                if (discount != 0)
                {
                    discount = discount / 100;
                }
            }

            return new double[] { discount, 0};
        }
    }
}

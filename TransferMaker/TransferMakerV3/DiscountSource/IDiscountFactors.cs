﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.DiscountSource
{
    public interface IDiscountFactors
    {
        double[] GetDiscountFactors<T>(T indicator);
    }
}

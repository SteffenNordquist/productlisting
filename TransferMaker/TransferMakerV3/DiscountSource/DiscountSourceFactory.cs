﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.DiscountSource
{
    public class DiscountSourceFactory
    {
        public static IDiscountFactors GetDiscountSource()
        {
            return new SuppliersDiscountFactors();
        }

    }
}

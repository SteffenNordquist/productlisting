﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMakerV3.Interfaces
{
    public interface ITransfer
    {

        void SetDatabase<T>(T database);

        void DoTransfer();

        bool GetActivation();

        void SetActivation(bool activation);

        string GetTransferName();

        string ErrorLogs();
    }
}

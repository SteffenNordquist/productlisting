﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.BuyPriceDiscount
{
    public class DefaultBuyPriceDiscount : IBuyPriceDiscount
    {
        public double GetDiscountedBuyPrice(double tax, double price, double discount, double factor1=0, double factor2=0)
        {
            double taxFactor = (100 + tax) / 100;
            var buyprice = price / taxFactor * ((100 - discount) / 100);
            var factor1Calculated = buyprice * factor1;
            buyprice = buyprice - factor1Calculated;
            var factor2Calculated = buyprice * factor2;
            buyprice = buyprice - factor2Calculated;

            return Math.Round(buyprice,2);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.BuyPriceDiscount
{
    public interface IBuyPriceDiscount
    {
        double GetDiscountedBuyPrice(double tax, double price, double discount, double factor1=0, double factor2=0);
    }
}

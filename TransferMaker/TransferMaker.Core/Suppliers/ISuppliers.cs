﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using MongoDB.Driver;

namespace TransferMaker.Core.Suppliers
{
    public interface ISuppliers
    {
        MongoCollection<SupplierEntity> SupplierCollection { get; }

        TReturn GetSupplier<TReturn>(string supplierName);
    }
}

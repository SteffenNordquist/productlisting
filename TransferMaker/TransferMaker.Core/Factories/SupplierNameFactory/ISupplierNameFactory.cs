﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Factories.SupplierNameFactory
{
    public interface ISupplierNameFactory
    {
        string GetSupplierName(string rawName);
    }
}

﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Core.Factories.SupplierNameFactory
{
    public class DefaultSupplierNameFactory : ISupplierNameFactory
    {
        public string GetSupplierName(string rawName)
        {
            if (rawName == null)
            {
                throw new ArgumentNullException("'rawName' must be set");
            }

            //flotten supplier special due to bad naming convention
            //missing '24'
            if (rawName.ToLower().Contains("flotten"))
            {
                rawName = "FLOTTEN24";
            }

            //3m supplier special due to bad naming convention
            //missing '_' in supplier collection
            if (rawName.ToLower().Contains("3m"))
            {
                rawName = "3m";
            }

            var supplier = AppCollections.SuppliersCollection().DataAccess.Queries.FindByKey<BsonDocument>("_id", rawName.ToUpper().Trim());

            if (supplier == null)
            {
                supplier = AppCollections.SuppliersCollection().DataAccess.Queries.FindByKey<BsonDocument>("_id", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(rawName.Trim()));
            }

            if (supplier == null || !supplier.Contains("_id"))
            {
                return "SupplierNameNotFound_" + rawName;
            }

            return supplier["_id"].AsString;
        }
    }
}

﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Settings;

namespace TransferMaker.Core.Collections
{
    public static class AppCollections
    {
        private static MongoCollection<BsonDocument> suppliersCollection;

        /// <summary>
        /// Works on returning the 'supplier' collection
        /// </summary>
        /// <returns>Returns 'supplier' collection</returns>
        public static MongoCollection<BsonDocument> SuppliersCollection()
        {
            if (suppliersCollection == null)
            {
                throw new NullReferenceException("The 'supplier' collection must be set at the entry point of the application.");
            }

            return suppliersCollection;
        }

        /// <summary>
        /// Sets the 'supplier' collection
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public static void SetSuppliersCollection(ISettings settings)
        {
            suppliersCollection = new MongoClient(settings.GetValue("suppliersConnectionString") + settings.GetValue("suppliersDatabase"))
                .GetServer()
                .GetDatabase(settings.GetValue("suppliersDatabase"))
                .GetCollection<BsonDocument>(settings.GetValue("suppliersCollection"));
        }
    }
}

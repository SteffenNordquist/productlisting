﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Rules
{
    public class DefaultRulesManager : IRulesManager
    {
        private List<IRule> rules;

        public int RulesCount
        {
            get { return rules.Count; }
        }

        public DefaultRulesManager()
        {
            rules = new List<IRule>();
        }

        public void AddRule(IRule rule)
        {
            if (rule == null)
            {
                throw new ArgumentNullException("rule must be set");
            }

            rules.Add(rule);
        }

        public bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct
        {
            if (obj == null || s == null)
            {
                throw new ArgumentNullException("obj and s must be set");
            }

            return rules.All(x => x.HasPassed(obj, s));
        }

        public void Clear()
        {
            rules.Clear();
        }

        
    }
}

﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Rules.SizeRules
{
    public class MilliMeterRule : IRule
    {
        public bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj must be set");
            }

            List<int> sizes = new List<int>
                    {
                        obj.getLength(),
                        obj.getWidth(),
                        obj.getHeight()
                    };

            if (sizes.Max() < 1000)
            {
                return true;
            }

            return false;
        }

    }
}

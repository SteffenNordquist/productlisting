﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Rules.SizeRules
{
    public class SizeRule : IRule
    {
        private List<IRule> rules;

        public SizeRule()
        {
            this.rules = new List<IRule>
            {
                new GramsRule(),
                new MilliMeterRule()
            };
        }

        public bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct
        {
            if (obj == null || s == null)
            {
                throw new ArgumentNullException("obj and s must be set");
            }

            return rules.All(x => x.HasPassed(obj, s));
        }

    }
}

﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Rules
{
    public interface IRule
    {
        bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct;
    }
}

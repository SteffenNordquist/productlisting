﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TransferMaker.Core.Rules.LimiterRules
{
    public class SalesLimiterRules : IRule
    {
        private List<string> blockedList;

        public SalesLimiterRules(List<string> blockedList)
        {
            this.blockedList = blockedList;
        }

        public bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj must be set");
            }

            bool flag = true;
            string ean = new Regex(@"\d{13}").Match(obj.getSKU()).Value;
            foreach (string blockedEan in blockedList)
            {
                if (ean.Equals(blockedEan))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }





    }
}

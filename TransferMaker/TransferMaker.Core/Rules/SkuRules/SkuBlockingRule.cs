﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Rules.SkuRules;

namespace TransferMaker.Core.Rules.SkuRules
{
    public class SkuBlockingRule : SkuBlackList, IRule
    {
        public bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct
        {
            if (s == null)
            {
                throw new ArgumentNullException("s must be set");
            }

            string nsku = (string)(object)s;
            nsku = CleanupSKU(nsku);

            if (GetBlackList().Where(x => x.Contains(nsku)).Select(x => x).Count() == 0)
            {
                return true;
            }
            return false;
        }

        private string CleanupSKU(string sku)
        {
            string[] skuChunks = sku.Split('-');

            //removing multiplier
            if (skuChunks.Count() > 3)
            {
                string newSku = String.Join("-", skuChunks.Take(skuChunks.Length - 1));
                sku = newSku;
            }

            return sku;
        }
    }
}

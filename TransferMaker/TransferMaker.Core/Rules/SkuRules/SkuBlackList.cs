﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Constructors;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Core.Rules.SkuRules
{
    public abstract class SkuBlackList
    {
        private HashSet<string> skuBlackList = null;

        private ISkuToBeBlockedConstructor jtlSkuToBeBlockedConstructor = new JTLSkuToBeBlockedConstructor();
        private ISkuToBeBlockedConstructor wur2SkuToBeBlockedCOnstructor = new Wur2SkuToBeBlockedConstructor();

        public HashSet<string> GetBlackList()
        {
            if (skuBlackList == null)
            {
                skuBlackList = new HashSet<string>(); 

                string query = " { _id : {$exists:true} } ";
                var skuDocuments = AppCollections.SkuBlackListCollection().DataAccess.Queries.Find<BsonDocument>(query);

                foreach(var skuDocument in skuDocuments)
                {
                    if (!skuBlackList.Contains(skuDocument["sku"].ToString()))
                    {
                        skuBlackList.Add(skuDocument["sku"].ToString());
                    }

                    string jtlSku = jtlSkuToBeBlockedConstructor.GetSKU(skuDocument["sku"].ToString());
                    if (!skuBlackList.Contains(jtlSku.Trim()))
                    {
                        skuBlackList.Add(jtlSku.Trim());
                    }

                    string wur2Sku = wur2SkuToBeBlockedCOnstructor.GetSKU(skuDocument["sku"].ToString());
                    if (!skuBlackList.Contains(wur2Sku.Trim()) && wur2Sku != "")
                    {
                        skuBlackList.Add(wur2Sku.Trim());
                    }

                    
                }


            }

            return skuBlackList;
        }
    }
}

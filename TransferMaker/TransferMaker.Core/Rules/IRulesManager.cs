﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Rules
{
    public interface IRulesManager
    {
        int RulesCount { get; }

        void AddRule(IRule rule);

        bool HasPassed<T, SKU>(T obj, SKU s) where T : IProduct;

        void Clear();
    }
}

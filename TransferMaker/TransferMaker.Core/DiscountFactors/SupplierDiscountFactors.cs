﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Core.DiscountFactors
{
    public class SupplierDiscountFactors : IDiscountFactors
    {
        public double[] GetDiscountFactors<T>(T indicator)
        {
            if (indicator == null)
            {
                throw new ArgumentNullException("'indicator' must be set");
            }

            string supplierName = (string)(object)indicator;
            supplierName = supplierName.ToLower().Replace("db", "");

            var supplier = AppCollections.SuppliersCollection().DataAccess.Queries.FindByKey<BsonDocument>("_id", supplierName.ToUpper().Trim());

            if (supplier == null)
            {
                supplier = AppCollections.SuppliersCollection().DataAccess.Queries.FindByKey<BsonDocument>("_id", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(supplierName.Trim()));
            }

            if (supplier == null || !supplier.Contains("contractCondition") || !supplier["contractCondition"].AsBsonDocument.Contains("Discount"))
            {
                return new double[] { 0, 0 };
            }

            double discount = 0;

            if (supplier["contractCondition"] != null && supplier["contractCondition"]["Discount"] != null)
            {
                discount = double.Parse(supplier["contractCondition"]["Discount"].ToString());
                if (discount != 0)
                {
                    discount = discount / 100;
                }
            }

            return new double[] { discount, 0 };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;
using DN_Classes.Sales;
using TransferMaker.Core.Suppliers;

namespace TransferMaker.Core.Transfers.Sales
{
    public class JtlTransfersBasedSalesTransfer : ISalesTransfer
    {
        private readonly ISold _soldItems;

        private readonly ISuppliers _bkSuppliers;

        private readonly IConfigurationManager _configurationManager;

        public JtlTransfersBasedSalesTransfer(ISold soldItems, ISuppliers bkSuppliers, IConfigurationManager configurationManager)
        {
            _soldItems = soldItems;
            _bkSuppliers = bkSuppliers;
            ErrorLog = String.Empty;
            _configurationManager = configurationManager;
        }

        public string ErrorLog { get; set; }

        public void DoTransfer()
        {
            List<string> supplierNames = _bkSuppliers.SupplierCollection.FindAll()
                .SetFields("_id")
                .Where(x=> x._id != "SETS")
                .Select(x => x._id).ToList();

            List<string> soldEans = _soldItems.GetSoldItems().ToList();

            object _lock = new object();

            StringBuilder mainWriter = new StringBuilder();
            string header = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}",
                              "Supplier", "SKU", "ProductIdentifier", "Stock", "Currency", "Price", "Discount", "Tax", "Empty", "Length", "Width", "Height", "Weight", "PriceType", "DaysToDelivery", "VE", "discountedBuyPrice", "Title", "EAN", "WeightInKG");
            mainWriter.AppendLine(header);

            Parallel.ForEach(supplierNames, new ParallelOptions { MaxDegreeOfParallelism = 8 }, supplierName =>
            {

                try
                {
                    Console.WriteLine(supplierName);

                    string jtlfilename = String.Format(_configurationManager.GetValue("JTLTransferFilename"), supplierName);

                    if (File.Exists(jtlfilename))
                    {
                        List<string> jtlLines = File.ReadAllLines(jtlfilename).AsParallel().Where(x => x != "" && x.Split('\t').Count() > 17 && soldEans.Contains(x.Split('\t')[17]))
                                    .Select(x => supplierName + "\t" + x).ToList();
                        lock (_lock)
                        {
                            jtlLines.ForEach(x => mainWriter.AppendLine(x));
                        }
                    }
                }
                catch (Exception ex) { ErrorLog = ex.Message + "\n" + ex.StackTrace; }

            });

            string filteredjtlfilename = String.Format(_configurationManager.GetValue("JTLTransferFilename"), "filtered");
            File.WriteAllText(filteredjtlfilename, mainWriter.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.Sales
{
    public interface ISalesTransfer
    {
        string ErrorLog { get; }

        void DoTransfer();
    }
}

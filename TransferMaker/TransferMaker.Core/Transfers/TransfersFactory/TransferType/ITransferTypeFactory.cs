﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.TransfersFactory.TransferType
{
    public interface ITransferTypeFactory
    {
        ITransfer GetTransfer<T>(T obj);
    }
}

﻿using DN.DataAccess.ConfigurationData;
using DN_Classes.OrderLimiter;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Activation;
using TransferMaker.Core.BuyPriceDiscount;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.DiscountFactors;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Core.Multiplier;
using TransferMaker.Core.Rules;
using TransferMaker.Core.Transfers.Header;
using TransferMaker.Core.Transfers.Rows;

namespace TransferMaker.Core.Transfers.TransfersFactory.TransferType
{
    public class DefaultTransferTypeFactory : ITransferTypeFactory
    {
        private List<string> normalBasedSuppliers;
        private List<string> underscoreAsinBasedSuppliers;
        private List<string> reductionBasedSuppliers;

        private readonly ISupplierNameFactory supplierNameFactory;
        private readonly IDiscountFactors discountFactorsWorker;
        private readonly IConfigurationManager configurationManager;
        private readonly ISalesLimiter salesLimiter;
        private readonly IHeader transferHeader;
        private readonly IJtlHeader jtlTransferHeader;
        private readonly IRulesManager rulesManager;
        private readonly IMultiplier multiplierWorker;
        private readonly ITransferRow transferRowWorkder;
        private readonly IJtlTransferRow jtlTransferRowWorker;
        private readonly IBuyPriceDiscount buyPriceDiscountWorker;

        public DefaultTransferTypeFactory(
            IConfigurationManager configurationManager, 
            ISupplierNameFactory supplierNameFactory,
            IDiscountFactors discountFactorsWorker,
            ISalesLimiter salesLimiter,
            IHeader transferHeader,
            IJtlHeader jtlTransferHeader,
            IRulesManager rulesManager,
            IMultiplier multiplierWorker,
            ITransferRow transferRowWorkder,
            IJtlTransferRow jtlTransferRowWorker,
            IBuyPriceDiscount buyPriceDiscountWorker
            )
        {
            normalBasedSuppliers = configurationManager.GetValue("normalBasedSuppliers").Split(';').ToList();
            underscoreAsinBasedSuppliers = configurationManager.GetValue("underscoreAsinBasedSuppliers").Split(';').ToList();
            reductionBasedSuppliers = configurationManager.GetValue("reductionBasedSuppliers").Split(';').ToList();

            this.supplierNameFactory = supplierNameFactory;
            this.discountFactorsWorker = discountFactorsWorker;
            this.configurationManager = configurationManager;
            this.salesLimiter = salesLimiter;
            this.transferHeader = transferHeader;
            this.jtlTransferHeader = jtlTransferHeader;
            this.rulesManager = rulesManager;
            this.multiplierWorker = multiplierWorker;
            this.transferRowWorkder = transferRowWorkder;
            this.jtlTransferRowWorker = jtlTransferRowWorker;
            this.buyPriceDiscountWorker = buyPriceDiscountWorker;
        }

        public ITransfer GetTransfer<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("'obj' should not be null");
            }

            string supplierName = obj.GetType().Name.Replace("DB", "");

            if (normalBasedSuppliers.Exists(x => x == supplierName.ToLower()))
            {
                return new NormalTransfer((IAmazonTransfer)obj, new SupplierActivationChecker(AppCollections.SuppliersCollection().DataAccess.Queries));
            }
            else if (underscoreAsinBasedSuppliers.Exists(x => x == supplierName.ToLower()))
            {
                return new UnderscoreAsinBasedTransfer((IDatabase)obj, new SupplierActivationChecker(AppCollections.SuppliersCollection().DataAccess.Queries));
            }
            else if (reductionBasedSuppliers.Exists(x => x == supplierName.ToLower()))
            {
                return new ReductionBasedTransfer((IDatabase)obj, new SupplierActivationChecker(AppCollections.SuppliersCollection().DataAccess.Queries));
            }
            else
            {
                return new AsinBasedTransfer(
                    (IDatabase)obj, 
                    new SupplierActivationChecker(AppCollections.SuppliersCollection().DataAccess.Queries), 
                    supplierNameFactory,
                    discountFactorsWorker,
                    configurationManager,
                    salesLimiter,
                    transferHeader,
                    jtlTransferHeader,
                    rulesManager,
                    multiplierWorker,
                    transferRowWorkder,
                    jtlTransferRowWorker,
                    buyPriceDiscountWorker);
            }

        }
    }
}

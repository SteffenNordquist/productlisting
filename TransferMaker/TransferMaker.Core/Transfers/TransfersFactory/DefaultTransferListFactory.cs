﻿using DN.DataAccess.ConfigurationData;
using ProcuctDB;
using ProcuctDB.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.TransfersFactory.TransferType;

namespace TransferMaker.Core.Transfers.TransfersFactory
{
    public class DefaultTransferListFactory : ITransferListFactory
    {
        private readonly ITransferTypeFactory transferTypeFactory;
        private readonly IConfigurationManager configurationManager;

        public DefaultTransferListFactory(ITransferTypeFactory transferTypeFactory, IConfigurationManager configurationManager)
        {
            this.transferTypeFactory = transferTypeFactory;
            this.configurationManager = configurationManager;
        }

        public IEnumerable<ITransfer> GetTransfers()
        {
            bool TranferForJTL = false;
            Boolean.TryParse(configurationManager.GetValue("TransferForJTL"), out TranferForJTL);

            bool TransferForKNVLIBRI = false;
            Boolean.TryParse(configurationManager.GetValue("TransferForKNVLIBRI"), out TransferForKNVLIBRI);

            bool TransferForOtherSuppliers = false;
            Boolean.TryParse(configurationManager.GetValue("TransferForOTHERSUPPLIERS"), out TransferForOtherSuppliers);


            List<ITransfer> SupplierListTransfer = new List<ITransfer>();

            #region for JTL

            if (TranferForJTL == true)
            {
                IDatabase jtlDB = new JTLDB();
                ITransfer jtltransfer = transferTypeFactory.GetTransfer<IDatabase>(jtlDB);
                SupplierListTransfer.Add(jtltransfer);
            }
            #endregion


            #region for Knv and Libri

            if (TransferForKNVLIBRI == true)
            {
                IAmazonTransfer database = new KnvDB();
                ITransfer knvtransfer = transferTypeFactory.GetTransfer<IAmazonTransfer>(database);
                SupplierListTransfer.Add(knvtransfer);

                IAmazonTransfer libridatabase = new LibriDB();
                ITransfer libritransfer = transferTypeFactory.GetTransfer<IAmazonTransfer>(libridatabase);
                SupplierListTransfer.Add(libritransfer);
            }
            #endregion


            #region for the rest of other suppliers

            if (TransferForOtherSuppliers == true)
            {
                ProductFinder productFinder = new ProductFinder();
                foreach (IDatabase database in productFinder.getAllDatabases().Where(x =>
                    !x.GetType().Name.ToLower().Contains("libri") &&
                    !x.GetType().Name.ToLower().Contains("knv") &&
                    !x.GetType().Name.ToLower().Contains("jtl") &&
                    !x.GetType().Name.ToLower().Contains("bremer") &&
                    !x.GetType().Name.ToLower().Contains("bvw") &&
                    !x.GetType().Name.ToLower().Contains("hoffmann")
                    ))
                {
                    ITransfer transfer = transferTypeFactory.GetTransfer<IDatabase>(database);
                    SupplierListTransfer.Add(transfer);
                }
                IAmazonTransfer bvwdatabase = new BVWDB();
                ITransfer bvwtransfer = transferTypeFactory.GetTransfer<IAmazonTransfer>(bvwdatabase);
                IAmazonTransfer hoffmanndatabase = new HoffmannDB();
                ITransfer hoffmanntransfer = transferTypeFactory.GetTransfer<IAmazonTransfer>(hoffmanndatabase);

                SupplierListTransfer.Add(bvwtransfer);
                SupplierListTransfer.Add(hoffmanntransfer);
            }
            #endregion

            return SupplierListTransfer;
        }
    }
}

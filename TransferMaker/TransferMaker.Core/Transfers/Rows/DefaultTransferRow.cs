﻿using DN_Classes.Products.Price;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.ProductSize;

namespace TransferMaker.Core.Transfers.Rows
{
    public class DefaultTransferRow : ITransferRow
    {
        private readonly IDefaultProductSize defaultProductSize;

        private readonly IProductSizesCollection productSizesCollection;

        public DefaultTransferRow(IDefaultProductSize productSize, IProductSizesCollection productSizesCollection)
        {
            this.defaultProductSize = productSize;
            this.productSizesCollection = productSizesCollection;
        }

        public string GetProductLine(IProduct product, string setSku, string setEan, double setPrice, int multiplier = 1, int stock = 0)
        {
            if (product == null)
            {
                throw new ArgumentNullException("product must be set");
            }

            if (stock == 0)
            {
                stock = product.getStock();
            }

            string ean = "no ean";
            if (setEan != null && setEan != "") { ean = setEan; }
            else { if (product.GetEanList().Count() > 0) { ean = product.GetEanList()[0]; } }


            string sku = "";
            if (setSku != null && setSku != "") { sku = setSku; }
            else { sku = product.getSKU(); }

            double price = 0;
            if (setPrice != null && setPrice != 0) { price = setPrice; }
            else { price = product.getPrice(); }

            PriceType priceTypeEnum = product.getPriceType();
            
            string currency = product.getCurrency();
            string discount = product.getDiscount().ToString(CultureInfo.InvariantCulture);
            int tax = product.getTax();
            int daysToDelivery = product.GetDaysToDelivery();
            int packingUnit = product.GetPackingUnit();

            bool include = true;

            if (price == 0)
            {
                include = false;
            }

            if (include)
            {
                string priceType = "04";
                if (priceTypeEnum == PriceType.flexiblePrice)
                {
                    priceType = "02";
                }

                string length = product.getLength().ToString()
                    , width = product.getWidth().ToString()
                    , height = product.getHeight().ToString()
                    , weight = Convert.ToInt32(product.getWeight()).ToString();

                if (length == "0" && width == "0" && height == "0" && weight == "0")
                {
                    defaultProductSize.ProcessProductSizes(product.getSupplierName());

                    length = defaultProductSize.Length.ToString(); 
                    width = defaultProductSize.Width.ToString(); ;
                    height = defaultProductSize.Height.ToString(); 
                    weight = defaultProductSize.Weight.ToString(); 
                }

                if (length == "0" && width == "0" && height == "0" && weight == "0")
                {
                    productSizesCollection.ProcessProductSizes(ean);

                    length = productSizesCollection.Length.ToString();
                    width = productSizesCollection.Width.ToString(); ;
                    height = productSizesCollection.Height.ToString();
                    weight = productSizesCollection.Weight.ToString();
                }

                if (length == "0") { length = ""; }
                if (width == "0") { width = ""; }
                if (height == "0") { height = ""; }
                if (weight == "0") { weight = ""; }

                double priceMultiplier = Math.Round(price * multiplier, 2);// : Math.Round(Convert.ToDouble(price) * 1, 2);

                string line = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
                    sku, ean, stock, currency, priceMultiplier.ToString(CultureInfo.InvariantCulture), discount, tax, string.Empty, length, width, height, weight, priceType, daysToDelivery, packingUnit);

                return line;
            }
            else return null;
        }
    }
}

﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.Rows
{
    public interface ITransferRow
    {
        string GetProductLine(IProduct product, string setSku, string setEan, double setPrice, int multiplier = 1, int stock = 0);
    }
}

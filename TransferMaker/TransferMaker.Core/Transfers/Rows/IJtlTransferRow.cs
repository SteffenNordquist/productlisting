﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.Rows
{
    public interface IJtlTransferRow
    {
        string GetProductLine(string transferLine, double discountedBuyPrice, string title, string ean, int weight);

        string GetProductLine(string transferLine, double discountedBuyPrice, IProduct product);
    }
}

﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Activation;

namespace TransferMaker.Core.Transfers
{
    public class ReductionBasedTransfer : ITransfer
    {
        private readonly IDatabase database;

        private readonly IActivationChecker activationChecker;

        private readonly string supplierTransferName;

        private string errorLog;

        public ReductionBasedTransfer(IDatabase database, IActivationChecker activationChecker)
        {
            this.database = database;
        }

        public string SupplierTransferName
        {
            get { return supplierTransferName; }
        }

        public string ErrorLog
        {
            get { return errorLog; }
        }

        public void DoTransfer()
        {
            throw new NotImplementedException();
        }
    }
}

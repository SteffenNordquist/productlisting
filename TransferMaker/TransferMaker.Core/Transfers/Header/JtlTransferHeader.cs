﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.Header
{
    public class JtlTransferHeader : IJtlHeader
    {

        private readonly IHeader transferHeader;

        public JtlTransferHeader(IHeader transferHeader)
        {
            this.transferHeader = transferHeader;    
        }

        public string GetJtlHeader()
        {
            return string.Format("{0}\t{1}\t{2}\t{3}\t{4}", transferHeader.GetHeader(), "discountedBuyPrice", "Title", "EAN", "WeightInKG");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.Header
{
    public class TransferHeader : IHeader
    {
        public string GetHeader()
        {
            string line = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
                              "SKU", "ProductIdentifier", "Stock", "Currency", "Price", "Discount", "Tax", "Empty", "Length", "Width", "Height", "Weight", "PriceType", "DaysToDelivery", "VE");

            return line; 
        }
    }
}

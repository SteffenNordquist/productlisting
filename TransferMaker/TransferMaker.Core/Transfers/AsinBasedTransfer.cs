﻿using DN.DataAccess;
using DN.DataAccess.ConfigurationData;
using DN_Classes.OrderLimiter;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Activation;
using TransferMaker.Core.BuyPriceDiscount;
using TransferMaker.Core.DiscountFactors;
using TransferMaker.Core.Factories.SupplierNameFactory;
using TransferMaker.Core.Multiplier;
using TransferMaker.Core.Rules;
using TransferMaker.Core.Rules.LimiterRules;
using TransferMaker.Core.Rules.SizeRules;
using TransferMaker.Core.Rules.SkuRules;
using TransferMaker.Core.Transfers.Header;
using TransferMaker.Core.Transfers.Rows;

namespace TransferMaker.Core.Transfers
{
    public class AsinBasedTransfer : ITransfer
    {
        private readonly IDatabase database;

        private readonly IActivationChecker activationChecker;

        private readonly IConfigurationManager configurationManager;

        private readonly ISalesLimiter salesLimiter;

        private readonly IHeader transferHeader;

        private readonly IJtlHeader jtlTransferHeader;

        private readonly IRulesManager rulesManager;

        private readonly IMultiplier multiplierWorker;

        private readonly ITransferRow transferRowWorkder;

        private readonly IJtlTransferRow jtlTransferRowWorker;

        private readonly IBuyPriceDiscount buyPriceDiscountWorker;

        private readonly string supplierTransferName;

        private string errorLog;

        private List<double> discountFactors;

        private List<string> transferLines = new List<string>();
        private List<string> jtlTransferLines = new List<string>();

        public AsinBasedTransfer(
            IDatabase database, 
            IActivationChecker activationChecker,
            ISupplierNameFactory supplierNameFactory,
            IDiscountFactors discountFactorsWorker,
            IConfigurationManager configurationManager,
            ISalesLimiter salesLimiter,
            IHeader transferHeader,
            IJtlHeader jtlTransferHeader,
            IRulesManager rulesManager,
            IMultiplier multiplierWorker,
            ITransferRow transferRowWorkder,
            IJtlTransferRow jtlTransferRowWorker,
            IBuyPriceDiscount buyPriceDiscountWorker)
        {
            this.database = database;
            this.activationChecker = activationChecker;
            this.configurationManager = configurationManager;

            this.supplierTransferName = supplierNameFactory.GetSupplierName(database.GetType().Name.ToLower().Replace("db", ""));

            this.discountFactors = discountFactorsWorker.GetDiscountFactors<string>(database.GetType().Name.ToLower().Replace("db", "")).ToList();
            this.salesLimiter = salesLimiter;
            this.transferHeader = transferHeader;
            this.jtlTransferHeader = jtlTransferHeader;
            this.rulesManager = rulesManager;
            this.multiplierWorker = multiplierWorker;
            this.transferRowWorkder = transferRowWorkder;
            this.jtlTransferRowWorker = jtlTransferRowWorker;
            this.buyPriceDiscountWorker = buyPriceDiscountWorker;

            this.errorLog = "";
        }

        public string SupplierTransferName
        {
            get { return supplierTransferName; }
        }

        public string ErrorLog
        {
            get { return errorLog; }
        }

        public void DoTransfer()
        {
            if (activationChecker.IsActivated<string>(supplierTransferName))
            {
                List<string> blockedEans = salesLimiter.BlockedList(supplierTransferName);
                string supplierBlockedListFileName = String.Format(configurationManager.GetValue("SalesLimiterBlockedListFileName"), supplierTransferName);
                File.WriteAllLines(supplierBlockedListFileName, blockedEans);

                jtlTransferLines.Add(jtlTransferHeader.GetJtlHeader());

                if (!supplierTransferName.ToLower().Contains("jpc"))
                {
                    rulesManager.AddRule(new SizeRule());
                }
                rulesManager.AddRule(new SkuBlockingRule());
                rulesManager.AddRule(new SalesLimiterRules(blockedEans));

                var sync = new object();
                var sync2 = new object();

                //foreach (IProduct product in database.GetTransferMakerReady(0,0))
                ParallelOptions parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 8 };
                Parallel.ForEach(database.GetTransferMakerReady(0, 0), parallelOptions, product =>
                {
                    lock (sync)
                    {
                        //foreach (string sku in product.GetSKUList())
                        Parallel.ForEach(product.GetSKUList(), parallelOptions, sku =>
                        {
                            lock (sync2)
                            {
                                try
                                {
                                    Console.WriteLine(supplierTransferName + " transfer : " + sku);

                                    if (rulesManager.HasPassed<IProduct, string>(product, sku))
                                    {
                                        string asin = null;
                                        asin = sku.Split('-')[1];

                                        string line = transferRowWorkder.GetProductLine(product, sku, asin, 0, multiplierWorker.GetMultiplier(sku));

                                        if (line != null)
                                        {
                                            transferLines.Add(line);

                                            double discountedBuyPrice = buyPriceDiscountWorker.GetDiscountedBuyPrice(
                                                (double)product.getTax(),
                                                (double)product.getPrice(),
                                                (double)product.getDiscount(),
                                                discountFactors[0],
                                                discountFactors[1]
                                                );

                                            jtlTransferLines.Add(jtlTransferRowWorker.GetProductLine(line, Math.Round(discountedBuyPrice, 2), product));
                                        }
                                    }
                                }
                                catch (Exception ex) { errorLog = ex.Message + "\n" + ex.StackTrace; }
                            }
                        }
                        );

                    }


                }
                );
            }

            string filename = String.Format(configurationManager.GetValue("TransferFilename"), supplierTransferName);
            string jtlfilename = String.Format(configurationManager.GetValue("JTLTransferFilename"), supplierTransferName);

            File.WriteAllLines(filename, transferLines);
            File.WriteAllLines(jtlfilename, jtlTransferLines);
        }
    }
}

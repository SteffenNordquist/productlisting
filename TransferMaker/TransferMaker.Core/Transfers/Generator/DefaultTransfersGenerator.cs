﻿using DN_Classes.AppStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.Transfers.TransfersFactory;

namespace TransferMaker.Core.Transfers.Generator
{
    public class DefaultTransfersGenerator : ITransfersGenerator
    {
        private readonly ITransferListFactory transferListFactory;

        public DefaultTransfersGenerator(ITransferListFactory transferListFactory)
        {
            this.transferListFactory = transferListFactory;
        }

        public void Generate()
        {
            //ParallelOptions parallerOptions = new ParallelOptions { MaxDegreeOfParallelism = 3 };
            foreach (TransferMaker.Core.Transfers.ITransfer transfer in transferListFactory.GetTransfers().ToList())
            //Parallel.ForEach(transferListFactory.GetTransfers().ToList(), TransferMaker.Core.Transfers.ITransfer transfer =>
            {
                Console.WriteLine("Start: " + transfer.SupplierTransferName + " " + DateTime.Now.ToString("HH:mm:ss"));

                using (ApplicationStatus appStatus = new ApplicationStatus("TransferMaker-" + transfer.SupplierTransferName + "-V3"))
                {
                    try
                    {
                        transfer.DoTransfer();

                        if (transfer.ErrorLog == "")
                        {
                            appStatus.AddMessagLine("Succesful transfer");
                            appStatus.Successful();
                        }
                        else
                        {
                            appStatus.AddMessagLine(transfer.ErrorLog);
                        }

                        Console.WriteLine("End: " + transfer.SupplierTransferName + " " + DateTime.Now.ToString("HH:mm:ss"));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        appStatus.AddMessagLine(ex.Message + " StackTrace:" + ex.StackTrace.ToString());

                        Console.WriteLine("Error: " + transfer.SupplierTransferName);

                    }
                }
            }
            //);
        }
    }
}

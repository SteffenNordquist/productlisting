﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers.Generator
{
    public interface ITransfersGenerator
    {
        void Generate();
    }
}

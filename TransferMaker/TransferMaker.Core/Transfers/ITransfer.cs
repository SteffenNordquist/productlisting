﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Transfers
{
    public interface ITransfer
    {
        string SupplierTransferName { get; }

        string ErrorLog { get; }

        void DoTransfer();
    }
}

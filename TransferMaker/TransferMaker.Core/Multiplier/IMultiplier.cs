﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Multiplier
{
    public interface IMultiplier
    {
        int GetMultiplier(string sku);
    }
}

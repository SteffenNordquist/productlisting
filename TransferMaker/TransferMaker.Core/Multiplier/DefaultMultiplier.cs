﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Multiplier
{
    public class DefaultMultiplier : IMultiplier
    {
        private int multiplier = 1;

        public int GetMultiplier(string sku)
        {
            if (sku == null)
            {
                throw new ArgumentNullException("sku must be set");
            }

            if (sku.Split('-').Length > 3)
            {
                if (sku.Split('-')[3] != "")
                {
                    try
                    {
                        multiplier = Convert.ToInt32(sku.Split('-')[3]);
                    }
                    catch { }
                }
            }

            return multiplier;
        }
    }
}

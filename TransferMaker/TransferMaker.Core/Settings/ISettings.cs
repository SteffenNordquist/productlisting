﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Settings
{
    public interface ISettings
    {
        void SetValue(string name, string value);

        string GetValue(string name);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Constructors
{
    public class JTLSkuToBeBlockedConstructor : ISkuToBeBlockedConstructor
    {
        public string GetSKU(string sku)
        {
            string[] skuSplit = sku.Split('-');

            string jtlSku = "JTL-" + string.Join("-", skuSplit.Skip(1).Take(skuSplit.Count() - 1));

            return jtlSku;
        }
    }
}

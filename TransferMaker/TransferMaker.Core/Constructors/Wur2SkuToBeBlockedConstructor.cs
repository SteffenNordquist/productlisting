﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Constructors
{
    public class Wur2SkuToBeBlockedConstructor : ISkuToBeBlockedConstructor
    {
        public string GetSKU(string sku)
        {
            string[] skuSplit = sku.Split('-');

            string wur2Sku = "";

            if (sku.StartsWith("WUR"))
            {
                wur2Sku = "WUR2-" + string.Join("-", skuSplit.Skip(1).Take(skuSplit.Count() - 1));
            }

            return wur2Sku;
        }
    }
}

﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.DataContext
{
    public class SuppliersDataContext : IDataContext
    {
        private readonly IDbConnection _dbConnection;

        /// <summary>
        /// Suppliers Data Context
        /// </summary>
        ///<param name="configurationManager">configurations container</param>
        /// <param name="dbConnectionFactory">database connection factory</param>
        public SuppliersDataContext(IConfigurationManager configurationManager, IDbConnectionFactory dbConnectionFactory)
        {
            dbConnectionFactory.ConnectionProperties.SetProperty("connectionstring", configurationManager.GetValue("suppliersconnectionstring"));
            dbConnectionFactory.ConnectionProperties.SetProperty("databasename", configurationManager.GetValue("suppliersdatabasename"));
            dbConnectionFactory.ConnectionProperties.SetProperty("collectionname", configurationManager.GetValue("supplierscollectionname"));

            this._dbConnection = dbConnectionFactory.CreateConnection(); 
        }

        public IDbConnection DbConnection
        {
            get { return _dbConnection; }
        }
    }
}

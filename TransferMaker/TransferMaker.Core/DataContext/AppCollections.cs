﻿using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.DataContext
{
    public static class AppCollections
    {
        private static IDbConnection suppliersDBConnection;
        private static IDbConnection skuBlackListDBConnection;
        private static IDbConnection productSizesDBConnection;

        /// <summary>
        /// Works on returning the 'supplier' collection
        /// </summary>
        /// <returns>Returns 'supplier' collection</returns>
        public static IDbConnection SuppliersCollection()
        {
            if (suppliersDBConnection == null)
            {
                throw new NullReferenceException("The 'supplier' collection must be set at the entry point of the application.");
            }
            
            return suppliersDBConnection;
        }

        /// <summary>
        /// Sets the 'supplier' collection
        /// </summary>
        /// <param name="dataContext">'supplier' data context</param>
        public static void SetSuppliersCollection(IDataContext dataContext)
        {
            suppliersDBConnection = dataContext.DbConnection;
        }

        /// <summary>
        /// Works on returning the 'sku black list' collection
        /// </summary>
        /// <returns>Returns 'sku black list' collection</returns>
        public static IDbConnection SkuBlackListCollection()
        {
            if (skuBlackListDBConnection == null)
            {
                throw new NullReferenceException("The 'sku black list' collection must be set at the entry point of the application.");
            }

            return skuBlackListDBConnection;
        }

        /// <summary>
        /// Sets the 'sku black list' collection
        /// </summary>
        /// <param name="dataContext">'sku black list' data context</param>
        public static void SetSkuBlackListCollection(IDataContext dataContext)
        {
            skuBlackListDBConnection = dataContext.DbConnection;
        }

        /// <summary>
        /// Works on returning the 'productsizes' collection
        /// </summary>
        /// <returns>Returns 'productsizes' collection</returns>
        public static IDbConnection ProductSizesCollection()
        {
            if (productSizesDBConnection == null)
            {
                throw new NullReferenceException("The 'sku black list' collection must be set at the entry point of the application.");
            }

            return productSizesDBConnection;
        }

        /// <summary>
        /// Sets the 'ProductSizes' collection
        /// </summary>
        /// <param name="dataContext">'ProductSizes' data context</param>
        public static void SetProductSizesCollection(IDataContext dataContext)
        {
            productSizesDBConnection = dataContext.DbConnection;
        }
    }
}

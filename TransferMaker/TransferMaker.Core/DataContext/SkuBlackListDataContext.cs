﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.DataContext
{
    public class SkuBlackListDataContext : IDataContext
    {
        private readonly IDbConnection _dbConnection;

        /// <summary>
        /// Suppliers Data Context
        /// </summary>
        ///<param name="configurationManager">configurations container</param>
        /// <param name="dbConnectionFactory">database connection factory</param>
        public SkuBlackListDataContext(IConfigurationManager configurationManager, IDbConnectionFactory dbConnectionFactory)
        {
            dbConnectionFactory.ConnectionProperties.SetProperty("connectionstring", configurationManager.GetValue("skublacklistconnectionstring"));
            dbConnectionFactory.ConnectionProperties.SetProperty("databasename", configurationManager.GetValue("skublacklistdatabasename"));
            dbConnectionFactory.ConnectionProperties.SetProperty("collectionname", configurationManager.GetValue("skublacklistcollectionname"));

            this._dbConnection = dbConnectionFactory.CreateConnection(); 
        }

        public IDbConnection DbConnection
        {
            get { return _dbConnection; }
        }
    }
}

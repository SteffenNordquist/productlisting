﻿using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.DataContext
{
    public interface IDataContext
    {
        IDbConnection DbConnection { get; }
    }
}

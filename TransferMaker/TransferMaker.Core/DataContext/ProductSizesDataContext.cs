﻿using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.DataContext
{
    public class ProductSizesDataContext : IDataContext
    {
        private readonly IDbConnection _dbConnection;

        /// <summary>
        /// product sizes Data Context
        /// </summary>
        ///<param name="configurationManager">configurations container</param>
        /// <param name="dbConnectionFactory">database connection factory</param>
        public ProductSizesDataContext(IConfigurationManager configurationManager, IDbConnectionFactory dbConnectionFactory)
        {
            dbConnectionFactory.ConnectionProperties.SetProperty("connectionstring", configurationManager.GetValue("productsizesconnectionstring"));
            dbConnectionFactory.ConnectionProperties.SetProperty("databasename", configurationManager.GetValue("productsizesdatabasename"));
            dbConnectionFactory.ConnectionProperties.SetProperty("collectionname", configurationManager.GetValue("productsizescollectionname"));

            this._dbConnection = dbConnectionFactory.CreateConnection(); 
        }

        public IDbConnection DbConnection
        {
            get { return _dbConnection; }
        }
    }
}

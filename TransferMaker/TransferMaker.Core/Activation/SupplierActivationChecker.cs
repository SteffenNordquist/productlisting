﻿using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Core.Activation
{
    /// <summary>
    /// Checks supplier activation
    /// </summary>
    public class SupplierActivationChecker : IActivationChecker
    {
        /// <summary>
        /// Container of the deactivated suppliers
        /// </summary>
        private List<string> deactivatedSuppliers = new List<string>();

        public SupplierActivationChecker(IDataQuery dataQueryContext)
        {
            string query = "{ active : false }";
            string[] setFields = {"_id"};

            dataQueryContext.Find<BsonDocument>(query, setFields).ToList()
                .ForEach(x=> deactivatedSuppliers.Add(x["_id"].AsString));
        }

        /// <summary>
        /// Works on checking the activation status of the passed supplier
        /// </summary>
        /// <typeparam name="T">type of the parameter</typeparam>
        /// <param name="obj">parameter value</param>
        /// <returns>Returns true/false if the supplier passed is active or deactivated</returns>
        public bool IsActivated<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("Param 'obj' must not be null");
            }

            string supplierName = (string)(object)obj;

            return !deactivatedSuppliers.Contains(supplierName, StringComparer.OrdinalIgnoreCase);
        }
    }
}

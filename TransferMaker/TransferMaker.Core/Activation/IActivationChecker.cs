﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.Activation
{
    /// <summary>
    /// Checks supplier activation
    /// </summary>
    /// <seealso cref="TransferMaker.Core.Activation.SupplierActivationChecker"/>
    public interface IActivationChecker
    {
        /// <summary>
        /// Works on checking the activation status of the passed param
        /// </summary>
        /// <typeparam name="T">type of the parameter</typeparam>
        /// <param name="obj">parameter value</param>
        /// <returns>Returns true/false if the param passed is active or deactivated</returns>
        bool IsActivated<T>(T obj);
    }
}

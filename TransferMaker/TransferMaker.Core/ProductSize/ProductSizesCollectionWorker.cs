﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.ProductSize
{
    public class ProductSizesCollectionWorker : ProductSizesCollection, IProductSizesCollection
    {
        private int _length = 0;
        public int Length
        {
            get { return _length; }
        }

        private int _width = 0;
        public int Width
        {
            get { return _width; }
        }

        private int _height = 0;
        public int Height
        {
            get { return _height; }
        }

        private int _weight = 0;
        public int Weight
        {
            get { return _weight; }
        }


        public void ProcessProductSizes(string ean)
        {
            var productSize = GetProductSizes().Find(x => x.ean == ean);

            if (productSize != null)
            {
                _length = productSize.Length;
                _height = productSize.Height;
                _width = productSize.Width;
                _weight = productSize.Weight;
            }
        }
    }
}

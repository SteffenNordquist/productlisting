﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;
using TransferMaker.Core.Factories.SupplierNameFactory;

namespace TransferMaker.Core.ProductSize
{
    public class SupplierDefaultProductSize : SuppliersDefaultSizesList, IDefaultProductSize
    {
        private int _length = 0;
        public int Length
        {
            get { return _length; }
        }

        private int _width = 0;
        public int Width
        {
            get { return _width; }
        }

        private int _height = 0;
        public int Height
        {
            get { return _height; }
        }

        private int _weight = 0;
        public int Weight
        {
            get { return _weight; }
        }

        private readonly ISupplierNameFactory supplierNameFactory;

        public SupplierDefaultProductSize(ISupplierNameFactory supplierNameFactory)
        {
            this.supplierNameFactory = supplierNameFactory;
        }

        public void ProcessProductSizes(string supplierName)
        {
            supplierName = supplierNameFactory.GetSupplierName(supplierName);

            var supplier = GetSuppliersDefaultSizes().Find(x => x.SupplierName == supplierName);

            if (supplier != null)
            {
                _length = supplier.Length;
                _height = supplier.Height;
                _width = supplier.Width;
                _weight = supplier.Weight;
            }
        }
    }
}

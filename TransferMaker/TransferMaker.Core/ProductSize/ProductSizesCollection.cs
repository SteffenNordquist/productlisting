﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Core.ProductSize
{
    public abstract class ProductSizesCollection
    {
        private List<ProductSizesCollectionEntity> productSizes = null;

        public List<ProductSizesCollectionEntity> GetProductSizes()
        {
            if (productSizes == null)
            {
                productSizes = new List<ProductSizesCollectionEntity>();

                string query = " { _id : {$exists:true} } ";
                var productSizeDocuments = AppCollections.ProductSizesCollection().DataAccess.Queries.Find<BsonDocument>(query);

                foreach (var productSizeDocument in productSizeDocuments)
                {
                    int _length = 0, _width = 0, _height = 0, _weight = 0;
                    if (productSizes.Find(x => x.ean == productSizeDocument["ean"].AsString) == null)
                    {
                        if (productSizeDocument.Contains("measure") && productSizeDocument["measure"] != null)
                        {
                            var measure = productSizeDocument["measure"].AsBsonDocument;
                            if (measure.Contains("length"))
                            {
                                int.TryParse(measure["length"].ToString(), out _length);
                            }
                            if (measure.Contains("width"))
                            {
                                int.TryParse(measure["width"].ToString(), out _width);
                            }
                            if (measure.Contains("height"))
                            {
                                int.TryParse(measure["height"].ToString(), out _height);
                            }
                            if (measure.Contains("weight"))
                            {
                                int.TryParse(measure["weight"].ToString(), out _weight);
                            }
                        }

                        productSizes.Add(
                                new ProductSizesCollectionEntity
                                {
                                    ean = productSizeDocument["ean"].AsString,
                                    Length = _length,
                                    Height = _height,
                                    Width = _width,
                                    Weight = _weight
                                }
                            );
                    }
                }


            }

            return productSizes;
        }
    }
}

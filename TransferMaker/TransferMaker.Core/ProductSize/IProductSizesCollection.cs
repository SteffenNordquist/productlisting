﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransferMaker.Core.ProductSize
{
    public interface IProductSizesCollection
    {
        int Length { get; }
        int Width { get; }
        int Height { get; }
        int Weight { get; }

        void ProcessProductSizes(string ean);
    }
}

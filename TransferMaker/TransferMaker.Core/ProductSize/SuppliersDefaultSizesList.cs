﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferMaker.Core.DataContext;

namespace TransferMaker.Core.ProductSize
{
    public abstract class SuppliersDefaultSizesList
    {
        private List<SuppliersDefaultSizeEntity> defaultSizes = null;

        public List<SuppliersDefaultSizeEntity> GetSuppliersDefaultSizes()
        {
            if (defaultSizes == null)
            {
                defaultSizes = new List<SuppliersDefaultSizeEntity>();

                string query = " { _id : {$exists:true} } ";
                var supplierDocuments = AppCollections.SuppliersCollection().DataAccess.Queries.Find<BsonDocument>(query);

                foreach (var supplierDocument in supplierDocuments)
                {
                    int _length = 0, _width = 0, _height = 0, _weight = 0;
                    if (defaultSizes.Find(x => x.SupplierName == supplierDocument["_id"].AsString) == null)
                    {
                        if (supplierDocument.Contains("plus") && supplierDocument["plus"].AsBsonDocument.Contains("simpleSize"))
                        {
                            var simpleSize = supplierDocument["plus"]["simpleSize"].AsBsonDocument;
                            if (simpleSize.Contains("Length"))
                            {
                                int.TryParse(simpleSize["Length"].ToString(), out _length);
                            }
                            if (simpleSize.Contains("Width"))
                            {
                                int.TryParse(simpleSize["Width"].ToString(), out _width);
                            }
                            if (simpleSize.Contains("Height"))
                            {
                                int.TryParse(simpleSize["Height"].ToString(), out _height);
                            }
                            if (simpleSize.Contains("Weight"))
                            {
                                int.TryParse(simpleSize["Weight"].ToString(), out _weight);
                            }
                        }

                        defaultSizes.Add(
                                new SuppliersDefaultSizeEntity
                                {
                                    SupplierName = supplierDocument["_id"].AsString,
                                    Length = _length,
                                    Height = _height,
                                    Width = _width,
                                    Weight = _weight
                                }
                            );
                    }
                }


            }

            return defaultSizes;
        }
    }
}
